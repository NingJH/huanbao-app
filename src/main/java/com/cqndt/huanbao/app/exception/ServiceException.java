package com.cqndt.huanbao.app.exception;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created with IntelliJ IDEA
 * User: zhaojianji
 * Date: 2016/11/22
 * Time: 11:47
 */

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class ServiceException extends RuntimeException {
    private int errCode;

    private String errMsg;

    public ServiceException(ErrorType errorType) {

        this.errCode = errorType.getCode();
        this.errMsg = errorType.getErrorMsg();
    }
}
