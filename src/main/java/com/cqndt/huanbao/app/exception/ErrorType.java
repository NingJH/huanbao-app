package com.cqndt.huanbao.app.exception;

/**
 * user: zhaojianji
 * date: 2017/05/25
 * desc:  描述用途
 */
public enum ErrorType {

    SERVER_ERROR(500, "服务出错!"),
    DATA_BIND_ERROR(405, "数据绑定错误!"),

    NOT_FOUND(404, "不存在该资源!"),
    FORBIDDEN(403, "没有权限"),
    TOKEN_ERROR(401, "登陆过期,请重新登陆"),
    USER_OR_PASSWORD_ERROR(200, "用户名或密码错误!"),
    USER_NOT_ENABLE_ERROR(200, "当前用户不能登录!"),
    USER_EXIST(200, "用户名已存在!"),
    ROLE_EXIST(200, "角色名已存在!"),
    FILE_NOT_EXIST(200, "文件不存在!"),
    QUALITY_EXIST(200, "同一园区只能新增一条环境质量!"),
    COMPANY_FACILITIE_EXIST(200, "同一企业只能新增一条企业环保设施!"),
    COMPANY_PROCEDURE_EXIST(200, "同一企业只能新增一条同一类型的手续!"),
    COMPANY_RISK_EXIST(200, "同一企业只能新增一条风险源!"),
    PARK_FACILITIE_EXIST(200, "同一园区只能新增一条园区环保设施!"),
    PARK_EXIST(200, "同一区域只能新增一条园区!"),
    DICTIONARY_EXIST(200, "标识不能重复,请重新输入!"),
    DICTIONARY_NAME_EXIST(200, "名称不能重复,请重新输入!"),
    FILE_ERROR(602, "文件不能为空!"),
    PHONE_EXIST(500,"电话已存在！"),
    ROLE_NOT_EXIST(200,"未绑定角色不能登录存在！"),
    FILE_NOT_BE_PIC(603, "文件不能为空!");



    private int code;
    private String errorMsg;

    ErrorType(int code, String errorMsg) {
        this.code = code;
        this.errorMsg = errorMsg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }


   /* private static  final Map<String,ErrorType> ERROR_TYPE_MAP = new HashMap<>();

    static {
        for(ErrorType type : values()){
            ERROR_TYPE_MAP.put(type.name(),type);
        }
    }

    public static ErrorType fromString(String name){

        return ERROR_TYPE_MAP.get(name);
    }*/


}
