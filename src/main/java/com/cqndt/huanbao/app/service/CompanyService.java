package com.cqndt.huanbao.app.service;


import java.util.List;
import java.util.Map;

public interface CompanyService {

    Map<String, Object> sortCompanyType();

    Map<String, Object> listCompanyTypeInfo();

    /**
     * 根据园区id 查询园区下面的公司
     * @param id
     * @return
     */
    List<Map<String,String>> selectCompanyList(Integer id, String context,Integer regionId,Integer userId);

    List<Map<String,Object>> getCompanyLonLat(Integer parkId, Integer regionId, int userId);
}
