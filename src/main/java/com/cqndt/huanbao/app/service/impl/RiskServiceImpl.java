package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbRiskMapper;
import com.cqndt.huanbao.app.service.RiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-29 11:02
 **/
@Service
public class RiskServiceImpl implements RiskService {

    @Autowired
    HbRiskMapper hbRiskMapper;

    @Override
    public List<Map<String, Object>> getParkInfoByUserId(Integer parkId,Integer regionId,Integer typeId,Integer waterTypeId,Integer airTypeId ,Integer udwaterTypeId,Integer soilTypeId,int userId) {
        switch (typeId){
            case 1:
                waterTypeId = 36;//水重大
                break;
            case 2:
                waterTypeId = 37;//水较大
                break;
            case 3:
                waterTypeId = 38;//水一般
                break;
            case 4:
                airTypeId = 39;//气重大
                break;
            case 5:
                airTypeId = 40;//气较大
                break;
            case 6:
                airTypeId = 41;//气一般
                break;
            case 7:
                udwaterTypeId = 1;//地下水
                break;
            case 8:
                soilTypeId = 1;//土壤
                break;
        }
        return hbRiskMapper.getParkInfoByUserId(parkId,regionId,waterTypeId,airTypeId,udwaterTypeId,soilTypeId,userId);
    }
}
