package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbParkMapper;
import com.cqndt.huanbao.app.pojo.HbPark;
import com.cqndt.huanbao.app.service.HbParkService;
import com.cqndt.huanbao.app.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HbParkServiceImpl implements HbParkService {

    @Autowired
    private HbParkMapper hbParkMapper;

    @Override
    public Map selectByUserId(int userId) {
        List<Map<String, Object>> list = hbParkMapper.selectByUserId(userId);
        Map<String, Object> map = new HashMap<>();
        if (null != list && list.size() > 0) {
            for (Map<String, Object> map1 : list) {
                Integer parkId = Integer.valueOf(map1.get("id").toString());
                List<Map<String, Object>> regionInfo = hbParkMapper.listRegionInfo(parkId);
                map1.put("regionInfo", regionInfo);
            }
        }
        map.put("park", list);
        return map;
    }

    @Override
    public HbPark selectParkById(Integer id) {
        HbPark park=hbParkMapper.selectPark(id);
        return park;
    }

    @Override
    public List<Map<String,String>> selectReportById(Integer parkId,Integer regionId,String context,Integer type) {
        List<Map<String,String>> reports=hbParkMapper.selectReportById(parkId,regionId,context,type);
        return reports;
    }

    @Override
    public String getGisLayer(Integer parkId, Integer regionId) {
        String layerUrl = null;
        if(!StringUtils.isEmpty(parkId)){
            layerUrl = hbParkMapper.getParkGisLayer(parkId);
        }
        if(!StringUtils.isEmpty(regionId)){
            layerUrl = hbParkMapper.getRegionGisLayer(regionId);
        }

        return layerUrl;
    }

    @Override
    public List<Map<String,Object>> getRegionLonLat(Integer regionId,Integer parkId) {
        return hbParkMapper.getRegionLonLat(regionId,parkId);
    }

    @Override
    public List<Map<String, Object>> fuJianList(Integer parkId,Integer regionId,String context) {
        return hbParkMapper.fuJianList(parkId,regionId,context);
    }

    @Override
    public List<Map<String, Object>> selectProcedureExamine(Integer parkId,Integer type,String context) {
        List<Map<String, Object>> maps = hbParkMapper.selectProcedureExamine(parkId,type,context);
//        maps.forEach(map->map.put("date", DateUtil.formatTime(String.valueOf(map.get("date")))));
        return maps;
    }
}
