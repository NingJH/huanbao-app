package com.cqndt.huanbao.app.service;

import com.cqndt.huanbao.app.pojo.HbFacilities;
import com.cqndt.huanbao.app.pojo.HbFacilitiesDetail;

import java.util.List;
import java.util.Map;

public interface FacilitiesService {

    /**
     * 根据园区id，查询园区设备列表
     * @param id
     * @return
     */
    List<Map<String,Object>> selectFacilitiesList(Integer id, String context,Integer regionId);

//    /**
//     * 根据环保设施id，查询环保设施详情
//     * @param id
//     * @return
//     */
//    List<Map<String,Object>> selectFacilitiesDetail(Integer id);
//
//    /**
//     * 根据环保设施id，查询环保设施基本信息
//     * @param id
//     * @return
//             */
//    HbFacilities selectFacilitiesinfo(Integer id);
}
