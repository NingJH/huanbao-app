package com.cqndt.huanbao.app.service;

import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-31 15:32
 **/
public interface MeasurePointService {

    List<Map<String,Object>> getmeasurePointLonLat(Integer parkId, Integer regionId, Integer measurePointType, int userId);

    List<Map<String,Object>> measureStatisticsRank(Integer parkId,Integer regionId,Integer type, int userId);
}
