package com.cqndt.huanbao.app.service.impl;


import com.cqndt.huanbao.app.dao.BuildingSewageTreatmenPlantMapper;
import com.cqndt.huanbao.app.service.BuildingSewageTreatmenPlantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BuildingSewageTreatmenPlantServiceImpl implements BuildingSewageTreatmenPlantService {

    @Autowired
    private BuildingSewageTreatmenPlantMapper buildingSewageTreatmenPlantMapper;

    @Override
    @Transactional
    public Map<String,Object> queryPlants(Integer buildId) {
//        List<BuildingSewageTreatmenPlants> list=buildingSewageTreatmenPlantMapper.selectPlants(buildId);
        Map<String,Object> baseInfo=buildingSewageTreatmenPlantMapper.selectPlants(buildId);
        List<Map<String,Object>> proInfo=buildingSewageTreatmenPlantMapper.selectPro(buildId);
        List<Map<String,Object>> sewInfo=buildingSewageTreatmenPlantMapper.selectSew(buildId);
        List<Map<String,Object>> unitInfo=buildingSewageTreatmenPlantMapper.selectUnit(buildId);

        baseInfo.put("proInfo",proInfo);
        baseInfo.put("sewInfo",sewInfo);
        baseInfo.put("unitInfo",unitInfo);

        return baseInfo;
    }


    @Override
    public List<Map<String,Object>> echar(Integer parkId,String deviceNo) {
        List<Map<String,Object>> resultList = new ArrayList<>();//总返回值
        if (null!=deviceNo&&deviceNo!=""){
            if (deviceNo.equals("20190826888888_001")){
                List<Map<String,Object>> tbs001 = buildingSewageTreatmenPlantMapper.selectTbs001();
                resultList.add(test1(tbs001,"pH值","无量纲"));
            }
            if (deviceNo.equals("20190826888888_011")) {
                List<Map<String,Object>> tbs011 = buildingSewageTreatmenPlantMapper.selectTbs011();
                resultList.add(test1(tbs011,"化学需氧量","毫克/升"));
            }
            if (deviceNo.equals("20190826888888_060")) {
                List<Map<String,Object>> tbs060 = buildingSewageTreatmenPlantMapper.selectTbs060();
                resultList.add(test1(tbs060,"氨氮","毫克/升"));
            }
            if (deviceNo.equals("20190826888888_101")) {
                List<Map<String,Object>> tbs101 = buildingSewageTreatmenPlantMapper.selectTbs101();
                resultList.add(test1(tbs101,"总磷","毫克/升"));
            }
        }

        return resultList;
    }
    private Map<String,Object> test1(List<Map<String,Object>> list,String name,String unit){
        Map<String,Object> map = new HashMap<>();
        List<Map<String,Object>> data = new ArrayList<>();
        for(Map<String,Object> so2 : list){
            Map<String,Object> val = new HashMap<>();
            val.put("key",so2.get("k"));
            val.put("value",so2.get("v"));
            val.put("date",so2.get("createTime"));
            map.put("name",name);
            map.put("isChoiced",false);
            map.put("unit",unit);
            data.add(val);
        }
        map.put("result",data);
        return map;
    }

    private Map<String,Object> test(List<Map<String,Object>> maps,String name,String unit){
        if(maps.size()<1){
            return new HashMap<>();
        }
        Map<String,Object> resultMap = new HashMap<>();
        Map<String,Object> resultMap2 = new HashMap<>();
        List<Map<String,Object>> list = new ArrayList<>();
        for(Map<String,Object> map : maps){
            if(map.get("k").toString().contains("Rtd")){
                Map<String,Object> map1 = new HashMap<>();
                map1.put("name",map.get("create_time"));
                List<Object> a = new ArrayList<>();
                a.add(map.get("create_time"));
                a.add(map.get("v"));
                map1.put("value",a);
                list.add(map1);
            }
        }
        resultMap2.put("dw",unit);
        resultMap2.put("data",list);
        resultMap.put(name,resultMap2);
        return resultMap;
    }
}
