package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.BuildingRainSewageMapper;
import com.cqndt.huanbao.app.pojo.BuildinRrainSewagePdf;
import com.cqndt.huanbao.app.service.IBuildingRainSewageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class BuildingRainSewageServiceImpl implements IBuildingRainSewageService {

    @Autowired
    private BuildingRainSewageMapper buildingRainSewageMapper;

    @Override
    public List<Map<String, Object>> selectRainSewagePdf(Integer rsId) {
        return buildingRainSewageMapper.selectRainSewagePdf(rsId);
    }
}
