package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbKeeperLonlatMapper;
import com.cqndt.huanbao.app.dao.HbUserMapper;
import com.cqndt.huanbao.app.service.KeeperLonlatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class KeeperLonlatServiceImpl implements KeeperLonlatService {

    @Autowired
    HbKeeperLonlatMapper hbKeeperLonlatMapper;

    @Autowired
    HbUserMapper hbUserMapper;

    @Override
    public List<Map<String, Object>> listKeeperLonlat(String parkId,String regionId) {
        List<Map<String,Object>> keeperLonlatList = hbKeeperLonlatMapper.listKeeperLonlat(parkId,regionId);
        return keeperLonlatList;
    }
}
