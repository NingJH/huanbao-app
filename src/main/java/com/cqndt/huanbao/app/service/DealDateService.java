package com.cqndt.huanbao.app.service;

import java.lang.annotation.*;

/**
 * Create by Intellij IDEA
 * User : mengjiajie
 * Mail : 15826014394@163.com
 * Time : 2019/6/12 17:23
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface DealDateService {
    String dealDate();
}
