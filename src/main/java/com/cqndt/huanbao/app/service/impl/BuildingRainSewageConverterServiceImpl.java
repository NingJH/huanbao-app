package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.BuildingRainSewageConverterMapper;
import com.cqndt.huanbao.app.pojo.BuildingRainSewageConverterPdf;
import com.cqndt.huanbao.app.service.IBuildingRainSewageConverterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class BuildingRainSewageConverterServiceImpl implements IBuildingRainSewageConverterService {

    @Autowired
    private BuildingRainSewageConverterMapper buildingRainSewageConverterMapper;

    @Override
    public List<BuildingRainSewageConverterPdf> selectRainSewageConverterAccount(BuildingRainSewageConverterPdf buildingRainSewageConverterPdf) {
        return buildingRainSewageConverterMapper.selectPdf(buildingRainSewageConverterPdf.getRscId());
    }
}
