package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbBuildingGeneralMapper;
import com.cqndt.huanbao.app.pojo.BuildingGeneral;
import com.cqndt.huanbao.app.pojo.BuildingGeneralAccount;
import com.cqndt.huanbao.app.pojo.BuildingGeneralGo;
import com.cqndt.huanbao.app.pojo.BuildingGeneralMeasure;
import com.cqndt.huanbao.app.service.IBuildingGeneralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Time : 2019-09-19 19:10
 **/
@Service
public class IBuildingGeneralServiceImpl implements IBuildingGeneralService {


    @Autowired
    private HbBuildingGeneralMapper hbBuildingGeneralMapper;

    @Override
    public BuildingGeneral selectGeneral(Integer id) {
        BuildingGeneral buildingGeneral = hbBuildingGeneralMapper.selectGeneral(id);
        buildingGeneral.setBuildingGeneralMeasureList(hbBuildingGeneralMapper.selectGeneralMeasure(buildingGeneral.getId()));
        buildingGeneral.setBuildingGeneralAccountList(hbBuildingGeneralMapper.selectGeneralAccount(buildingGeneral.getId()));
        return buildingGeneral;
    }


    @Override
    public List<BuildingGeneralGo> queryGeneralGo(BuildingGeneralGo buildingGeneralGo) {
        List<BuildingGeneralGo> bg=hbBuildingGeneralMapper.queryGeneralGo(buildingGeneralGo);
        return bg;
    }

    @Override
    public List<BuildingGeneralMeasure> selectGeneralMeasure(Integer generalId) {
        return hbBuildingGeneralMapper.selectGeneralMeasure(generalId);
    }

    @Override
    public List<BuildingGeneralAccount> selectGeneralAccount(BuildingGeneralAccount buildingGeneralAccount) {
        return hbBuildingGeneralMapper.selectGeneralAccount(buildingGeneralAccount.getGeneralId());
    }

}
