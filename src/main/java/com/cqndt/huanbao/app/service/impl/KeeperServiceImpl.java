package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbIOtherAlarmRecordsMapper;
import com.cqndt.huanbao.app.dao.HbKeeperMapper;
import com.cqndt.huanbao.app.dao.HbUserMapper;
import com.cqndt.huanbao.app.pojo.HbKeeper;
import com.cqndt.huanbao.app.pojo.HbKeeperLonlat;
import com.cqndt.huanbao.app.service.KeeperService;
import com.cqndt.huanbao.app.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class KeeperServiceImpl implements KeeperService {
    @Autowired
    HbKeeperMapper hbKeeperMapper;
    @Autowired
    HbUserMapper hbUserMapper;
    @Autowired
    HbIOtherAlarmRecordsMapper hbIOtherAlarmRecordsMapper;

    @Override
    public List<HbKeeper> listKeeper(String name) {
        return hbKeeperMapper.listKeeper(name);
    }

    @Override
    public List<Map<String, Object>> selectRecordList(Integer companyId, Integer userId, Integer type) {
        String roleName=hbUserMapper.getRoleName(userId);
        List<Map<String, Object>> recordListt=null;
        if (roleName.equals("环保管家移动版")){
            log.info("普通手机管理员");
             recordListt=hbKeeperMapper.recordList1(companyId,userId,type);
        }
        if (roleName.equals("手机超级管理员")){
            log.info("手机超级管理员");
           recordListt=hbKeeperMapper.recordList(companyId,type);
        }
        return recordListt;
    }

    @Override
    public List<Map<String, Object>> selectMeasureDetail(Integer parkId, Integer measureId, Integer type,Integer regionId) {
        //获取指定时间当天的结束时间
        Date beginTime= DateUtil.getDateToEnd(new Date());
        String begin=DateUtil.dateToString(beginTime);

        //获取距当天 10天之前10天的日期
        Date time=DateUtil.beforeNDaysDate(new Date(),10);

        //获取指定时间当天的开始时间
        Date endTime=DateUtil.getDateToStart(time);
        String end=DateUtil.dateToString(endTime);
        Double max=0.0;
        List<Map<String, Object>> measureDetail=hbKeeperMapper.measureDetail(parkId,measureId,type,begin,end,regionId);
        List<Map<String, Object>> maxData=hbKeeperMapper.max(parkId,measureId,type,begin,end,regionId);
        String maxValue="";
        String maxValueTime="";
        for(Map<String,Object> map:maxData){
            maxValue=String.valueOf(map.get("point_value"));
            maxValueTime=String.valueOf(map.get("t"));
        }
        if(measureDetail.size()==1){
            Date now=new Date();
            Map<String,Object> map=new HashMap<>();
            map.put("parkName","0.0");
            map.put("t",now);
            map.put("pointName","0.0");
            map.put("point_value","0.0");
            map.put("item_name","0.0");
            map.put("time",now);
            map.put("uploadTime",now);
            map.put("lat_lon","0.0");
            map.put("target","0.0");
            measureDetail.add(map);
        }
        for (Map<String,Object> map:measureDetail){
            map.put("maxValue", maxValue);
            map.put("maxValueTime",maxValueTime);
        }
        return measureDetail;
    }

    @Override
    public List<Map<String, Object>> selectMeasureList(Integer parkId,String context,Integer regionId,Integer type) {
        List<Map<String,Object>> list=hbKeeperMapper.measureList(parkId,context,regionId,type);
        for (Map<String,Object> map:list){
            int id=Integer.parseInt(String.valueOf(map.get("id")));
            List<Map<String,Object>> tp=hbKeeperMapper.lists(id);
            map.put("type",tp);
        }
        return list;
    }

    @Override
    public List<Map<String,Object>> selectWarnAlarmList(Integer parkId,String context,Integer userId) {
        List<Map<String,Object>> list=hbKeeperMapper.warnAlarmList(parkId,context);
        Date time=new Date();
        String now=DateUtil.dateToStr(time);
        for (Map<String,Object> map: list) {
            Integer id=Integer.parseInt(String.valueOf(map.get("id")));
            List<Map<String,Object>> detail=hbKeeperMapper.warnAlarmDetail(id);
            for(Map<String,Object> m: detail){
                String timeAd=String.valueOf(m.get("alarm_date"));//告警time
                String timeWd=String.valueOf(m.get("warn_date"));//预警time
                if(timeAd!=null&&!"".equals(timeAd)&&now.compareTo(timeAd)>0){
                    m.put("status","告警");
                    continue;
                }
                if(timeWd!=null&&!"".equals(timeWd)&&now.compareTo(timeWd)>0){
                    m.put("status","预警");
                }
            }
            map.put("detail",detail);
        }

        return list;
    }

    @Override
    public List<Map<String,Object>> selectWarnAlarmDetail(Integer id) {
        List<Map<String,Object>> list=hbKeeperMapper.warnAlarmDetail(id);
        return list;
    }

    @Override
    public int addKeeperLonlat(String lon, String lat, Integer userId) {
        //用户经纬度
        HbKeeperLonlat hbKeeperLonlat=hbKeeperMapper.queryKeeperLonlat(userId);
        System.out.println("用户经纬度："+hbKeeperLonlat);
        Integer keeperId = hbKeeperMapper.selectKeeperId(userId);
        System.out.println("查询环保管家id");
        int count=0;
        Date dt = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("添加经纬度时间》》》》》》"+formatter.format(dt));

        if(hbKeeperLonlat!=null){
            System.out.println("有经纬度进行经纬度更新");
            count=hbKeeperMapper.updateKeeperLonlat(keeperId,lon,lat,formatter.format(dt),userId);
        }else {
            System.out.println("无经纬度进行定位数据增加");
            count=hbKeeperMapper.addKeeperLonlat(keeperId,lon,lat,formatter.format(dt),userId);
        }
        if (count!=0){
            return 1;
        }
        return 0;
    }

    @Override
    public List<Map<String, Object>> getsgAlarmList(String name, Integer userId) {
        return hbKeeperMapper.getsgAlarmList(name,userId);
    }
}
