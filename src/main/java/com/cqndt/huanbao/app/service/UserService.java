package com.cqndt.huanbao.app.service;


import com.cqndt.huanbao.app.pojo.HbUser;
import com.cqndt.huanbao.app.pojo.JwtToken;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface UserService {

//    Map<String, Object> getUserInfo(String username, String password);

    JwtToken getUserByUsernameAndPassword(HbUser user, HttpServletRequest request, HttpServletResponse response);
}
