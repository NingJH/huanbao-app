package com.cqndt.huanbao.app.service;


import com.cqndt.huanbao.app.pojo.BuildingAccidentPool;
import com.cqndt.huanbao.app.pojo.BuildingAccidentPoolAnnal;

import java.util.List;

/**
 * @Time : 2019-09-19 09:46
 **/
public interface IBuildingAccidentPoolService {


    /**
     * 查询应急事故池
     * @param id
     * @return
     */
    List<BuildingAccidentPool> selectAccidentPool(Integer id);

    /**
     * 查询应急事故池记录
     * @param buildingAccidentPoolAnnal
     * @return
     */
    List<BuildingAccidentPoolAnnal> selectAccidentPoolAnnl(BuildingAccidentPoolAnnal buildingAccidentPoolAnnal);

}
