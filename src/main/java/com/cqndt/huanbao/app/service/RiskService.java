package com.cqndt.huanbao.app.service;

import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-29 11:01
 **/
public interface RiskService {

    List<Map<String,Object>> getParkInfoByUserId(Integer parkId, Integer regionId, Integer typeId,Integer waterTypeId, Integer airTypeId, Integer udwaterTypeId, Integer soilTypeId, int userId);

}
