package com.cqndt.huanbao.app.service;

import java.util.List;

public interface DictionaryItemService {
    List<String> listDictionaryItemAll(String typeCode);
}
