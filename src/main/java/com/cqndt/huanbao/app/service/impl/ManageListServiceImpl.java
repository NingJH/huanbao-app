package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbManageListMapper;
import com.cqndt.huanbao.app.service.ManageListService;
import com.cqndt.huanbao.app.util.PdfUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ManageListServiceImpl implements ManageListService {

    @Autowired
    HbManageListMapper hbManageListMapper;

    @Autowired
    PdfUtil pdfUtil;
    @Override
    public List<Map<String, Object>> getManageListByUserId(Integer userId,String name) {
        List<Integer> companyId = hbManageListMapper.getUserId(userId);
        if(companyId!=null && companyId.size()>0){
            return hbManageListMapper.getManageListByUserId(companyId,name);
        }
        return null;
    }
}
