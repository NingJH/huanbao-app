package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.DeviceMapper;
import com.cqndt.huanbao.app.pojo.Device;
import com.cqndt.huanbao.app.service.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class DeviceServiceImpl implements DeviceService {
@Autowired
    DeviceMapper deviceMapper;
    @Override
    public List<Device> getParkDeviceList(Integer parkId, Integer regionId, Integer userId) {
        return deviceMapper.getParkDeviceList(parkId,regionId,userId);
    }

    @Override
    public List<Device> getCompanyDeviceList(Integer parkId, Integer regionId, Integer userId) {
        return deviceMapper.getCompanyDeviceList(parkId,regionId,userId);
    }
}
