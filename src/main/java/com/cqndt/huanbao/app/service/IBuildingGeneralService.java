package com.cqndt.huanbao.app.service;


import com.cqndt.huanbao.app.pojo.BuildingGeneral;
import com.cqndt.huanbao.app.pojo.BuildingGeneralAccount;
import com.cqndt.huanbao.app.pojo.BuildingGeneralGo;
import com.cqndt.huanbao.app.pojo.BuildingGeneralMeasure;

import java.util.List;
import java.util.Map;

/**
 * @Time : 2019-09-19 19:09
 **/
public interface IBuildingGeneralService {


    /**
     * 查询工业固废暂存场
     * @param id
     * @return
     */
    BuildingGeneral selectGeneral(Integer id);

    /**
     * 查询一般工业固废去向
     * @param buildingGeneralGo
     * @return
     */
    List<BuildingGeneralGo> queryGeneralGo(BuildingGeneralGo buildingGeneralGo);

    List<BuildingGeneralMeasure> selectGeneralMeasure(Integer generalId);

    List<BuildingGeneralAccount> selectGeneralAccount(BuildingGeneralAccount buildingGeneralAccount);
}
