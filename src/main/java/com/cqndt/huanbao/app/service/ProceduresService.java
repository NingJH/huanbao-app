package com.cqndt.huanbao.app.service;


import java.util.Map;

public interface ProceduresService {

    Map<String,Object> selectMeasureReport(Integer parkId, Integer companyId, Integer type,Integer regionId);
}
