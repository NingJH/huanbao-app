package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbCompanyMapper;
import com.cqndt.huanbao.app.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private HbCompanyMapper hbCompanyMapper;

    @Override
    public Map<String, Object> sortCompanyType() {
        Map<String, Object> map = new HashMap<>();
        //第一级菜单
        List<Map<String, Object>> fristCompanyTypes = hbCompanyMapper.CompanyType(0);
        fristCompanyTypes.remove("重点与一般企业");
        for (Map<String, Object> fristmap : fristCompanyTypes) {
            Integer fristId = Integer.valueOf(fristmap.get("id").toString());
            //第二级菜单
            List<Map<String, Object>> secondCompanyTypes = hbCompanyMapper.CompanyType(fristId);
            for (Map<String, Object> secondmap : secondCompanyTypes) {
                Integer secondId = Integer.valueOf(secondmap.get("id").toString());
                //第三级菜单
                List<Map<String, Object>> thirdCompanyTypes = hbCompanyMapper.CompanyType(secondId);
                for (Map<String, Object> thirdmap : thirdCompanyTypes) {
                    Integer thirdmapId = Integer.valueOf(thirdmap.get("id").toString());
                    //第四级菜单
                    List<Map<String, Object>> forthCompanyTypes = hbCompanyMapper.CompanyType(thirdmapId);
                    for (Map<String, Object> forthmap : forthCompanyTypes) {
                    }
                    thirdmap.put("forth", forthCompanyTypes);
                    thirdmap.put("isCheck", false);
                }
                secondmap.put("third", thirdCompanyTypes);
                secondmap.put("isCheck", false);
            }
            fristmap.put("second", secondCompanyTypes);
        }
        map.put("menu", fristCompanyTypes);
        return map;
    }

    @Override
    public Map<String, Object> listCompanyTypeInfo() {
        Map<String, Object> map = new HashMap<>();
        List<Map<String, Object>> fristCompanyTypes = hbCompanyMapper.listImpCompanyType();
        for (Map<String, Object> map1 : fristCompanyTypes) {
            Integer parentId = Integer.valueOf(map1.get("id").toString());
            List<Map<String, Object>> secondCompanyTypes = hbCompanyMapper.CompanyTypeInfo(parentId);
            map1.put("second", secondCompanyTypes);
        }
        map.put("menu", fristCompanyTypes);
        return map;
    }

    @Override
    public List<Map<String,String>> selectCompanyList(Integer id,String context,Integer regionId,Integer userId) {
        List<Map<String,String>> map=hbCompanyMapper.companyList(id,context,regionId,userId);
        return map;
    }

    @Override
    public List<Map<String, Object>> getCompanyLonLat(Integer parkId, Integer regionId,int userId) {
        return hbCompanyMapper.getCompanyLonLat(parkId,regionId,userId);
    }
}
