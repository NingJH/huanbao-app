package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbFacilitiesEnclosureMapper;
import com.cqndt.huanbao.app.pojo.HbFacilities;
import com.cqndt.huanbao.app.pojo.HbFacilitiesDetail;
import com.cqndt.huanbao.app.service.FacilitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class FacilitiesServiceImpl implements FacilitiesService {

    @Autowired
    HbFacilitiesEnclosureMapper hbFacilitiesEnclosureMapper;
    @Override
    public List<Map<String,Object>> selectFacilitiesList(Integer id, String context,Integer regionId) {
        //查询设备列表
        List<Map<String,Object>> hbFacilities=hbFacilitiesEnclosureMapper.queryFacilitiesList(id,context,regionId);
        for (Map<String,Object> map: hbFacilities) {
            Integer fid=Integer.parseInt(String.valueOf(map.get("id")));
            List<Map<String,Object>> list=hbFacilitiesEnclosureMapper.queryHbFacilitiesDetail(fid);
            map.put("detail",list);
        }
        return hbFacilities;
    }

//    @Override
//    public List<HbFacilitiesDetail> selectFacilitiesDetail(Integer id) {
//        return hbFacilitiesEnclosureMapper.queryHbFacilitiesDetail(id);
//    }
//
//    @Override
//    public HbFacilities selectFacilitiesinfo(Integer id) {
//        return hbFacilitiesEnclosureMapper.queryFacilitiesInfo(id);
//    }
}
