package com.cqndt.huanbao.app.service;

import com.cqndt.huanbao.app.pojo.BuildingRainSewageConverterPdf;

import java.util.List;
import java.util.Map;

public interface IBuildingRainSewageConverterService {
    /**
     * 查询雨污管网台账
     * @return
     */
    List<BuildingRainSewageConverterPdf> selectRainSewageConverterAccount(BuildingRainSewageConverterPdf buildingRainSewageConverterPdf);
}
