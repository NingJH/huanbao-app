package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.BuildingFocusEnergyMapper;
import com.cqndt.huanbao.app.pojo.BuildingFocusEnergyImg;
import com.cqndt.huanbao.app.pojo.BuildingPowerCompany;
import com.cqndt.huanbao.app.service.BuildingFocusEnergyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class BuildingFocusEnergyServiceImpl implements BuildingFocusEnergyService {
    @Autowired
    private BuildingFocusEnergyMapper buildingFocusEnergyMapper;

    @Override
    public List<Map<String, Object>> selectRainFocusEnergyProcess(Integer feId) {
        return buildingFocusEnergyMapper.selectRainFocusEnergyProcess(feId);
    }

    @Override
    public List<Map<String, Object>> selectRainFocusEnergyOutIn(BuildingPowerCompany buildingPowerCompany) {
        return buildingFocusEnergyMapper.selectRainFocusEnergyOutIn(buildingPowerCompany);
    }

}
