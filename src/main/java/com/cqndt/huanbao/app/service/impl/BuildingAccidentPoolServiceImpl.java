package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbBuildingAccidentPoolMapper;
import com.cqndt.huanbao.app.pojo.BuildingAccidentPool;
import com.cqndt.huanbao.app.pojo.BuildingAccidentPoolAnnal;
import com.cqndt.huanbao.app.service.IBuildingAccidentPoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Time : 2019-09-19 15:30
 **/
@Service
public class BuildingAccidentPoolServiceImpl implements IBuildingAccidentPoolService {


    @Autowired
    HbBuildingAccidentPoolMapper hbBuildingAccidentPoolMapper;


    @Override
    public List<BuildingAccidentPool> selectAccidentPool(Integer id) {

        return hbBuildingAccidentPoolMapper.selectAccidentPool(id);
    }

    @Override
    public List<BuildingAccidentPoolAnnal> selectAccidentPoolAnnl(BuildingAccidentPoolAnnal buildingAccidentPoolAnnal) {
        return hbBuildingAccidentPoolMapper.selectAccidentPoolAnnl(buildingAccidentPoolAnnal);
    }


}
