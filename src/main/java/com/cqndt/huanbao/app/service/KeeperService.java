package com.cqndt.huanbao.app.service;

import com.cqndt.huanbao.app.pojo.HbKeeper;

import java.util.List;
import java.util.Map;

public interface KeeperService {

        List<HbKeeper> listKeeper(String name);

        List<Map<String, Object>> selectRecordList(Integer companyId, Integer userId, Integer type);

        List<Map<String, Object>> selectMeasureDetail(Integer parkId, Integer measureId, Integer type,Integer regionId);

        List<Map<String, Object>> selectMeasureList(Integer parkId,String context,Integer regionId,Integer type);

        /**
         * type    1手续；3合同；
         * status -1未预告警；1预警；2告警
         * @param parkId
         * @return
         */
        List<Map<String,Object>> selectWarnAlarmList(Integer parkId, String context,Integer userId);

        //hpe表的id
        List<Map<String,Object>> selectWarnAlarmDetail(Integer id);

        int addKeeperLonlat(String lon, String lat,Integer userId);

        List<Map<String,Object>> getsgAlarmList(String name, Integer userId);

}
