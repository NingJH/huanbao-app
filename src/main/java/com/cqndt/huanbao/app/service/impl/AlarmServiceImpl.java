package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.AlarmMapper;
import com.cqndt.huanbao.app.dao.HbIOtherAlarmRecordsMapper;
import com.cqndt.huanbao.app.pojo.*;
import com.cqndt.huanbao.app.service.AlarmService;
import com.cqndt.huanbao.app.util.AliPushUtil;
import com.cqndt.huanbao.app.util.JsonDateValueProcessor;
import com.cqndt.huanbao.app.webSokect.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-09-06 10:19
 **/
@Service
@Slf4j
public class AlarmServiceImpl implements AlarmService {

    @Autowired
    AlarmMapper alarmMapper;
    @Autowired
    private HbIOtherAlarmRecordsMapper otherAlarmRecordsMapper;
    @Autowired
    private WebSocketServer webSocketServer;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    @Transactional
    public int alarmPush(String ids,String lon, String lat, String alarmName, String content, Integer level, String remakes,Integer parkId,Integer regionId,Integer userId) {
        System.out.println("ids>>>>>>>>>>>>>>>>>>>>>>"+ids);
        System.out.println("lon>>>>>>>>>>>>>>>>>>>>>"+lat);
        System.out.println("lat>>>>>>>>>>>>>>>>>>>>>>>"+lat);
        System.out.println("alarmName>>>>>>>>>>>>>>>>>"+alarmName);
        System.out.println("content>>>>>>>>>>>>>>>>>"+content);
        System.out.println("level>>>>>>>>>>>>>>>>>>>>>>"+level);
        System.out.println("parkId>>>>>>>>>>>>>>>>>>>>"+parkId);
        System.out.println("regionId>>>>>>>>>>>>>>>>>>>>"+regionId);
        System.out.println("userId>>>>>>>>>>>>>>>>>>>>"+userId);
        //根据选择查询报警推送对象
        List<HbUser> listSelect = new ArrayList<>();
        String levelName = null;
        switch (level){
            case 2:
                levelName = "一级报警(区县级)";
                break;
            case 3://二级报警(园区级)
                levelName = "二级报警(园区级)";
//                String[] attr = null;
//                //如果选择全部（默认）就是查整个园区的企业关联账户推送，如果选择分区，就查询分区下面的企业关联账户进行推送。
//                //加上配置用户
//                if(null != ids && !("").equals(ids)){
//                    attr = ids.split(",");
//                    if(attr.length<=0){
//                        break;
//                    }
//                    listSelect = alarmMapper.selectWarmObject(parkId,attr,null,0);
//                }else{
//                    if(null !=regionId && regionId!=0 && regionId!=-1){
//                        attr = regionId.toString().split(",");
//                    }
//                    listSelect = alarmMapper.selectWarmObject(parkId,attr,null,0);
//                }
                break;
            case 4://三级报警(企业级)
                levelName = "三级报警(企业级)";
                //如果没有选择企业，就是推送后台配置的用户，如果选择一家或者多家企业，就推送对应的用户加后台配置用户
                if(null != ids && !("").equals(ids)){
                    StringBuffer userIds = new StringBuffer();
                    String[] comIds = ids.split(",");
                    if(comIds.length<=0){
                        break;
                    }
                    List<HbCompany> comList = alarmMapper.selectWarmCompany(null,null,comIds,userId);
                    for (HbCompany company:comList) {
                        userIds = userIds.append(","+company.getAlarmUserId());
                    }
                    String[] alarmUserIds = userIds.toString().split(",");
                    listSelect = alarmMapper.selectWarmObject(null,null,alarmUserIds,0);
                }
                break;
            case 1:
                levelName = "特级报警(省市级)";
                break;
        }
        //根据level报警等级查找该等级下需要通知的所有用户(配置用户)
        List<HbUser> users = alarmMapper.getUserByAlarmLevelId(level);
        if(!listSelect.isEmpty()){
            users.addAll(listSelect);
        }
        //用户去重
        for (int i = 0; i < users.size() - 1; i++) {
            for (int j = users.size() - 1; j > i; j--) {
                if (users.get(j).getId() == users.get(i).getId()) {
                    users.remove(j);
                }
            }
        }

        Alarm alarm = new Alarm();
        Date today = new Date();
        alarm.setLon(lon);
        alarm.setLat(lat);
        alarm.setName(alarmName);
        alarm.setContent(content);
        alarm.setLevel(level);
        alarm.setRemakes(remakes);
        alarm.setAlarmDate(today);
        alarm.setAddDate(today);
        alarm.setDel(0);
        alarm.setPushType(1);//推送1，表示预警
        alarm.setUserId(0);
        alarm.setParkId(parkId);
        alarm.setRegionId(regionId);
        alarm.setIsRead(0);
        alarmMapper.saveAlarm(alarm);
        //如果有时间，将时间格式转为yyyy-MM-dd
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor("yyy-MM-dd HH:mm:ss"));
        String uuid = UUID.randomUUID().toString().replaceAll("\\-", "");
        for(HbUser user : users){
            System.out.println("一键告警推送的推送用户phone+>>>>>>>>>>>>>>>>>>>>"+ user.getPhone());
            System.out.println("一键告警推送的推送用户id+>>>>>>>>>>>>>>>>>>>>"+  user.getId());
            System.out.println("一键告警推送的推送用户type+>>>>>>>>>>>>>>>>>>>>"+ user.getType());
            //创建32为随机数
            //添加推送信息记录
            OtherAlarmRecords otherAlarmRecords = null;
            if (StringUtils.isEmpty(regionId)&&regionId!=-1){
                otherAlarmRecords = new OtherAlarmRecords(alarm,parkId);
            } else{
                otherAlarmRecords = new OtherAlarmRecords(alarm,parkId,regionId);
            }
            otherAlarmRecords.setPhone(user.getPhone());
            //添加随机数，以便统计数量分组
            otherAlarmRecords.setUuid(uuid);
            otherAlarmRecords.setUserId(user.getId());
            otherAlarmRecordsMapper.saveOtherAlarmRecords(otherAlarmRecords);
            JSONObject json = new JSONObject();
            json = json.fromObject(otherAlarmRecords, jsonConfig);
            switch (user.getType()){
                case 1:
                    //推送给web端
                    try {
                        System.out.println("进入app一键告警websocet推送前>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        webSocketServer.sendInfo(user.getId(),json.toString());
                        System.out.println("进入app一键告警websocet推送后>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                    } catch (IOException e) {
                        System.out.println("websocket推送失败");
                        e.printStackTrace();
                    }
                    break;
                case 3:
                    //推送给手机端
                    if (stringRedisTemplate.hasKey("jwt_token:" + user.getUsername() + user.getType() + ":token")) {
                        String oldToken = stringRedisTemplate.opsForValue().get("jwt_token:" + user.getUsername() + user.getType() + ":token");
                        try {
                            oldToken = oldToken + ":secret";
                            oldToken = oldToken.substring(50,100);
                            System.out.println("进入app一键告警阿里推送前>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                            AliPushUtil.AdvancedPush("事故告警", "告警名称:"+alarmName+"  告警等级："+levelName+" 告警时间："+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(today)+"  经纬度："+lon+","+lat+" 内容： "+content, json, oldToken);
                            System.out.println("进入app一键告警推阿里送后>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        } catch (Exception e) {
                            System.out.println("阿里推送失败");
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }
        return 1;
    }

    @Override
    public List<ParkRegion> selectWarmRegion(Integer parkId, Integer userId){
        return alarmMapper.selectWarmRegion(parkId,userId);
    }


    @Override
    public List<HbCompany> selectWarmCompany(Integer parkId, Integer regionId, Integer userId){
        return alarmMapper.selectWarmCompany(parkId,regionId,null,userId);
    }
}
