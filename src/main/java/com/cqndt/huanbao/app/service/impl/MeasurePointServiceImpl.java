package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbMeasurePointMapper;
import com.cqndt.huanbao.app.service.MeasurePointService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-31 15:32
 **/
@Service
@Slf4j
public class MeasurePointServiceImpl implements MeasurePointService {

    @Autowired
    private HbMeasurePointMapper hbMeasurePointMapper;

    @Override
    public List<Map<String, Object>> getmeasurePointLonLat(Integer parkId,Integer regionId,Integer measurePointType,int userId) {
        return hbMeasurePointMapper.getmeasurePointLonLat(parkId,regionId,measurePointType,userId);
    }

    @Override
    public List<Map<String, Object>> measureStatisticsRank(Integer parkId,Integer regionId,Integer type,int userId) {
        log.info("-------------------测点-------------------");
        switch (type){
            case 1:
                //大气环境
                List<Map<String,Object>> rank1 = measurePoint1(parkId,regionId,userId);
                return rank1;
            case 2:
                //地表水环境
                List<Map<String,Object>> rank2 = measurePoint2(parkId,regionId,userId);
                return rank2;
            case 3:
                //声环境
                List<Map<String,Object>> rank3 = measurePoint3(parkId,regionId,userId);
                return rank3;
            case 4:
                //地下水环境
                List<Map<String,Object>> rank4 = measurePoint4(parkId,regionId,userId);
                return rank4;
            case 5:
                //土壤环境
                List<Map<String,Object>> rank5 = measurePoint5(parkId,regionId,userId);
                return rank5;
        }
        return null;
    }

    //大气环境
    public List<Map<String,Object>> measurePoint1(Integer parkId,Integer regionId,int userId) {
        List<Map<String,Object>> resultList = new ArrayList<>();//总返回值
        //二氧化硫
        List<Map<String,Object>> so2List = hbMeasurePointMapper.measurePoint(parkId,regionId,1,3);
        String so2Top = hbMeasurePointMapper.measurePointTop(parkId,regionId,1,3);
        resultList.add(test1(so2List,so2Top,"二氧化硫"));
        //二氧化氮
        List<Map<String,Object>> no2List = hbMeasurePointMapper.measurePoint(parkId,regionId,1,4);
        String no2Top = hbMeasurePointMapper.measurePointTop(parkId,regionId,1,4);
        resultList.add(test1(no2List,no2Top,"二氧化氮"));
        //可吸入颗粒物（PM10）
        List<Map<String,Object>> pm10List = hbMeasurePointMapper.measurePoint(parkId,regionId,1,6);
        String pm10Top = hbMeasurePointMapper.measurePointTop(parkId,regionId,1,6);
        resultList.add(test1(pm10List,pm10Top,"可吸入颗粒物（PM10）"));
        return resultList;
    }
    //地表水环境
    public List<Map<String,Object>> measurePoint2(Integer parkId,Integer regionId,int userId) {
        List<Map<String,Object>> resultList = new ArrayList<>();//总返回值
        //ph
        List<Map<String,Object>> phList = hbMeasurePointMapper.measurePoint(parkId,regionId,2,28);
        String phTop = hbMeasurePointMapper.measurePointTop(parkId,regionId,2,28);
        resultList.add(test1(phList,phTop,"ph"));
        //COD
        List<Map<String,Object>> codList = hbMeasurePointMapper.measurePoint(parkId,regionId,2,29);
        String codTop = hbMeasurePointMapper.measurePointTop(parkId,regionId,2,29);
        resultList.add(test1(codList,codTop,"COD"));
        //BOD5
        List<Map<String,Object>> bod5List = hbMeasurePointMapper.measurePoint(parkId,regionId,2,30);
        String bod5Top = hbMeasurePointMapper.measurePointTop(parkId,regionId,2,30);
        resultList.add(test1(bod5List,bod5Top,"BOD5"));
        return resultList;
    }
    //声环境
    public List<Map<String,Object>> measurePoint3(Integer parkId,Integer regionId,int userId) {
        List<Map<String,Object>> resultList = new ArrayList<>();//总返回值
        //噪音
        List<Map<String,Object>> dBList = hbMeasurePointMapper.measurePoint(parkId,regionId,3,36);
        String dBTop = hbMeasurePointMapper.measurePointTop(parkId,regionId,3,36);
        resultList.add(test1(dBList,dBTop,"噪音"));
        return resultList;
    }
    //地下水环境
    public List<Map<String,Object>> measurePoint4(Integer parkId,Integer regionId,int userId) {
        List<Map<String,Object>> resultList = new ArrayList<>();//总返回值
        //K+
        List<Map<String,Object>> kList = hbMeasurePointMapper.measurePoint(parkId,regionId,4,32);
        String kTop = hbMeasurePointMapper.measurePointTop(parkId,regionId,4,32);
        resultList.add(test1(kList,kTop,"K+"));
        //Na+
        List<Map<String,Object>> na2List = hbMeasurePointMapper.measurePoint(parkId,regionId,4,33);
        String na2Top = hbMeasurePointMapper.measurePointTop(parkId,regionId,4,33);
        resultList.add(test1(na2List,na2Top,"Na+"));
        return resultList;
    }
    //土壤环境
    public List<Map<String,Object>> measurePoint5(Integer parkId,Integer regionId,int userId) {
        List<Map<String,Object>> resultList = new ArrayList<>();//总返回值
        //pH
        List<Map<String,Object>> pHList = hbMeasurePointMapper.measurePoint(parkId,regionId,5,34);
        String pHTop = hbMeasurePointMapper.measurePointTop(parkId,regionId,5,34);
        resultList.add(test1(pHList,pHTop,"pH"));
        //二氧化氮
        List<Map<String,Object>> no2List = hbMeasurePointMapper.measurePoint(parkId,regionId,5,35);
        String no2Top = hbMeasurePointMapper.measurePointTop(parkId,regionId,5,35);
        resultList.add(test1(no2List,no2Top,"二氧化氮"));
        return resultList;
    }

    private Map<String,Object> test1(List<Map<String,Object>> list,String top,String name){
        Map<String,Object> map = new HashMap<>();
        List<Map<String,Object>> data = new ArrayList<>();
        for(Map<String,Object> so2 : list){
            Map<String,Object> val = new HashMap<>();
            val.put("value",so2.get("value"));
            val.put("date",so2.get("date"));
            map.put("name",name);
            map.put("isChoiced",false);
            map.put("top",top);
            data.add(val);
        }
        map.put("result",data);
        return map;
    }
}
