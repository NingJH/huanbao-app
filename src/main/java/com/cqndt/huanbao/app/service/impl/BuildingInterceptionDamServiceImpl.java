package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbBuildingInterceptionDamMapper;
import com.cqndt.huanbao.app.pojo.BuildingInterceptionDam;
import com.cqndt.huanbao.app.service.IBuildingInterceptionDamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Time : 2019-09-18 09:25
 **/
@Service
public class BuildingInterceptionDamServiceImpl implements IBuildingInterceptionDamService {


    @Autowired
    HbBuildingInterceptionDamMapper buildingInterceptionDamMapper;




    @Override
    public List<BuildingInterceptionDam> selectInterceptionDam(Integer id) {
        return buildingInterceptionDamMapper.selectInterceptionDamById(id);
    }

}
