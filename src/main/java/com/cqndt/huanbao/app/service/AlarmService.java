package com.cqndt.huanbao.app.service;

import com.cqndt.huanbao.app.pojo.HbCompany;
import com.cqndt.huanbao.app.pojo.OtherAlarmRecords;
import com.cqndt.huanbao.app.pojo.ParkRegion;

import java.util.List;
import java.util.Map;

public interface AlarmService {
    List<ParkRegion> selectWarmRegion(Integer parkId, Integer userId);

    List<HbCompany> selectWarmCompany(Integer parkId, Integer regionId, Integer userId);
    int alarmPush(String ids,String lon, String lat, String alarmName, String content, Integer level, String remakes,Integer parkId, Integer regionId,Integer userId);

}
