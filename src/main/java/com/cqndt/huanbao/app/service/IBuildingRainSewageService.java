package com.cqndt.huanbao.app.service;

import java.util.List;
import java.util.Map;

public interface IBuildingRainSewageService {

    List<Map<String,Object>> selectRainSewagePdf(Integer rsId);
}
