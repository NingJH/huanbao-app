package com.cqndt.huanbao.app.service;

import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-30 15:27
 **/
public interface StatisticsService {

    List<Map<String,Object>> companyStatisticsRank(Integer type, Integer companyId, int userId);

    List<Map<String,Object>> statisticsRank(Integer parkId, Integer regionId,Integer type, int userId);

    List<Map<String,Object>> statisticsRank1(Integer parkId, Integer regionId,Integer type, int userId);
}
