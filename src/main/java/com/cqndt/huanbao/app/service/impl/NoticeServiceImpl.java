package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbNoticeMapper;
import com.cqndt.huanbao.app.pojo.HbNotice;
import com.cqndt.huanbao.app.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-29 11:22
 **/
@Service
public class NoticeServiceImpl implements NoticeService {

    @Autowired
    HbNoticeMapper hbNoticeMapper;

    @Override
    public List<HbNotice> selectNotice(int userId) {
        return hbNoticeMapper.selectNotice(userId);
    }

}
