package com.cqndt.huanbao.app.service;

import java.util.List;
import java.util.Map;

public interface BuildingSewageTreatmenPlantService {

    Map<String,Object> queryPlants(Integer buildId);

    List<Map<String,Object>> echar(Integer parkId,String deviceNo);

}
