package com.cqndt.huanbao.app.service;


import com.cqndt.huanbao.app.pojo.BuildingPowerCompany;

import java.util.List;

public interface BuildingPowerCompanyService {


    List<BuildingPowerCompany> companyList(BuildingPowerCompany buildingPowerCompany);

}
