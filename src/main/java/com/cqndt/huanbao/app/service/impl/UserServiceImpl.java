package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.*;
import com.cqndt.huanbao.app.exception.ErrorType;
import com.cqndt.huanbao.app.exception.ServiceException;
import com.cqndt.huanbao.app.pojo.*;
import com.cqndt.huanbao.app.service.UserService;
import com.cqndt.huanbao.app.util.AliPushUtil;
import com.cqndt.huanbao.app.util.SHA1Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Value("${jwt.salt}")
    private String salt;
    @Value("${jwt.issuer}")
    private String issuer;
    @Value("${jwt.audience}")
    private String audienceSalt;
    @Value("${jwt.TTLMillis}")
    private long TTLMillis;

    @Autowired
    private HbUserMapper userMapper;

    @Autowired
    private MenuMapper menuMapper;

//    @Autowired
//    private RedisUtils redisUtils;

    @Autowired
    private HbUserCompanyMapper userCompanyMapper;

    @Autowired
    private HbRoleMapper hbRoleMapper;

    @Autowired
    private HbRoleFunctionMapper hbRoleFunctionMapper;

    @Autowired
    private HbFunctionMapper hbFunctionMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;



//    @Override
//    public Map<String, Object> getUserInfo(String username, String password) {
//        Map<String, Object> map = null;
//        Map<String, Object> userInfo = userMapper.getUserInfo(username, password);
//        if (!StringUtils.isEmpty(userInfo)) {
//            Integer userId = Integer.parseInt(userInfo.get("id").toString());
//            int id = userMapper.getRoleByUserId((Integer) userInfo.get("id"));
//            List<Integer> list = menuMapper.getMenuByUserNotHave(id);
//            log.info("查询出来的权限   : {}",list);
//            map = new HashMap<>();
//            String token = ShiroUtils.sha256("token" + userInfo.get("id").toString() + userInfo.get("username").toString(), RandomStringUtils.randomAlphanumeric(20));
//            String a = null;
//            try{
//                a = redisUtils.get("token" + userInfo.get("id").toString());
//            }catch (NullPointerException e){
//                a = "";
//            }
//            if(!StringUtils.isEmpty(a)){
//                if(!a.equals(token)){
//                    //把a推送给app
////                    AliPushUtil.pushNoticeByAccount("标题","正文");
//                }
//            }
//            Role role = hbRoleMapper.getRoleByUserId(Integer.parseInt(userInfo.get("id").toString()));
//
//            map.put("menu",appendMenu(role.getId()));
//            map.put("role",role);
//            map.put("token", token);
//            map.put("userInfo", userInfo);
////            if (list.size() >0){
////                StringBuilder have = new StringBuilder();
////                for (Integer i:list){
////                    have.append(i).append(",");
////                }
////                log.info("查询出来的权限   : {}",have);
////                map.put("have",have.substring(0,have.length()-1));
////            }else {
////                map.put("have","");
////            }
//            redisUtils.set("token" + userInfo.get("id").toString(), token, 86400);
//
//
//
//            //添加用户绑定园区
//            //查询该用户下的所有分区
//            List<Map<String,Object>> hbUserCompanyList =  userCompanyMapper.selectRegionIdByUserId(Integer.parseInt(userInfo.get("id").toString()));
//            Set<Integer> parkIdList = new HashSet<>();
//            for(Map<String,Object> hbUserCompany : hbUserCompanyList){
//                parkIdList.add(userCompanyMapper.selectParkByRegionId(userId,Integer.parseInt(hbUserCompany.get("region_id").toString())));
//            }
//            List<Map<String,Object>> parkList = new ArrayList<>();
//            for(Integer parkId : parkIdList){
//                Map<String,Object> parkName = new HashMap<>();
//                //园区id和名称
//                List<Map<String,Object>> parkIN = userCompanyMapper.getParkNameByid(parkId);
//                for(Map<String,Object> park : parkIN){
//                    parkName.put("parkName",park.get("name"));
//                    parkName.put("parkId",park.get("id"));
//                }
//                List<Map<String,Object>> regionName = new ArrayList<>();
//                for(Map<String,Object> map2 : hbUserCompanyList){
//                    Map<String,Object> regionId = new HashMap<>();
//                    List<Map<String,Object>> regionList = userCompanyMapper.selectRegion(userId,parkId,Integer.parseInt(map2.get("region_id").toString()));
//                    for(Map<String,Object> region : regionList){
//                        if(!region.isEmpty()){
//                            regionId.put("regionId",region.get("id"));
//                            regionId.put("regionName",region.get("region_name"));
//                            regionName.add(regionId);
//                        }
//                    }
//                }
//                parkName.put("value",regionName);
//                parkList.add(parkName);
//            }
//            userInfo.put("park",parkList);
//        }
//        return map;
//    }

    private List<Integer> appendMenu(int roleId) {
        List<Integer> parentMenu = hbRoleFunctionMapper.listParentIdsByRoleId(roleId);
        log.info("一级菜单" + parentMenu);
        return parentMenu;
    }

    @Override
    public JwtToken getUserByUsernameAndPassword(HbUser user, HttpServletRequest request, HttpServletResponse response) {
        HbUser u = userMapper.getUserByUsernameAndPassword(user);
        if (u == null) {
            throw new ServiceException(ErrorType.USER_OR_PASSWORD_ERROR);
        }
        if (u.getIsEnable() == 1){
            throw new ServiceException(ErrorType.USER_NOT_ENABLE_ERROR);
        }
        user = u;
        Role role = hbRoleMapper.getRoleByUserId(u.getId());
        if (role == null) {
            throw new ServiceException(ErrorType.ROLE_NOT_EXIST);
        }
        u.setRole(role);
        log.info("登录的用户信息{}", u);
        return createToken(request,u);
    }

    private JwtToken createToken(HttpServletRequest request, HbUser u) {
        //设置audience
        Audience audience = setAudience(u);
        JwtToken jwtToken = JwtHelper.createJWT(String.valueOf(u.getId()), String.valueOf(u.getRole().getId()),request.getSession().getId(),u.getUsername(), audience);
        jwtToken.getExpireTime();
        System.out.println("设置前过期时间"+jwtToken.getExpireTime());
        jwtToken.setExpireTime(System.currentTimeMillis() + 60*1000);
        System.out.println("设置后的过期时间"+jwtToken.getExpireTime());
        String token = jwtToken.getToken();
        token = "bearer;" + token;
        //设置jwtToken值
        setJwtToken(jwtToken, u, token);
        //存入redis
        toStringRedisTemplate(u, token, audience.getBase64Secret());
        return jwtToken;
    }

    private Audience setAudience(HbUser u) {
        Audience audience = new Audience();
        audience.setIssuer(issuer);
        String base64Secret = SHA1Util.getSha1(u.getUsername() + salt);
        audience.setBase64Secret(base64Secret);
        audience.setTTLMillis(TTLMillis);
        audience.setAudience(SHA1Util.getSha1(String.valueOf(u.getId()) + audienceSalt));
        return audience;
    }

    private void setJwtToken(JwtToken jwtToken, HbUser u, String token) {
        //添加用户绑定园区
        //查询该用户下的所有分区
        List<Map<String,Object>> hbUserCompanyList =  userCompanyMapper.selectRegionIdByUserId(u.getId());
        Set<Integer> parkIdList = new HashSet<>();
        for(Map<String,Object> hbUserCompany : hbUserCompanyList){
            parkIdList.add(userCompanyMapper.selectParkByRegionId(u.getId(),Integer.parseInt(hbUserCompany.get("region_id").toString())));
        }
        List<Map<String,Object>> parkList = new ArrayList<>();
        for(Integer parkId : parkIdList){
            Map<String,Object> parkName = new HashMap<>();
            //园区id和名称
            List<Map<String,Object>> parkIN = userCompanyMapper.getParkNameByid(parkId);
            for(Map<String,Object> park : parkIN){
                parkName.put("parkName",park.get("name"));
                parkName.put("lonlat",park.get("lat_lon"));
                parkName.put("parkId",park.get("id"));
            }
            List<Map<String,Object>> regionName = new ArrayList<>();
            Map<String,Object> park = new HashMap<>();
            park.put("regionId",-1);
            park.put("regionName","全部");
            park.put("regionLonlat","");
            regionName.add(park);
            for(Map<String,Object> map2 : hbUserCompanyList){
                Map<String,Object> regionId = new HashMap<>();
                Map<String,Object> all = new HashMap<>();
                List<Map<String,Object>> regionList = userCompanyMapper.selectRegion(u.getId(),parkId,Integer.parseInt(map2.get("region_id").toString()));
                for(Map<String,Object> region : regionList){
                    if(!region.isEmpty()){
                        regionId.put("regionId",region.get("id"));
                        regionId.put("regionName",region.get("region_name"));
                        regionId.put("regionLonlat",region.get("lat_lon"));
                        regionName.add(regionId);
                    }
                }
            }
            if(!regionName.isEmpty()){
                parkName.put("value",regionName);
                parkList.add(parkName);
            }
        }
        jwtToken.setPark(parkList);

        jwtToken.setUserId(u.getId());
        jwtToken.setName(u.getName());
        jwtToken.setToken(token);
        jwtToken.setRole(u.getRole());
        jwtToken.setPhone(u.getPhone());
        jwtToken.setMenus(appendMenu(u.getRole().getId()));
        List<Integer> gisMenus = new ArrayList<>();
        gisMenus.add(1);
        gisMenus.add(2);
        gisMenus.add(3);
        gisMenus.add(4);
        gisMenus.add(5);
        jwtToken.setGisMenus(gisMenus);
//        jwtToken.setFunctions(hbFunctionMapper.listFunctionByRoleId(u.getRole().getId()));
    }

    private void toStringRedisTemplate(HbUser u, String token, String base64Secret) {
        if (stringRedisTemplate.hasKey("jwt_token:" + u.getUsername() + u.getType() + ":token")) {
            String oldToken = stringRedisTemplate.opsForValue().get("jwt_token:" + u.getUsername() + u.getType() + ":token");

            if (stringRedisTemplate.hasKey("jwt_token:" + oldToken + ":secret")) {
                stringRedisTemplate.delete("jwt_token:" + oldToken + ":secret");
                try {
                    oldToken = oldToken + ":secret";
                    oldToken = oldToken.substring(50,100);
                    AliPushUtil.AdvancedPush("您的账号已在其他地方登录","您的账号已在其他地方登录",null,  oldToken);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        stringRedisTemplate.opsForValue().set("jwt_token:" + u.getUsername() + u.getType() + ":token", token);
        stringRedisTemplate.opsForValue().set("jwt_token:" + token + ":secret", base64Secret);
        //保存token至数据库
        HbUserToken savetoken = userMapper.getByUserName(u.getUsername());
        if(null == savetoken){
            savetoken = new HbUserToken();
            savetoken.setPhone(u.getPhone());
            savetoken.setType(u.getType());
            savetoken.setUserName(u.getUsername());
            savetoken.setToken(token);
            userMapper.saveHbUserToken(savetoken);
        }else{
            savetoken.setToken(token);
            userMapper.upHbUserToken(savetoken);
        }
    }
}
