package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbSewageMapper;
import com.cqndt.huanbao.app.dao.HbStatisticsMapper;
import com.cqndt.huanbao.app.service.StatisticsService;
import com.cqndt.huanbao.app.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-30 15:29
 **/
@Service
@Slf4j
public class StatisticsServiceImpl implements StatisticsService {

    @Autowired
    HbSewageMapper hbSewageMapper;

    @Autowired
    HbStatisticsMapper hbStatisticsMapper;

    @Override
    public List<Map<String, Object>> statisticsRank(Integer parkId, Integer regionId,Integer type, int userId) {
        List yearList = new ArrayList<>();
        int year = DateUtil.getYear();//当前年份
        yearList.add(year+1);
        yearList.add(year);
        yearList.add(year-1);
        yearList.add(year-2);
        yearList.add(year-3);
        log.info("-------------------园区最近五次上传（废水、一般固废、危险固废）-------------------");
        switch (type){
            case 1:
                List<Map<String,Object>> rank1 = park1(parkId,regionId,yearList,"wasteWater-issue",userId);//废水排放量单位
                return  rank1;
            case 2:
                List<Map<String,Object>> rank2 = park2(parkId,regionId,yearList,"wasteWater-issue",userId);//废水排放量单位
                return rank2;
            case 3:
                List<Map<String,Object>> rank3 = park3(parkId,regionId,yearList,"flueGas-issue",userId);//废水排放量单位
                return rank3;
        }
        return null;
    }

    @Override
    public List<Map<String, Object>> statisticsRank1(Integer parkId, Integer regionId,Integer type, int userId) {
        List yearList = new ArrayList<>();
        int year = DateUtil.getYear();//当前年份
        yearList.add(year+1);
        yearList.add(year);
        yearList.add(year-1);
        yearList.add(year-2);
        yearList.add(year-3);
        log.info("-------------------园区最近五次上传（废水、一般固废、危险固废）-------------------");
        switch (type){
            case 1:
                List<Map<String,Object>> rank1 = parkCurve1(parkId,regionId,yearList,"wasteWater-issue",userId);//废水排放量单位
                return  rank1;
            case 2:
                List<Map<String,Object>> rank2 = parkCurve2(parkId,regionId,yearList,"wasteWater-issue",userId);//废水排放量单位
                return rank2;
            case 3:
                List<Map<String,Object>> rank3 = parkCurve3(parkId,regionId,yearList,"flueGas-issue",userId);//废水排放量单位
                return rank3;
        }
        return null;
    }

    //进5年某种排放类型数据(园区统计)
    private List<Map<String,Object>> park1(Integer parkId,Integer regionId,List<Integer> yearList,String typeCode ,int userId) {
        log.info("-------------------园区年度总排名-------------------");
        List<Map<String,Object>> resultList = new ArrayList<>();
        for(Integer year : yearList){
            List<Map<String,Object>> data = new ArrayList<>();
            Map<String,Object> resultMap = new HashMap<>();
            //单位
            String unit = hbSewageMapper.getItemName(typeCode);
            //废水值
            String fs = hbStatisticsMapper.rankSolidByYear1(parkId, regionId,year,userId);
            String fsTop = hbStatisticsMapper.rankSolidByYearTop1(parkId,regionId,1,yearList,userId);
            Map<String,Object> map = new HashMap<>();
            map.put("name","废水");
            map.put("top",fsTop);
            map.put("val",StringUtils.isEmpty(fs)?"0":fs);
            map.put("unit",unit);
            data.add(map);
            //一般固废
            String ybgf = hbStatisticsMapper.rankSolidByYear(parkId,regionId, 15,year,userId);
            String ybgfTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,15,yearList,userId);
            Map<String,Object> map1 = new HashMap<>();
            map1.put("name","一般固废");
            map1.put("top",StringUtils.isEmpty(ybgfTop)?"0":ybgfTop);
            map1.put("val",StringUtils.isEmpty(ybgf)?"0":ybgf);
            map1.put("unit",unit);
            data.add(map1);
            //危险固废
            String wxgf  = hbStatisticsMapper.rankSolidByYear(parkId,regionId,16,year,userId);
            String wxgffTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,16,yearList,userId);
            Map<String,Object> map2 = new HashMap<>();
            map2.put("name","危险固废");
            map2.put("top",StringUtils.isEmpty(wxgffTop)?"0":wxgffTop);
            map2.put("val",StringUtils.isEmpty(wxgf)?"0":wxgf);
            map2.put("unit",unit);
            data.add(map2);
            resultMap.put("year",year);
            resultMap.put("data",data);
            resultList.add(resultMap);
        }
        return resultList;
    }

    //进5年某种排放类型数据(园区曲线统计)
    private List<Map<String,Object>> parkCurve1(Integer parkId,Integer regionId,List<Integer> yearList,String typeCode ,int userId) {
        log.info("-------------------园区年度总排名曲线-------------------");
        List<Map<String,Object>> resultList = new ArrayList<>();//总返回值
        Map<String,Object> resultMap = new HashMap<>();//废水
        List<Map<String,Object>> data = new ArrayList<>();//废水
        Map<String,Object> resultMap1 = new HashMap<>();//一般固废
        List<Map<String,Object>> data1 = new ArrayList<>();//一般固废
        Map<String,Object> resultMap2 = new HashMap<>();//危险固废
        List<Map<String,Object>> data2 = new ArrayList<>();//危险固废
        for(Integer year : yearList){
            //单位
            String unit = hbSewageMapper.getItemName(typeCode);
            //废水值
            String fs = hbStatisticsMapper.rankSolidByYear1(parkId, regionId,year,userId);
            String fsTop = hbStatisticsMapper.rankSolidByYearTop1(parkId,regionId,1,yearList,userId);
            Map<String,Object> map = new HashMap<>();
            map.put("year",year);
            map.put("val",StringUtils.isEmpty(fs)?"0":fs);
            data.add(map);
            resultMap.put("name","废水");//废水
            resultMap.put("unit",unit);//废水
            resultMap.put("top",StringUtils.isEmpty(fsTop)?"0":fsTop);//废水
            //一般固废
            String ybgf = hbStatisticsMapper.rankSolidByYear(parkId,regionId, 15,year,userId);
            String ybgfTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,15,yearList,userId);
            Map<String,Object> map1 = new HashMap<>();
            map1.put("year",year);
            map1.put("val",StringUtils.isEmpty(ybgf)?"0":ybgf);
            data1.add(map1);
            resultMap1.put("name","一般固废");//一般固废
            resultMap1.put("unit",unit);//一般固废
            resultMap1.put("top",StringUtils.isEmpty(ybgfTop)?"0":ybgfTop);//一般固废
            //危险固废
            String wxgf  = hbStatisticsMapper.rankSolidByYear(parkId,regionId,16,year,userId);
            String wxgffTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,16,yearList,userId);
            Map<String,Object> map2 = new HashMap<>();
            map2.put("val",StringUtils.isEmpty(wxgf)?"0":wxgf);
            map2.put("year",year);
            data2.add(map2);
            resultMap2.put("name","危险固废");//危险固废
            resultMap2.put("unit",unit);//危险固废
            resultMap2.put("top",StringUtils.isEmpty(wxgffTop)?"0":wxgffTop);//危险固废
        }
        resultMap.put("data",data);//废水
        resultList.add(resultMap);//废水
        resultMap1.put("data",data1);//一般固废
        resultList.add(resultMap1);//一般固废
        resultMap2.put("data",data2);//危险固废
        resultList.add(resultMap2);//危险固废
        return resultList;
    }

    //进5年某种排放类型数据(园区统计)
    private List<Map<String,Object>> park2(Integer parkId,Integer regionId,List<Integer> yearList,String typeCode ,int userId) {
        log.info("-------------------园区年度废水指标-------------------");
        List<Map<String,Object>> resultList = new ArrayList<>();
        for(Integer year : yearList){
            List<Map<String,Object>> data = new ArrayList<>();
            Map<String,Object> resultMap = new HashMap<>();
            //单位
            String unit = hbSewageMapper.getItemName(typeCode);
            //cod
            String cod = hbStatisticsMapper.rankSolidByYear(parkId,regionId, 6,year,userId);
            String codTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,6,yearList,userId);
            Map<String,Object> map = new HashMap<>();
            map.put("name","cod");
            map.put("val",StringUtils.isEmpty(cod)?"0":cod);
            map.put("top",StringUtils.isEmpty(codTop)?"0":codTop);
            map.put("unit",unit);
            data.add(map);
            //tp
            String tp = hbStatisticsMapper.rankSolidByYear(parkId,regionId, 7,year,userId);
            String tpTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,7,yearList,userId);
            Map<String,Object> map1 = new HashMap<>();
            map1.put("name","tp");
            map1.put("val",StringUtils.isEmpty(tp)?"0":tp);
            map1.put("top",StringUtils.isEmpty(tpTop)?"0":tpTop);
            map1.put("unit",unit);
            data.add(map1);
            //NH3-N
            String nh  = hbStatisticsMapper.rankSolidByYear(parkId,regionId,8,year,userId);
            String nhTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,8,yearList,userId);
            Map<String,Object> map2 = new HashMap<>();
            map2.put("name","nh3-n");
            map2.put("val",StringUtils.isEmpty(nh)?"0":nh);
            map2.put("top",StringUtils.isEmpty(nhTop)?"0":nhTop);
            map2.put("unit",unit);
            data.add(map2);
            //TN
            String tn  = hbStatisticsMapper.rankSolidByYear(parkId,regionId,22,year,userId);
            String tnTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,22,yearList,userId);
            Map<String,Object> map3 = new HashMap<>();
            map3.put("name","tn");
            map3.put("val",StringUtils.isEmpty(tn)?"0":tn);
            map3.put("top",StringUtils.isEmpty(tnTop)?"0":tnTop);
            map3.put("unit",unit);
            data.add(map3);
            //重金属
            String zjs  = hbStatisticsMapper.rankSolidByYear(parkId,regionId,10,year,userId);
            String zjsTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,10,yearList,userId);
            Map<String,Object> map4 = new HashMap<>();
            map4.put("name","重金属");
            map4.put("val",StringUtils.isEmpty(zjs)?"0":zjs);
            map4.put("top",StringUtils.isEmpty(zjsTop)?"0":zjsTop);
            map4.put("unit",unit);
            data.add(map4);
            resultMap.put("year",year);
            resultMap.put("data",data);
            resultList.add(resultMap);
        }
        return resultList;
    }

    //进5年某种排放类型数据(园区曲线统计)
    private List<Map<String,Object>> parkCurve2(Integer parkId,Integer regionId,List<Integer> yearList,String typeCode ,int userId) {
        log.info("-------------------园区年度废水指标曲线-------------------");
        List<Map<String,Object>> resultList = new ArrayList<>();//总返回值
        Map<String,Object> resultMap = new HashMap<>();//cod
        List<Map<String,Object>> data = new ArrayList<>();//cod
        Map<String,Object> resultMap1 = new HashMap<>();//tp
        List<Map<String,Object>> data1 = new ArrayList<>();//tp
        Map<String,Object> resultMap2 = new HashMap<>();//NH3-N
        List<Map<String,Object>> data2 = new ArrayList<>();//NH3-N
        Map<String,Object> resultMap3 = new HashMap<>();//TN
        List<Map<String,Object>> data3 = new ArrayList<>();//TN
        Map<String,Object> resultMap4 = new HashMap<>();//重金属
        List<Map<String,Object>> data4 = new ArrayList<>();//重金属
        for(Integer year : yearList){
            //单位
            String unit = hbSewageMapper.getItemName(typeCode);
            //cod
            String cod = hbStatisticsMapper.rankSolidByYear(parkId,regionId, 6,year,userId);
            String codTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,6,yearList,userId);
            Map<String,Object> map = new HashMap<>();
            map.put("year",year);
            map.put("val",StringUtils.isEmpty(cod)?"0":cod);
            data.add(map);
            resultMap.put("name","cod");//cod
            resultMap.put("unit",unit);//cod
            resultMap.put("top",StringUtils.isEmpty(codTop)?"0":codTop);//cod
            //tp
            String tp = hbStatisticsMapper.rankSolidByYear(parkId,regionId, 7,year,userId);
            String tpTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,7,yearList,userId);
            Map<String,Object> map1 = new HashMap<>();
            map1.put("year",year);
            map1.put("val",StringUtils.isEmpty(tp)?"0":tp);
            data1.add(map1);
            resultMap1.put("name","tp");//tp
            resultMap1.put("unit",unit);//tp
            resultMap1.put("top",StringUtils.isEmpty(tpTop)?"0":tpTop);//tp
            //NH3-N
            String nh  = hbStatisticsMapper.rankSolidByYear(parkId,regionId,8,year,userId);
            String nhTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,8,yearList,userId);
            Map<String,Object> map2 = new HashMap<>();
            map2.put("val",StringUtils.isEmpty(nh)?"0":nh);
            map2.put("year",year);
            data2.add(map2);
            resultMap2.put("name","nh3-n");//NH3-N
            resultMap2.put("unit",unit);//NH3-N
            resultMap2.put("top",StringUtils.isEmpty(nhTop)?"0":nhTop);//NH3-N
            //TN
            String tn  = hbStatisticsMapper.rankSolidByYear(parkId,regionId,22,year,userId);
            String tnTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,22,yearList,userId);
            Map<String,Object> map3 = new HashMap<>();
            map3.put("val",StringUtils.isEmpty(tn)?"0":tn);
            map3.put("year",year);
            data3.add(map3);
            resultMap3.put("name","tn");//TN
            resultMap3.put("unit",unit);//TN
            resultMap3.put("top",StringUtils.isEmpty(tnTop)?"0":tnTop);//TN
            //重金属
            String zjs  = hbStatisticsMapper.rankSolidByYear(parkId,regionId,10,year,userId);
            String zjsTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,10,yearList,userId);
            Map<String,Object> map4 = new HashMap<>();
            map4.put("val",StringUtils.isEmpty(zjs)?"0":zjs);
            map4.put("year",year);
            data4.add(map4);
            resultMap4.put("name","重金属");//重金属
            resultMap4.put("unit",unit);//重金属
            resultMap4.put("top",StringUtils.isEmpty(zjsTop)?"0":zjsTop);//重金属
        }
        resultMap.put("data",data);//cod
        resultList.add(resultMap);//cod
        resultMap1.put("data",data1);//tp
        resultList.add(resultMap1);//tp
        resultMap2.put("data",data2);//NH3-N
        resultList.add(resultMap2);//NH3-N
        resultMap3.put("data",data3);//TN
        resultList.add(resultMap3);//TN
        resultMap4.put("data",data4);//重金属
        resultList.add(resultMap4);//重金属
        return resultList;
    }

    //进5年某种排放类型数据(园区统计)
    private List<Map<String,Object>> park3(Integer parkId,Integer regionId,List<Integer> yearList,String typeCode ,int userId) {
        log.info("-------------------园区年度废气指标-------------------");
        List<Map<String,Object>> resultList = new ArrayList<>();
        for(Integer year : yearList){
            List<Map<String,Object>> data = new ArrayList<>();
            Map<String,Object> resultMap = new HashMap<>();
            //单位
            String unit = hbSewageMapper.getItemName(typeCode);
            //SO2
            String so2 = hbStatisticsMapper.rankSolidByYear(parkId,regionId, 11,year,userId);
            String so2Top = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,11,yearList,userId);
            Map<String,Object> map = new HashMap<>();
            map.put("name","so2");
            map.put("val",StringUtils.isEmpty(so2)?"0":so2);
            map.put("top",StringUtils.isEmpty(so2Top)?"0":so2Top);
            map.put("unit",unit);
            data.add(map);
            //NO2
            String no2 = hbStatisticsMapper.rankSolidByYear(parkId,regionId, 12,year,userId);
            String no2Top = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,12,yearList,userId);
            Map<String,Object> map1 = new HashMap<>();
            map1.put("name","no2");
            map1.put("val",StringUtils.isEmpty(no2)?"0":no2);
            map1.put("top",StringUtils.isEmpty(no2Top)?"0":no2Top);
            map1.put("unit",unit);
            data.add(map1);
            //VOCS
            String vocs  = hbStatisticsMapper.rankSolidByYear(parkId,regionId,13,year,userId);
            String vocsTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,13,yearList,userId);
            Map<String,Object> map2 = new HashMap<>();
            map2.put("name","vocs");
            map2.put("val",StringUtils.isEmpty(vocs)?"0":vocs);
            map2.put("top",StringUtils.isEmpty(vocsTop)?"0":vocsTop);
            map2.put("unit",unit);
            data.add(map2);
            //颗粒物
            String klw  = hbStatisticsMapper.rankSolidByYear(parkId,regionId,23,year,userId);
            String klwTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,23,yearList,userId);
            Map<String,Object> map3 = new HashMap<>();
            map3.put("name","klw");
            map3.put("val",StringUtils.isEmpty(klw)?"0":klw);
            map3.put("top",StringUtils.isEmpty(klwTop)?"0":klwTop);
            map3.put("unit",unit);
            data.add(map3);
            resultMap.put("year",year);
            resultMap.put("data",data);
            resultList.add(resultMap);
        }
        return resultList;
    }

    //进5年某种排放类型数据(园区曲线统计)
    private List<Map<String,Object>> parkCurve3(Integer parkId,Integer regionId,List<Integer> yearList,String typeCode ,int userId) {
        log.info("-------------------园区年度废气指标曲线-------------------");
        List<Map<String,Object>> resultList = new ArrayList<>();//总返回值
        Map<String,Object> resultMap = new HashMap<>();//SO2
        List<Map<String,Object>> data = new ArrayList<>();//SO2
        Map<String,Object> resultMap1 = new HashMap<>();//NO2
        List<Map<String,Object>> data1 = new ArrayList<>();//NO2
        Map<String,Object> resultMap2 = new HashMap<>();//VOCS
        List<Map<String,Object>> data2 = new ArrayList<>();//VOCS
        Map<String,Object> resultMap3 = new HashMap<>();//颗粒物
        List<Map<String,Object>> data3 = new ArrayList<>();//颗粒物
        for(Integer year : yearList){
            //单位
            String unit = hbSewageMapper.getItemName(typeCode);
            //SO2
            String so2 = hbStatisticsMapper.rankSolidByYear(parkId,regionId, 11,year,userId);
            String so2Top = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,11,yearList,userId);
            Map<String,Object> map = new HashMap<>();
            map.put("year",year);
            map.put("val",StringUtils.isEmpty(so2)?"0":so2);
            data.add(map);
            resultMap.put("name","so2");//cod
            resultMap.put("unit",unit);//cod
            resultMap.put("top",StringUtils.isEmpty(so2Top)?"0":so2Top);//cod
            //NO2
            String no2 = hbStatisticsMapper.rankSolidByYear(parkId,regionId, 12,year,userId);
            String no2Top = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,12,yearList,userId);
            Map<String,Object> map1 = new HashMap<>();
            map1.put("year",year);
            map1.put("val",StringUtils.isEmpty(no2)?"0":no2);
            data1.add(map1);
            resultMap1.put("name","no2");//tp
            resultMap1.put("unit",unit);//tp
            resultMap1.put("top",StringUtils.isEmpty(no2Top)?"0":no2Top);//tp
            //VOCS
            String vocs  = hbStatisticsMapper.rankSolidByYear(parkId,regionId,13,year,userId);
            String vocsTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,13,yearList,userId);
            Map<String,Object> map2 = new HashMap<>();
            map2.put("val",StringUtils.isEmpty(vocs)?"0":vocs);
            map2.put("year",year);
            data2.add(map2);
            resultMap2.put("name","vocs");//NH3-N
            resultMap2.put("unit",unit);//NH3-N
            resultMap2.put("top",StringUtils.isEmpty(vocsTop)?"0":vocsTop);//NH3-N
            //颗粒物
            String klw  = hbStatisticsMapper.rankSolidByYear(parkId,regionId,23,year,userId);
            String klwTop = hbStatisticsMapper.rankSolidByYearTop(parkId,regionId,23,yearList,userId);
            Map<String,Object> map3 = new HashMap<>();
            map3.put("val",StringUtils.isEmpty(klw)?"0":klw);
            map3.put("year",year);
            data3.add(map3);
            resultMap3.put("name","klw");//TN
            resultMap3.put("unit",unit);//TN
            resultMap3.put("top",StringUtils.isEmpty(klwTop)?"0":klwTop);//TN
        }
        resultMap.put("data",data);//cod
        resultList.add(resultMap);//cod
        resultMap1.put("data",data1);//tp
        resultList.add(resultMap1);//tp
        resultMap2.put("data",data2);//NH3-N
        resultList.add(resultMap2);//NH3-N
        resultMap3.put("data",data3);//TN
        resultList.add(resultMap3);//TN
        return resultList;
    }

    @Override
    public List<Map<String,Object>> companyStatisticsRank(Integer type,Integer companyId,int userId) {
        List list = new ArrayList<>();
        int year = DateUtil.getYear();//当前年份
        list.add(year+1);
        list.add(year);
        list.add(year-1);
        list.add(year-2);
        list.add(year-3);
        log.info("-------------------企业最近五次上传（废水、一般固废、危险固废）-------------------");
        switch (type){
            case 1:
                List<Map<String,Object>> rank1 = company1(companyId,1,list,"wasteWater-issue",userId);//废水排放量单位
                return  rank1;
            case 2:
                List<Map<String,Object>> rank2 = company2(companyId,list,"wasteWater-issue","废水",userId);//废水排放量单位
                return rank2;
            case 3:
                List<Map<String,Object>> rank3 = company3(companyId,list,"flueGas-issue","废水",userId);//废水排放量单位
                return rank3;
        }
        return null;
    }

    //进5年某种排放类型数据(企业统计)
    private List<Map<String,Object>> company1(Integer companyId,Integer sewageId,List<Integer> yearList,String typeCode ,int userId) {
        List<Map<String,Object>> resultList = new ArrayList<>();
        for(Integer year : yearList){
            List<Map<String,Object>> data = new ArrayList<>();
            Map<String,Object> resultMap = new HashMap<>();
            //单位
            String unit = hbSewageMapper.getItemName(typeCode);
            //废水值
            String fs1 = hbStatisticsMapper.yearTimes1(companyId, sewageId,year,userId);
            double fs = Double.parseDouble(StringUtils.isEmpty(fs1)? "0" : fs1);
            //一般固废
            String ybgf1 = hbStatisticsMapper.yearTimes(companyId, 15,year,userId);
            double ybgf = Double.parseDouble(StringUtils.isEmpty(ybgf1)? "0" : ybgf1);
            //危险固废
            String wxgf1 = hbStatisticsMapper.yearTimes(companyId, 16,year,userId);
            double wxgf = Double.parseDouble(StringUtils.isEmpty(wxgf1)? "0" : wxgf1);

            Map<String,Object> map = new HashMap<>();
            map.put("name","废水");
            map.put("val",test(fs));
            map.put("unit",unit);
            data.add(map);
            Map<String,Object> map1 = new HashMap<>();
            map1.put("name","一般固废");
            map1.put("val",test(ybgf));
            map1.put("unit",unit);
            data.add(map1);
            Map<String,Object> map2 = new HashMap<>();
            map2.put("name","危险固废");
            map2.put("val",test(wxgf));
            map2.put("unit",unit);
            data.add(map2);
            resultMap.put("year",year);
            resultMap.put("data",data);
            resultList.add(resultMap);
        }
        return resultList;
    }

    //进5年某种排放类型数据(企业统计)
    private List<Map<String,Object>> company2(Integer companyId,List<Integer> yearList,String typeCode,String name,int userId) {
        List<Map<String,Object>> resultList = new ArrayList<>();
        for(Integer year : yearList){
            List<Map<String,Object>> data = new ArrayList<>();
            Map<String,Object> resultMap = new IdentityHashMap<>();
            //单位
            String unit = hbSewageMapper.getItemName(typeCode);

            //COD
            String cod1 = hbStatisticsMapper.yearTimes(companyId, 6,year,userId);
            double cod = Double.parseDouble(StringUtils.isEmpty(cod1)? "0" : cod1);

            //TP
            String tp1 = hbStatisticsMapper.yearTimes(companyId, 7,year,userId);
            double tp = Double.parseDouble(StringUtils.isEmpty(tp1)? "0" : tp1);
            String tp2= String.valueOf(cod);
            //NH3-N
            String nh1 = hbStatisticsMapper.yearTimes(companyId, 8,year,userId);
            double nh = Double.parseDouble(StringUtils.isEmpty(nh1)? "0" : nh1);

            //TN
            String tn1 = hbStatisticsMapper.yearTimes(companyId, 22,year,userId);
            double tn = Double.parseDouble(StringUtils.isEmpty(tn1)? "0" : tn1);

            //重金属
            String zjs1 = hbStatisticsMapper.yearTimes(companyId, 10,year,userId);
            double zjs = Double.parseDouble(StringUtils.isEmpty(zjs1)? "0" : zjs1);

            Map<String,Object> map = new HashMap<>();
            map.put("name","cod");
            map.put("val",test(cod));
            map.put("unit",unit);
            data.add(map);
            Map<String,Object> map1 = new HashMap<>();
            map1.put("name","tp");
            map1.put("val",tp2);
            map1.put("unit",unit);
            data.add(map1);
            Map<String,Object> map2 = new HashMap<>();
            map2.put("name","nh3-n");
            map2.put("val",test(nh));
            map2.put("unit",unit);
            data.add(map2);
            Map<String,Object> map3 = new HashMap<>();
            map3.put("name","tn");
            map3.put("val",test(tn));
            map3.put("unit",unit);
            data.add(map3);
            Map<String,Object> map4 = new HashMap<>();
            map4.put("name","重金属");
            map4.put("val",test(zjs));
            map4.put("unit",unit);
            data.add(map4);
            resultMap.put("year",year);
            resultMap.put("data",data);
            resultList.add(resultMap);
        }
        return resultList;
    }

    //进5年某种排放类型数据(企业统计)
    private List<Map<String,Object>> company3(Integer companyId,List<Integer> yearList,String typeCode,String name,int userId) {
        List<Map<String,Object>> resultList = new ArrayList<>();
        for(Integer year : yearList){
            List<Map<String,Object>> data = new ArrayList<>();
            Map<String,Object> resultMap = new IdentityHashMap<>();

            //单位
            String unit = hbSewageMapper.getItemName(typeCode);

            //SO2
            String so21 = hbStatisticsMapper.yearTimes(companyId, 11,year,userId);
            double so2 = Double.parseDouble(StringUtils.isEmpty(so21)? "0" : so21);

            //NO2
            String no21 = hbStatisticsMapper.yearTimes(companyId, 12,year,userId);
            double no2 = Double.parseDouble(StringUtils.isEmpty(no21)? "0" : no21);

            //VOCS
            String vocs1 = hbStatisticsMapper.yearTimes(companyId, 13,year,userId);
            double vocs = Double.parseDouble(StringUtils.isEmpty(vocs1)? "0" : vocs1);

            //颗粒物
            String klw1 = hbStatisticsMapper.yearTimes(companyId, 23,year,userId);
            double klw = Double.parseDouble(StringUtils.isEmpty(klw1)? "0" : klw1);

            Map<String,Object> map = new HashMap<>();
            map.put("name","so2");
            map.put("val",test(so2));
            map.put("unit",unit);
            data.add(map);
            Map<String,Object> map1 = new HashMap<>();
            map1.put("name","no2");
            map1.put("val",test(no2));
            map1.put("unit",unit);
            data.add(map1);
            Map<String,Object> map2 = new HashMap<>();
            map2.put("name","vocs");
            map2.put("val",test(vocs));
            map2.put("unit",unit);
            data.add(map2);
            Map<String,Object> map3 = new HashMap<>();
            map3.put("name","颗粒物");
            map3.put("val",test(klw));
            map3.put("unit",unit);
            data.add(map3);
            resultMap.put("year",year);
            resultMap.put("data",data);
            resultList.add(resultMap);
        }
        return resultList;
    }

    private BigDecimal test(double val){
        Formatter rmZero = new Formatter();
        // 进行格式化截断尾部小数并转化成字符串
        String rm = ""+rmZero.format("%g", val);
        // 将字符串解析成double并存入大数类
        BigDecimal todo = BigDecimal.valueOf(Double.parseDouble(rm));
        return todo.stripTrailingZeros();
    }
}
