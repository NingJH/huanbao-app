package com.cqndt.huanbao.app.service;

import java.util.List;
import java.util.Map;

public interface ManageListService {

    List<Map<String,Object>> getManageListByUserId(Integer userId, String name);
}
