package com.cqndt.huanbao.app.service;


import com.cqndt.huanbao.app.pojo.HbNotice;

import java.util.List;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-29 11:22
 **/
public interface NoticeService {

    List<HbNotice> selectNotice(int userId);
}
