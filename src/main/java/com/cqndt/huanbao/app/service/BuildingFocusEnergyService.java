package com.cqndt.huanbao.app.service;


import com.cqndt.huanbao.app.pojo.BuildingPowerCompany;

import java.util.List;
import java.util.Map;

public interface BuildingFocusEnergyService {

    List<Map<String,Object>> selectRainFocusEnergyProcess(Integer feId);

    List<Map<String,Object>> selectRainFocusEnergyOutIn(BuildingPowerCompany buildingPowerCompany);
}
