package com.cqndt.huanbao.app.service;

import com.cqndt.huanbao.app.pojo.AlarmLevelCollection;
import com.cqndt.huanbao.app.pojo.AlarmLevelCondition;


/**
 * Create by Intellij IDEA
 * User : wuhao
 * Mail : 863254617@qq.com
 * Time : 2019/7/24 14:18
 */
public interface IAlarmRoleService {
    AlarmLevelCollection listAlarmLevelByCondition(AlarmLevelCondition alarmLevelCondition);
}
