package com.cqndt.huanbao.app.service;


import com.cqndt.huanbao.app.pojo.BuildingInterceptionDam;

import java.util.List;

/**
 * @Time : 2019-09-18 09:25
 **/
public interface IBuildingInterceptionDamService {

    /**
     * 查询拦截坝
     * @param id
     * @return
     */
    List<BuildingInterceptionDam> selectInterceptionDam(Integer id);
}
