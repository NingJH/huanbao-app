package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.BuildingPowerCompanyMapper;
import com.cqndt.huanbao.app.pojo.BuildingPowerCompany;
import com.cqndt.huanbao.app.service.BuildingPowerCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BuildingPowerCompanyServiceImpl implements BuildingPowerCompanyService {

    @Autowired
    private BuildingPowerCompanyMapper buildingPowerCompanyMapper;


    @Override
    public List<BuildingPowerCompany> companyList(BuildingPowerCompany buildingPowerCompany) {
        List<BuildingPowerCompany> companyList=buildingPowerCompanyMapper.companyList(buildingPowerCompany);
        return companyList;
    }

}
