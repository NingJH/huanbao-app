package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbUserCompanyMapper;
import com.cqndt.huanbao.app.dao.HbUserMapper;
import com.cqndt.huanbao.app.pojo.Device;
import com.cqndt.huanbao.app.pojo.HbUser;
import com.cqndt.huanbao.app.pojo.HbUserCompany;
import com.cqndt.huanbao.app.pojo.HbUserToken;
import com.cqndt.huanbao.app.service.AdvancedPushService;
import com.cqndt.huanbao.app.util.AliPushUtil;
import com.cqndt.huanbao.app.util.JsonDateValueProcessor;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class AdvancedPushServiceImpl implements AdvancedPushService {
    @Autowired
    private HbUserMapper hbUserMapper;
    @Autowired
    HbUserCompanyMapper userCompanyMapper;
    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Override
    @Async
    public void AdvancedPush(Integer userId) {
        //获取所有app用户
       // List<HbUser> userList = hbUserMapper.getUserList();
        //获取所有告警设备
      //  List<Device> deviceList=hbUserMapper.getDeviceLsit();
        //System.out.println("进入执行token方法");
      //  System.out.println("用户id>>>>>>>>"+ userId);
        HbUser hbUser=hbUserMapper.getUserByuserId(userId);
       // System.out.println("查询对应的电话号码"+hbUser.getPhone());
        HbUserToken token = hbUserMapper.getByPhone(hbUser.getPhone());
       // System.out.println("打印登录最终执行的token>>>>>>>>>>>>"+token.getToken());
        List<Map<String, String>> mapList1 = hbUserMapper.getAlarmTBS001();
        List<Map<String, String>> mapList2 = hbUserMapper.getAlarmTBS011();
        List<Map<String, String>> mapList3 = hbUserMapper.getAlarmTBS060();
        List<Map<String, String>> mapList4 = hbUserMapper.getAlarmTBS101();
        Map<String, String> alarmValue1 = hbUserMapper.getAlarmValueTBS001();
        Map<String, String> alarmValue2 = hbUserMapper.getAlarmValueTBS011();
        Map<String, String> alarmValue3 = hbUserMapper.getAlarmValueTBS060();
        Map<String, String> alarmValue4 = hbUserMapper.getAlarmValueTBS101();
        if (null!=token) {
            mapList1.forEach(item -> {
                if (2 == Integer.parseInt(alarmValue1.get("is_fault"))) {
                    if (Double.parseDouble(item.get("v")) > Double.parseDouble(alarmValue1.get("max_value")) ||
                            Double.parseDouble(item.get("v")) < Double.parseDouble(alarmValue1.get("min_value"))) {
                        JsonConfig jsonConfig1 = new JsonConfig();
                        jsonConfig1.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
                        JSONObject json1 = new JSONObject();
                        json1 = json1.fromObject(item, jsonConfig1);
                        try {
                            if (null != token) {
                                AliPushUtil.AdvancedPush(item.get("device_no"), "设备编号:" + item.get("device_no") + "  告警值：" + item.get("v") + "时间：" + item.get("create_time") + "  经纬度：" + item.get("lon") + "," + item.get("lat") + "地址： " + item.get("address"), json1, token.getToken().substring(50, 100));
                            }
                        } catch (Exception e) {
                            System.out.println("阿里推送失败");
                            e.printStackTrace();
                        }
                    }
                }
            });
            mapList2.forEach(item1 -> {
                if (2 == Integer.parseInt(alarmValue2.get("is_fault"))) {
                    if (Double.parseDouble(item1.get("v")) > Double.parseDouble(alarmValue2.get("max_value"))) {
                        JsonConfig jsonConfig2 = new JsonConfig();
                        jsonConfig2.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
                        JSONObject json2 = new JSONObject();
                        json2 = json2.fromObject(item1, jsonConfig2);
                        try {
                            if (null != token) {
                                System.out.println("进入告警推送");
                                AliPushUtil.AdvancedPush(item1.get("device_name"), "设备编号:" + item1.get("device_no") + "  告警值：" + item1.get("v") + "时间：" + item1.get("create_time") + "  经纬度：" + item1.get("lon") + "," + item1.get("lat") + "地址： " + item1.get("address"), json2, token.getToken().substring(50, 100));
                            }
                        } catch (Exception e) {
                            System.out.println("阿里推送失败");
                            e.printStackTrace();
                        }
                    }
                }
            });
            mapList3.forEach(item3 -> {
                if (2 == Integer.parseInt(alarmValue3.get("is_fault"))) {
                    if (Double.parseDouble(item3.get("v")) > Double.parseDouble(alarmValue3.get("max_value"))) {
                        JsonConfig jsonConfig3 = new JsonConfig();
                        jsonConfig3.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
                        JSONObject json3 = new JSONObject();
                        json3 = json3.fromObject(item3, jsonConfig3);
                        try {
                            if (null != token) {
                                System.out.println("进入告警推送");
                                AliPushUtil.AdvancedPush(item3.get("device_name"), "设备编号:" + item3.get("device_no") + "  告警值：" + item3.get("v") + "时间：" + item3.get("create_time") + "  经纬度：" + item3.get("lon") + "," + item3.get("lat") + "地址： " + item3.get("address"), json3, token.getToken().substring(50, 100));
                            }
                        } catch (Exception e) {
                            System.out.println("阿里推送失败");
                            e.printStackTrace();
                        }
                    }
                }
            });
            mapList4.forEach(item4 -> {
                if (2 == Integer.parseInt(alarmValue4.get("is_fault"))) {
                    if (Double.parseDouble(item4.get("v")) > Double.parseDouble(alarmValue4.get("max_value"))) {
                        JsonConfig jsonConfig4 = new JsonConfig();
                        jsonConfig4.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
                        JSONObject json4 = new JSONObject();
                        json4 = json4.fromObject(item4, jsonConfig4);
                        try {
                            if (null != token) {
                                System.out.println("进入告警推送");
                                AliPushUtil.AdvancedPush(item4.get("device_name"), "设备编号:" + item4.get("device_no") + "  告警值：" + item4.get("v") + "时间：" + item4.get("create_time") + "  经纬度：" + item4.get("lon") + "," + item4.get("lat") + "地址： " + item4.get("address"), json4, token.getToken().substring(50, 100));
                            }
                        } catch (Exception e) {
                            System.out.println("阿里推送失败");
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
        }


//        for (HbUser hbUser:userList) {
//            //查询用户对应的园区、分区
//            HbUserCompany hbUserCompany= userCompanyMapper.getHbUserCompanyInfo(hbUser.getId());
//            for (Device device:deviceList) {
//                if (null!=hbUserCompany) {
//                    if (hbUserCompany.getParkId() == device.getParkId() || hbUserCompany.getRegionId() == device.getRegionId()) {
//                        HbUserToken token = hbUserMapper.getByPhone(hbUser.getPhone());
//                        if (null != token) {
//                            System.out.println("进入推送>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//                            mapList1.forEach(item -> {
//                                if (2 == Integer.parseInt(alarmValue1.get("is_fault"))) {
//                                    if (Double.parseDouble(item.get("v")) > Double.parseDouble(alarmValue1.get("max_value")) ||
//                                            Double.parseDouble(item.get("v")) < Double.parseDouble(alarmValue1.get("min_value"))) {
//                                        JsonConfig jsonConfig1 = new JsonConfig();
//                                        jsonConfig1.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
//                                        JSONObject json1 = new JSONObject();
//                                        json1 = json1.fromObject(item, jsonConfig1);
//                                        try {
//                                            if (null != token) {
//                                                System.out.println("进入推送>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//                                                AliPushUtil.AdvancedPush(item.get("device_no"), item.toString(), json1, token.getToken().substring(50, 100));
//                                            }
//                                        } catch (Exception e) {
//                                            System.out.println("阿里推送失败");
//                                            e.printStackTrace();
//                                        }
//                                    }
//                                }
//                                mapList2.forEach(item1 -> {
//                                    if (2 == Integer.parseInt(alarmValue2.get("is_fault"))) {
//                                        if (Double.parseDouble(item1.get("v")) > Double.parseDouble(alarmValue2.get("max_value"))) {
//                                            JsonConfig jsonConfig1 = new JsonConfig();
//                                            jsonConfig1.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
//                                            JSONObject json1 = new JSONObject();
//                                            json1 = json1.fromObject(item1, jsonConfig1);
//                                            try {
//                                                if (null != token) {
//                                                    AliPushUtil.AdvancedPush(item1.get("device_no"), item1.toString(), json1, token.getToken().substring(50, 100));
//                                                }
//                                            } catch (Exception e) {
//                                                System.out.println("阿里推送失败");
//                                                e.printStackTrace();
//                                            }
//                                        }
//                                    }
//                                });
//                                mapList3.forEach(item3 -> {
//                                    if (2 == Integer.parseInt(alarmValue3.get("is_fault"))) {
//                                        if (Double.parseDouble(item3.get("v")) > Double.parseDouble(alarmValue3.get("max_value"))) {
//                                            JsonConfig jsonConfig1 = new JsonConfig();
//                                            jsonConfig1.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
//                                            JSONObject json1 = new JSONObject();
//                                            json1 = json1.fromObject(item3, jsonConfig1);
//                                            try {
//                                                if (null != token) {
//                                                    AliPushUtil.AdvancedPush(item3.get("device_no"), item3.toString(), json1, token.getToken().substring(50, 100));
//                                                }
//                                            } catch (Exception e) {
//                                                System.out.println("阿里推送失败");
//                                                e.printStackTrace();
//                                            }
//                                        }
//                                    }
//                                });
//                                mapList4.forEach(item4 -> {
//                                    if (2 == Integer.parseInt(alarmValue4.get("is_fault"))) {
//                                        if (Double.parseDouble(item4.get("v")) > Double.parseDouble(alarmValue4.get("max_value"))) {
//                                            JsonConfig jsonConfig1 = new JsonConfig();
//                                            jsonConfig1.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
//                                            JSONObject json1 = new JSONObject();
//                                            json1 = json1.fromObject(item4, jsonConfig1);
//                                            try {
//                                                if (null != token) {
//                                                    AliPushUtil.AdvancedPush(item4.get("device_no"), item4.toString(), json1, token.getToken().substring(50, 100));
//                                                }
//                                            } catch (Exception e) {
//                                                System.out.println("阿里推送失败");
//                                                e.printStackTrace();
//                                            }
//                                        }
//                                    }
//                                });
//                            });
//                        }
//                    }
//                }
//            }
//        }


}
