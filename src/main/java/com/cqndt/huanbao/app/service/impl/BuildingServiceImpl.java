package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.*;
import com.cqndt.huanbao.app.pojo.Building;
import com.cqndt.huanbao.app.pojo.BuildingSewageTreatmenPlants;
import com.cqndt.huanbao.app.service.IBuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Time : 2019-09-12 15:13
 **/
@Service
public class BuildingServiceImpl implements IBuildingService {

    @Autowired
    private HbBuildingMapper hbBuildingMapper;
    @Autowired
    private HbBuildingInterceptionDamMapper buildingInterceptionDamMapper;
    @Autowired
    private BuildingSewageTreatmenPlantMapper buildingSewageTreatmenPlantMapper;
    @Autowired
    private HbBuildingAccidentPoolMapper buildingAccidentPoolMapper;
    @Autowired
    private HbBuildingGeneralMapper buildingGeneralMapper;
    @Autowired
    private BuildingRainSewageConverterMapper buildingRainSewageConverterMapper;
    @Autowired
    private BuildingRainSewageMapper buildingRainSewageMapper;
    @Autowired
    private BuildingFocusEnergyMapper buildingFocusEnergyMapper;

//    @Autowired
//    private ICompanyService companyService;


    @Override
    public List<Map<String,Object>> selectBuilding(Building building, int userId) {


        List<Map<String,Object>> buildingList = hbBuildingMapper.selectBuilding(building);
        List<Map<String,Object>> maps = new ArrayList<>();
        for(Map< String,Object> building1 : buildingList){
            if(StringUtils.isEmpty(building1.get("region_id"))){
                building1.put("region_name",null);
            }
//            building1.put("areaRsult",companyService.appendAreaPark(Integer.parseInt(building1.get("park_id").toString())));
            Integer buildingId = Integer.parseInt(building1.get("id").toString());
            switch (Integer.parseInt(building1.get("facility_type").toString())){
                //1,污水处理厂
                case 1:
                    maps = buildingSewageTreatmenPlantMapper.selectSewageTreatmenPlantByBuildingId(buildingId);
                    for(Map<String,Object> map : maps){
                        building1.put("buildId",map.get("buildId"));
                        building1.put("imgUrl",map.get("imgUrl"));
                        building1.put("title",map.get("name"));
                        building1.put("introduction",map.get("introduction"));
                    }
                    break;
                //2,拦截坝
                case 2:
                    maps = buildingInterceptionDamMapper.selectInterceptionDamByBuildingId(buildingId);
                    for(Map<String,Object> map : maps){
                        building1.put("imgUrl",map.get("imgUrl"));
                        building1.put("title",map.get("name"));
                        building1.put("introduction",map.get("introduction"));
                    }
                    break;
                //3,应急事故池
                case 3:
                    maps = buildingAccidentPoolMapper.selectAccidentPoolByBuildingId(buildingId);
                    for(Map<String,Object> map : maps){
                        building1.put("imgUrl",map.get("imgUrl"));
                        building1.put("title",map.get("name"));
                        building1.put("introduction",map.get("introduction"));
                        building1.put("accidentPoolId",map.get("accidentPoolId"));
                    }
                    break;
                //4,一般工业固废暂存场
                case 4:
                    maps = buildingGeneralMapper.selectGeneralByBuildingId(buildingId);
                    for(Map<String,Object> map : maps){
                        building1.put("imgUrl",map.get("imgUrl"));
                        building1.put("title",map.get("name"));
                        building1.put("introduction",map.get("introduction"));
                        building1.put("generalId",map.get("generalId"));
                    }
                    break;
                //5,雨污切换阀
                case 5:
                    maps = buildingRainSewageConverterMapper.selectRainSewageConverterByBuildingId(buildingId);
                    for(Map<String,Object> map : maps){
                        building1.put("imgUrl",map.get("imgUrl"));
                        building1.put("title",map.get("name"));
                        building1.put("introduction",map.get("introduction"));
                        building1.put("rscId",map.get("rscId"));
                    }
                    break;
                //6,雨污管网
                case 6:
                    maps = buildingRainSewageMapper.selectRainSewageByBuildingId(buildingId);
                    for(Map<String,Object> map : maps){
                        building1.put("imgUrl",map.get("imgUrl"));
                        building1.put("title",map.get("name"));
                        building1.put("introduction",map.get("introduction"));
                        building1.put("rsId",map.get("rsId"));
                    }
                    break;
                //7,集中供能
                case 7:
                    maps = buildingFocusEnergyMapper.selectFocusEnergyByBuildingId(buildingId);
                    for(Map<String,Object> map : maps){
                        building1.put("imgUrl",map.get("imgUrl"));
                        building1.put("title",map.get("name"));
                        building1.put("introduction",map.get("introduction"));
                        building1.put("feId",map.get("feId"));
                    }
                    break;
            }
        }
        return buildingList;
    }

    @Override
    public List<Map<String, Object>> selectBuild(Building building) {
        List<Map<String,Object>> buildingList = hbBuildingMapper.selectBuild(building);
        List<Map<String,Object>> maps = new ArrayList<>();

        for(Map< String,Object> building1 : buildingList){
            Map<String,Object> map=new HashMap<String,Object>();
            if((Integer) building1.get("facility_type")==6){
                int rainSewageType=hbBuildingMapper.getRainSewageType((Integer) building1.get("id"));
                map.put("rainType",rainSewageType);
            }
            System.out.println("循环经纬度1111111"+building1.get("center_lon_lat"));
            map.put("id",building1.get("id"));
            map.put("parkId",building1.get("park_id"));
            map.put("region",building1.get("region_id"));
            map.put("facilityType",building1.get("facility_type"));
            map.put("centerLonLat",building1.get("center_lon_lat") == null ? "" :building1.get("center_lon_lat"));
            map.put("borderLonLat",building1.get("border_lon_lat")== null ? "" :building1.get("border_lon_lat"));
            map.put("ywLonLat",building1.get("lon_lat")== null ? "" :building1.get("lon_lat"));
            map.put("name",building1.get("name"));
            map.put("addDate",building1.get("add_date"));
            map.put("parkName",building1.get("park_name"));
            map.put("regionName",building1.get("region_name"));
            maps.add(map) ;
        }

        return maps;
    }
}
