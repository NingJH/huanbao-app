package com.cqndt.huanbao.app.service;



import com.cqndt.huanbao.app.pojo.HbPark;

import java.util.List;
import java.util.Map;

public interface HbParkService {

    Map selectByUserId(int userId);

    /**
     * 根据id查询园区信息
     * @param id
     * @return
     */
    HbPark selectParkById(Integer id);

    /**
     * 根据园区ID,分区ID查询对应的环保手续
     * @param parkId,regionId
     * @return
     */
    List<Map<String,String>> selectReportById(Integer parkId, Integer regionId, String context,Integer type);

    String getGisLayer(Integer parkId, Integer regionId);

    List<Map<String,Object>> getRegionLonLat(Integer regionId,Integer parkId);

    List<Map<String,Object>> fuJianList(Integer parkId,Integer regionId,String context );

    List<Map<String,Object>> selectProcedureExamine(Integer parkId,Integer type,String context);
}
