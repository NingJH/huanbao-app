package com.cqndt.huanbao.app.service;


import com.cqndt.huanbao.app.pojo.Building;

import java.util.List;
import java.util.Map;

/**
 * @Time : 2019-09-12 15:13
 **/
public interface IBuildingService {

    List<Map<String,Object>> selectBuilding(Building building, int userId);
    List<Map<String,Object>> selectBuild(Building building);

}
