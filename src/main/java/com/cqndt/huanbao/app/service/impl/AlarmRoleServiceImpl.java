package com.cqndt.huanbao.app.service.impl;


import com.cqndt.huanbao.app.dao.IAlarmRoleMapper;
import com.cqndt.huanbao.app.pojo.AlarmLevel;
import com.cqndt.huanbao.app.pojo.AlarmLevelCollection;
import com.cqndt.huanbao.app.pojo.AlarmLevelCondition;
import com.cqndt.huanbao.app.service.IAlarmRoleService;
import com.cqndt.huanbao.app.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Create by Intellij IDEA
 * User : wuhao
 * Mail : 863254617@qq.com
 * Time : 2019/7/24 14:18
 */
@Service
public class AlarmRoleServiceImpl implements IAlarmRoleService {

    @Autowired
    private IAlarmRoleMapper alarmRoleMapper;

    @Override
    public AlarmLevelCollection listAlarmLevelByCondition(AlarmLevelCondition alarmLevelCondition) {
        int totalCount = alarmRoleMapper.countAlarmLevelByCondition(alarmLevelCondition);
        List<AlarmLevel> entityList = alarmRoleMapper.listAlarmLevelByCondition(alarmLevelCondition);
        entityList.forEach(alarmLevel -> alarmLevel.setDate(DateUtil.formatTime(alarmLevel.getDate())));
        AlarmLevelCollection alarmLevelCollection= new AlarmLevelCollection();
        alarmLevelCollection.setTotalCount(totalCount);
        alarmLevelCollection.setEntityList(entityList);
        return alarmLevelCollection;
    }

}
