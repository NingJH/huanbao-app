package com.cqndt.huanbao.app.service;

import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-06-19 09:52
 **/
public interface KeeperLonlatService {
    List<Map<String,Object>> listKeeperLonlat(String parkId,String regionId);
}
