package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.HbProceduresMapper;
import com.cqndt.huanbao.app.service.ProceduresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProceduresServiceImpl implements ProceduresService {

    @Autowired
    HbProceduresMapper hbProceduresMapper;

    @Override
    public Map selectMeasureReport(Integer parkId,Integer companyId,Integer type,Integer regionId) {
        //查询公司简介
        String introduction=hbProceduresMapper.companyIntro(companyId);
        //查询监测报告信息
        List<Map<String,String>> report=hbProceduresMapper.measureReport(parkId,companyId,type,regionId);

        //String detail = hbProceduresMapper.selectProceduresContent(companyId);
        Map<String,Object> map=new HashMap<>();
        map.put("companyIntroduction",introduction);
        map.put("report",report);
        //map.put("content",detail);
        return map;
    }
}
