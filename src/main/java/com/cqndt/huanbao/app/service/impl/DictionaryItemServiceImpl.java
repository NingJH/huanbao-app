package com.cqndt.huanbao.app.service.impl;

import com.cqndt.huanbao.app.dao.DictionaryItemMapper;
import com.cqndt.huanbao.app.service.DictionaryItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class DictionaryItemServiceImpl implements DictionaryItemService {

    @Resource
    private DictionaryItemMapper dictionaryItemMapper;

    @Override
    public List<String> listDictionaryItemAll(String typeCode) {
        return dictionaryItemMapper.listDictionaryItemAll(typeCode);
    }
}
