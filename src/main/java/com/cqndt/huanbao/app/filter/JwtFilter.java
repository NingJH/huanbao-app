package com.cqndt.huanbao.app.filter;


import com.cqndt.huanbao.app.exception.ErrorType;
import com.cqndt.huanbao.app.exception.ServiceException;
import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.pojo.JwtHelper;
import com.cqndt.huanbao.app.pojo.MyHttpRequestWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * 类的详细说明
 *
 * @author zhjj
 * @version 1.00
 * Date:2018/04/09 14:19
 */
@WebFilter(urlPatterns = {"/environment/*"})
@Slf4j
public class JwtFilter implements Filter {

    @Resource
    private StringRedisTemplate stringRedisTemplate;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("过滤器初始化");
    }

    /**
     * Reserved claims（保留），它的含义就像是编程语言的保留字一样，属于JWT标准里面规定的一些claim。JWT标准里面定好的claim有：
     * <p>
     * iss(Issuser)：代表这个JWT的签发主体；
     * sub(Subject)：代表这个JWT的主体，即它的所有人；
     * aud(Audience)：代表这个JWT的接收对象；
     * exp(Expiration time)：是一个时间戳，代表这个JWT的过期时间；
     * nbf(Not Before)：是一个时间戳，代表这个JWT生效的开始时间，意味着在这个时间之前验证JWT是会失败的；
     * iat(Issued at)：是一个时间戳，代表这个JWT的签发时间；
     * jti(JWT ID)：是JWT的唯一标识。
     *
     * @param req
     * @param res
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        log.info("请求地址" + request.getServletPath());

        if (request.getServletPath().equals("/api/v2/auth/download") || request.getServletPath().equals("/environment/user/login")) {
            log.info("该请求为登录请求,不请求拦截器");
            return;
        }

        Map map = request.getParameterMap();
        Set set = map.keySet();
        Iterator iterator = set.iterator();
        Map hashMap = new HashMap();
        while (iterator.hasNext()) {
            Object key = iterator.next();
            key = String.valueOf(key);
            String[] object = (String[]) map.get(key);
            hashMap.put(key, object);
        }
        //等到请求头信息authorization信息
        final String authHeader = request.getHeader("authorization");
        log.info("请求头token" + authHeader);
        if ("OPTIONS".equals(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);
            chain.doFilter(req, res);
        } else {

            if (authHeader == null || !authHeader.startsWith("bearer;")) {
                throw new ServiceException(ErrorType.FORBIDDEN);
            }
            final String token = authHeader.substring(7);

            try {
                String base64Secret = stringRedisTemplate.opsForValue().get("jwt_token:" + authHeader + ":secret");
                final Claims claims = JwtHelper.parseJWT(token, base64Secret);
                if (claims == null) {
                    log.info("解析异常");
                    sendErrorInfo(request, response);
                    return;
                }

                System.out.println("jwt 过滤器 " + claims.get("userId"));
                hashMap.put("userId", new String[]{claims.get("userId").toString()});
                hashMap.put("roleId", new String[]{claims.get("roleId").toString()});
                hashMap.put("name", new String[]{claims.get("name").toString()});

//                    String refreshJwtToken = userService.refreshJwtToken(Integer.parseInt(new String[]{claims.get("userId").toString()}[0]));
                hashMap.put("refreshJwtToken", authHeader);

            } catch (final Exception e) {
                e.printStackTrace();
                sendErrorInfo(request, response);
                return;
            }
            MyHttpRequestWrapper myHttpRequestWrapper = new MyHttpRequestWrapper((HttpServletRequest) request);
            myHttpRequestWrapper.setParameterMap(hashMap);
            chain.doFilter(myHttpRequestWrapper, res);
        }
    }

    private void sendErrorInfo(HttpServletRequest request, HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try (PrintWriter writer = response.getWriter()) {
            response.setStatus(HttpServletResponse.SC_OK);
            AjaxResult ajaxResult = new AjaxResult(false, ErrorType.TOKEN_ERROR.getCode(), ErrorType.TOKEN_ERROR.getErrorMsg());
            ObjectMapper mapper = new ObjectMapper();
            String mapJakcson = mapper.writeValueAsString(ajaxResult);
            writer.print(mapJakcson);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {

    }


}
