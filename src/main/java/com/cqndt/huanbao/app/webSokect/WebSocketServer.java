package com.cqndt.huanbao.app.webSokect;

import com.cqndt.huanbao.app.aop.serivce.HttpClientUtils;
import com.cqndt.huanbao.app.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Create by Intellij IDEA
 * User : mengjiajie
 * Mail : 15826014394@163.com
 * Time : 2019/6/26 15:30
 **/

@Slf4j
@ServerEndpoint(value = "/websocket/{userId}/{sessionId}")
@Component
public class WebSocketServer {
        private static String url = "http://94.191.111.217:8085/auth/statLoginLog/updateStatLoginLog";
//    private static String url = "http://192.168.10.124:8085/auth/statLoginLog/updateStatLoginLog";
    private static HttpClientUtils httpClientUtils = new HttpClientUtils();
    //静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
    private static int onlineCount = 0;
    //concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
    private static CopyOnWriteArraySet<WebSocketServer> webSocketSet = new CopyOnWriteArraySet<WebSocketServer>();
    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;
    private int userId;
    private String sessionId;

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(@PathParam(value = "userId") int userId, @PathParam(value = "sessionId") String sessionId, Session session) {
        this.session = session;
        this.userId = userId;
        this.sessionId = sessionId;
        webSocketSet.add(this);     //加入set中
        addOnlineCount();           //在线数加1
        log.info("有新连接加入！当前在线人ID为" + userId);
        log.info("当前连接数为"+webSocketSet.size());
//        try {
//            sendMessage("连接成功");
//        } catch (IOException e) {
//            log.error("websocket IO异常");
//        }
    }
    //	//连接打开时执行
    //	@OnOpen
    //	public void onOpen(@PathParam("user") String user, Session session) {
    //		currentUser = user;
    //		System.out.println("Connected ... " + session.getId());
    //	}

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        webSocketSet.remove(this);  //从set中删除
        subOnlineCount();           //在线数减1
        log.info("有一连接关闭！退出人人ID为" + userId);
        log.info("当前连接数为" + webSocketSet.size());
        loginOut();
    }

    private void loginOut() {
        Map<String, String> map = new HashMap<>();
        map.put("sessionId", sessionId);
        map.put("exitTime", DateUtil.formatDate(new Date(), "yyyy-MM-dd hh:mm:ss"));
        map.put("exitProcess", "3");
        httpClientUtils.httpPostForm(url, map);
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        log.info("来自客户端的消息:" + message);
//        群发消息
        for (WebSocketServer item : webSocketSet) {
            try {
                item.sendMessage(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误"+error.getMessage());
    }


    public void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }


    /**
     * 群发自定义消息
     */
    public static void sendInfo(int userId, String message) throws IOException {
        System.out.println("前端传过来的用户ID>>>>>>>>>>>>>>"+userId);
        System.out.println("app推送数据给前端>>>>>>>>>>>>>>>"+message);
        for (WebSocketServer item : webSocketSet) {
            if (userId == item.userId) {
                try {
                    item.sendMessage(message);
                    System.out.println(">>>>>>>>>>>>当前用户推送成功>>>>>>>>>>>>>>>>>");
                } catch (IOException ignored) {
                    System.out.println(">>>>>>>>>>>>当前用户推送抛出异常>>>>>>>>>>>>>>>>>");
                }
            }

        }
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        WebSocketServer.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        WebSocketServer.onlineCount--;
    }
}
