package com.cqndt.huanbao.app.pojo;

import com.cqndt.huanbao.app.exception.ErrorInfo;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * user: zhaojianji
 * date: 2017/05/19
 * desc:  描述用途
 */
@Data
public class AjaxResult {
    private boolean ok;//成功与失败判定标志*
    private  int code ; //状态代码 约定同http状态码 200 -> ok
    private  String  message ;//附加消息 供前端控制台输出 用于排查
    private Object data; // 具体数据
    private boolean page; // 是否分页*
    private int curPage; // 当前页码*
    private int Count; // 总记录数
    private int totalPage; // 总页数*
    private int curRecord; //当前返回数 前端可直接使用进行遍历*
    private int beginRow; // 起始索引 可不填 前端如果可以更改每页记录数才需要*
    private int endRow; // 结束索引 可不填 前端如果可以更改每页记录数才需要*
    private int prePageSize; // 每页记录数 可不填 前端如果可以更改每页记录数才需要*
    private List<ErrorInfo> errors;//*
    private String refreshJwtToken;
    private Map<String,Object> response;

    public AjaxResult() {
    }

    public AjaxResult(boolean ok, int code, String message, String refreshJwtToken) {
        this.ok = ok;
        this.code = code;
        this.message = message;
        this.refreshJwtToken = refreshJwtToken;
    }
    public AjaxResult(boolean ok, int code, String message, Object data) {
        this.ok = ok;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public AjaxResult(boolean ok, int code, String message) {
        this.ok = ok;
        this.code = code;
        this.message = message;
    }

    public AjaxResult(boolean ok, String message) {
        this.ok = ok;
        this.message = message;
    }


}

