package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

/**
 * Create Njh
 *
 * @Time : 2019-09-11 15:20
 **/
@Data
public class HbAlarmUser {
    private Integer Id;//id

    private Integer userId;//用户id

    private Integer alarmLevelId;//告警等级id

    private Integer level;//告警等级

    private Date date;//添加时间

    private String remarks;//备注

    private Integer del;//删除标志
}
