package com.cqndt.huanbao.app.pojo;

import lombok.Data;

@Data
public class DictionaryItem {
    private int id = -1 ; // 编号

    private String typeCode ; // 类型标识

    private String itemCode ; // 字典标识

    private String itemName ; // 字典名称

    private String itemValue ; // 字典值

    private String conversionUnit ; // 换算单位

    private int conversionValue = -1 ; // 换算率

    private int algorithm = -1 ; // 算法

    private int sort = -1 ; // 排序

    private int isEnable = -1 ; // 是否启用

}
