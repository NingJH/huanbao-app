package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbLink {
    private Integer id;

    private Integer parentId;

    private String name;

    private String url;

    private Integer type;

    private Date date;

    private String remakes;

    private Integer del;
}