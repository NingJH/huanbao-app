package com.cqndt.huanbao.app.pojo;

import lombok.Data;

@Data
public class HbArea {
    private Integer id;

    private Integer areaParent;

    private Integer unitId;

    private Integer areaCode;

    private Double lon;

    private Double lat;

    private Integer level;

    private String arcgistcUrl;

    private String arcgisbjUrl;

    private String areaName;
}