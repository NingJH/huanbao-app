package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Data
public class ParkRegion {
    private int id = -1 ; //id
    private int parkId = -1 ; // 园区id
    private String regionName ; // 分区名称id
    private String latLon; // 经纬度
    private String layerUrl; // 图层Url
    private int del = 0 ; // 删除标志
    private String date = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE); // 创建日期
    private String remakes ; // 备注
    private int state = 0 ; // 状态
}
