package com.cqndt.huanbao.app.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Time : 2019-09-19 18:36
 **/
@Data
public class BuildingGeneralGo {
    private Integer id;

    private Integer generalId;//一般固废暂存场id

    private String goPdfUrl;//一般工业固废去向pdf地址

    private String goPdfName;//一般工业固废去向pdf名称

    private String goIntroduction;//一般工业固废去向介绍

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date goTime;//去向时间

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addDate;//添加时间

    private Integer del;//删除标志

    private Integer currentPage;//当前页

    private Integer max;//最大条数


    private Integer page;
    private Integer size;
}
