package com.cqndt.huanbao.app.pojo;

import lombok.Data;

@Data
public class Function {
    private int id = -1 ; // 编号
    private String functionName ; // 功能名称
    private String functionDesc ; // 功能描述
    private int menuId = -1 ; // 企业id
    private int del = -1 ; // 删除标志
    private String date ; // 创建日期
    private String remakes ; // 备注
    private int state = -1 ; // 状态
    private boolean hava;

}
