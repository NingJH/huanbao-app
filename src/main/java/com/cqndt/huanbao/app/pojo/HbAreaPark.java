package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbAreaPark {
    private Integer id;

    private Integer areaId;

    private Integer parkId;

    private Date date;

    private String remakes;

    private Integer del;

    private Integer state;
}