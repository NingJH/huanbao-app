package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbUserRole {
    private Integer id;

    private Integer userId;

    private String roleId;

    private Date date;

    private String remakes;

    private Integer del;

    private Integer state;
}