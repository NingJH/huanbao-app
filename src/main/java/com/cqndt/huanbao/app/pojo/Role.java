package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Role {
    private int id = -1 ; // 编号
    private String roleName ; // 角色名
    private String roleDes ; // 角色描述
    private int type = -1 ; // 类型
    private String date ; // 增加时间
    private int state = -1 ; // 状态
    private int del = -1 ; // 删除标志
    private String remark ; // 备注
    private List<Integer> menuId;//菜单Id
    private int limitType;
}
