package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbMenuRole {
    private Integer id;

    private String menuId;

    private String roleId;

    private Date date;

    private String remakes;

    private Integer del;

    private Integer state;
}