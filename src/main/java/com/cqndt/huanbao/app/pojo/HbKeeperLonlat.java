package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-06-19 09:32
 **/
@Data
public class HbKeeperLonlat {
    private Integer id;
    private Integer keeperId;
    private String lon;
    private String lat;
    private Date date;
    private Integer del;
    private String remarks;
    private Integer userId;
}
