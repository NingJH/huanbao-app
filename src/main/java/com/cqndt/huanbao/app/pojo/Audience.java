package com.cqndt.huanbao.app.pojo;

import lombok.Data;

/**
 * 类的详细说明
 *
 * @author zhjj
 * @version 1.00
 * Date:2018/04/09 14:23
 */
@Data
public class Audience {
    private String issuer;
    private String base64Secret;//基于openid和盐 sha1
    private String audience;
    private long TTLMillis;
    private String sessionId;

}
