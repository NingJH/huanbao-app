package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbQualityEnclosure {
    private Integer id;

    private Integer parkId;

    private String reportNum;

    private String reportName;

    private String folderUrl;

    private Date monitorDate;

    private Date date;

    private String remarks;

    private Integer del;
}