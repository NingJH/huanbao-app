package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class BuildingFocusEnergy {
    private Integer id;
    private String img;
    private String content;
    private String processContent;
    private Integer buildId;
    private Date time;
    private Integer del;
    private List<BuildingFocusEnergyImg> imgInfo;



    private Integer parkId;//园区id
    private Integer regionId;//分区id
    private String name;//设施名称
    private String centerLonLat;//中心点经纬度
    private String borderLonLat;//边界经纬度
}
