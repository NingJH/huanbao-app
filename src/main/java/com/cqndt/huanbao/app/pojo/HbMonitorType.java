package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbMonitorType {
    private Integer id;

    private Integer monitorType;

    private String monitorTypeName;

    private Integer pointType;

    private String pointTypeName;

    private Date date;

    private String remarks;

    private String del;
}