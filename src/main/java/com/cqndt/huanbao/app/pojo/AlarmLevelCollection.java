package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.List;

@Data
public class AlarmLevelCollection {
    private int totalCount=0;
    private List<AlarmLevel> entityList;
}
