package com.cqndt.huanbao.app.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Time : 2019-09-18 14:30
 **/
@Data
public class BuildingAccidentPoolAnnal {
    private Integer id;

    private Integer accidentPoolId;//事故池id

    private String annal;//记录

    private Integer type;//1历史进水，2历史出水

    private String pdfUrl;//pdf地址

    private String pdfName;//pdf文件名

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date date;//记录时间

    private Date addDate;//添加时间

    private String remarks;//备注

    private Integer del;//删除标志

    private Integer page;
    private Integer size;
    private String startTime;
    private String endTime;
}
