package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 类的详细说明
 *
 * @author zhjj
 * @version 1.00
 * Date:2018/04/10 17:24
 */
@Data
public class JwtToken {
    private String token;
    private long expireTime;
    private Role role;
    private int userId;
    private String name;
//    List<Function> functions;
    private List<Integer> menus;
    private String sessionId;

    private String phone;
    private List<Map<String,Object>> park;
    private List<Integer> gisMenus;
}
