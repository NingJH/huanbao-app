package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class BuildingRainSewageConverter {
    private Integer id;
    private Integer buildId;
    private String content;
    private String centerLonLat;
    private Date uploadTime;
    private String name;
    private String imgUrl;
    private Integer del;
    private String borderLonLat;

    private Integer parkId;//园区id
    private Integer regionId;//分区id

    private List<PdfInfo> pdfs;
}
