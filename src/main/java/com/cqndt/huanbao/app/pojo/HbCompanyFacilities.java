package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbCompanyFacilities {
    private Integer id;

    private Integer companyId;

    private String imgUrl;

    private String content;

    private Date date;

    private String remarks;

    private Integer del;
}