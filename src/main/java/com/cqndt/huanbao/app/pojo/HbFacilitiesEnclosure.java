package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbFacilitiesEnclosure {
    private Integer id;

    private Integer companyId;//企业id

    private Date date;//创建时间

    private String fileUrl;//文件路径

    private Integer del;//删除标志

    private String remarks;//备注

    private String fileName;//文件名称

    private Integer facilitiesId;//设施id

    private Integer state;//状态 1 审核通过 2 审核不通过 0 待审核
}
