package com.cqndt.huanbao.app.pojo;


import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class BuildingRainSewage {
    private Integer id;

    private Integer type;//类型

    private String content;//文本内容

    private String lonLat;//经纬度

    private Date uploadTime;//上传时间

    private String name;//名称

    private Integer buildId;//环保设施id

    private Integer del;//删除状态

    private Integer parkId;//园区id
    private Integer regionId;//分区id

    private List<PdfInfo> pdfs;
}
