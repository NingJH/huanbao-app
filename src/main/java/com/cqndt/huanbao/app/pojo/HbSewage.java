package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class  HbSewage{
    private Integer id;

    private Integer parentId;

    private String typeName;

    private Integer type;

    private Date date;

    private String remarks;

    private Integer del;

    private String content;

    private String imgUrl;

    private String unit;
}