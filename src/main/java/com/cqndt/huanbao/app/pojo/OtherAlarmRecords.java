package com.cqndt.huanbao.app.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class OtherAlarmRecords {
    private int id = -1 ; // 编号
    private Integer alarmId;
    private Integer userId;
    private Integer isRead;
    private String lat_lon ; // 经纬度
    private String phone ; // 电话
    private String content ; // 内容
    private int type = -1 ; // 类型1:一键告警
    private int pushType = -1 ; // 推送类型 1:预警 2:告警
    private int parkId = -1 ; // 园区id
    private int regionId = -1 ; // 分区id
    private int deal_status = -1 ; // 处置状态1未处理；2正在处理；3已处理
    private Date alarm_date ; // 增加时间
    private int state = -1 ; // 状态
    private int del = -1 ; // 删除标志
    private String remake ; // 备注
    private String remakes; // 告警处理备注
    private String name ; // 告警标题
    private int alarmLevel = -1; // 告警等级
    private int dealUserId = -1; // 处理告警用户id
    private String dealDate; // 处理告警时间
    private String uuid;//唯一id

    private Integer userType;

    public OtherAlarmRecords(Alarm alarm,int parkId,Integer regionId){
        this.isRead = 0;
        this.alarmId = alarm.getId();
        this.content = alarm.getContent();
        this.lat_lon = alarm.getLon()+","+alarm.getLat();
        this.phone = alarm.getPhone();
        this.pushType = alarm.getPushType();
        this.type = 1;
        this.remake = alarm.getRemakes();
        this.parkId = parkId;
        this.regionId = regionId;
        this.alarm_date = alarm.getAlarmDate();
        this.alarmLevel = alarm.getLevel();
        this.name = alarm.getName();
    }
    public OtherAlarmRecords(Alarm alarm,int parkId){
        this.isRead = 0;
        this.alarmId = alarm.getId();
        this.content = alarm.getContent();
        this.lat_lon = alarm.getLon()+","+alarm.getLat();
        this.phone = alarm.getPhone();
        this.pushType = alarm.getPushType();
        this.type = 1;
        this.remake = alarm.getRemakes();
        this.parkId = parkId;
        this.alarm_date = alarm.getAlarmDate();
        this.alarmLevel = alarm.getLevel();
        this.name = alarm.getName();
    }
}
