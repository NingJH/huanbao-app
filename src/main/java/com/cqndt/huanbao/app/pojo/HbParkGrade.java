package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbParkGrade {
    private Integer id;

    private Integer parkId;

    private String grade;

    private Double maxScore;

    private Double minScore;

    private Date date;

    private String remarks;

    private Integer del;
}