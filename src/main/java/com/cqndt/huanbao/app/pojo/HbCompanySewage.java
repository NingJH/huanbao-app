package com.cqndt.huanbao.app.pojo;

import lombok.Data;

@Data
public class HbCompanySewage {
    private Integer id;

    private Integer companyId;

    private Integer sewageId;

    private Double threshould;

    private Integer del;

    private String unit;
}