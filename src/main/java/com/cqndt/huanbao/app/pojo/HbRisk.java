package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbRisk {
    private Integer id;

    private Integer companyId;//企业id

    private Integer type;//类型:0水风险源 1大气风险源 2土壤风险源 3地下水风险源

    private String url;//地址

    private Date date;//创建时间

    private String remakes;//备注

    private Integer del;//删除标志

    private String content;//内容
}