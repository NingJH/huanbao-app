package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbUser {
    private Integer id;// 编号

    private String name;// 姓名

    private String username;// 用户名

    private String password;// 密码

    private String address;// 地址

    private String phone;// 电话

    private Integer type;// 类型

    private Date date;// 增加时间

    private Integer state;// 状态

    private Integer del;// 删除标志

    private String remakes;// 备注

    private Role role;//角色

    private int roleId = -1;//角色id

    private int parkId = -1 ;//园区id

    private int isEnable = -1;//是否启用、0，启用，1，未启用

    private String qq;//qq号

    private int isQqdisplay;//qq号是否展示

    private String token;

}