package com.cqndt.huanbao.app.pojo;

import lombok.Data;

@Data
public class PdfInfo {
    private String pdfUrl;
    private String pdfName;
}
