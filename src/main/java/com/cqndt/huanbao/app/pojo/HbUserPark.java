package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbUserPark {
    private Integer id;

    private Integer userId;

    private Integer parkId;

    private Date date;

    private String remakes;

    private Integer del;

    private Integer state;

}