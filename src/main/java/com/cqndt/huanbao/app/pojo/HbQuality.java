package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbQuality {
    private Integer id;

    private Integer parkId;

    private String imgUrl;

    private String head;

    private String content;

    private Date date;

    private String remarks;

    private Integer del;

}