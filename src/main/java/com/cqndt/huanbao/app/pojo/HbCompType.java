package com.cqndt.huanbao.app.pojo;

import lombok.Data;

@Data
public class HbCompType {
    private Integer id;

    private Integer parkId;

    private Integer companyId;

    private Integer companyTypeId;

    private Integer del;
}