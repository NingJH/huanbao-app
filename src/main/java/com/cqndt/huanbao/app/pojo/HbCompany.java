package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class HbCompany {
    private Integer id;

    private Integer parkId;

    private String name;

    private String imgUrl;

    private String latLon;

    private String type;
    private Double score;

    private String info;

    private String address;//地理位置

    private Date date;

    private String remakes;//重点企业一般企业

    private Integer del;

    private Integer userId;

    private Integer eiaStatus;

    private Integer checkStatus;

    private Integer licenceStatus;

    private String centerLatLon;

    private String borderLatLon;

    private Integer accountId;

    private String uuid;

    private String introduction;

    private String types;//行业类型分类id

    private Integer regionId;//分区id

    private Integer waterTypeId;//企业水风险等级

    private Integer airTypeId;//企业大气风险等级

    private Integer cleanTypeId;//清洁生产水平等级

    private String typeCode;//单位标识

    private String threshould;//预警阀值

    private String unit;//单位

    private List<HbCompanySewage> companySewageList;
    private String alarmUserId;

    private String regionName;







    private int currentPage;
    private int max;
    private String searchName;


    private Integer udwaterTypeId;
    private Integer soilTypeId;



}