package com.cqndt.huanbao.app.pojo;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

/**
 * 类的详细说明
 *
 * @author zhjj
 * @version 1.00
 * Date:2018/04/09 14:29
 */
public class JwtHelper {
    /**
     * 解析jwt
     */
    public static Claims parseJWT(String jsonWebToken, String base64Security) {
        try {
            return Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(base64Security))
                    .parseClaimsJws(jsonWebToken).getBody();
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * @param userId   用户id 负载中
     * @param audience 客户端id 接受对象
     * @return
     */
    public static JwtToken createJWT(String userId, String roleId,String sessionId,String username,
                                     Audience audience) {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //生成签名密钥
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(audience.getBase64Secret());
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        JwtToken jwtToken;
        jwtToken = new JwtToken();
        long expMillis = nowMillis + audience.getTTLMillis();
        Date exp = new Date(expMillis);
        jwtToken.setExpireTime(expMillis);


        //添加构成JWT的参数
        JwtBuilder builder = Jwts.builder().setHeaderParam("typ", "JWT")
                .claim("userId", userId)
                .claim("roleId", roleId)
                .claim("sessionId", sessionId)
                .claim("name", username)
                .setIssuer(audience.getIssuer())
                .setAudience(audience.getAudience())
                .setExpiration(exp).setNotBefore(now)
                .signWith(signatureAlgorithm, signingKey);
        //添加Token过期时间

        String jwt = builder.compact();
        jwtToken.setToken(jwt);
        //生成JWT
        return jwtToken;
    }
}
