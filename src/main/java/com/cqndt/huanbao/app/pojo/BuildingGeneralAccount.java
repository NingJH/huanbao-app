package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

/**
 * @Time : 2019-09-20 16:21
 **/
@Data
public class BuildingGeneralAccount {
    private Integer id;

    private Integer generalId;//一般固废暂存场id

    private String pdfUrl;//台账pdf路径

    private String pdfName;//台账pdf名称

    private Date addDate;//添加时间

    private Integer del;//删除标志


    private Integer page;
    private Integer size;
}
