package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Menu {
    private int id = -1 ; // 编号
    private String menuName ; // 菜单名
    private String date ; // 增加时间
    private int state = -1 ; // 状态
    private int del = -1 ; // 删除标志
    private int parentId = -1 ; // 父级id
    private String remark ; // 备注
    private List<Menu> childMenu;
    private String icon;
    private String path;
    private int type;
    private int limitType;
    private List<Function> functions;
    private int isPark;

}
