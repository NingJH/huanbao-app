package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

/**
 * @Time : 2019-09-20 16:18
 **/
@Data
public class BuildingGeneralMeasure {
    private Integer id;

    private Integer generalId;//一般固废暂存场id

    private String measureImgUrl;//防护措施图片地址

    private String measureImgIntroduction;//防护措施介绍

    private Date addDate;//添加时间

    private Integer del;//删除标志
}
