package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbNotice {
    private Integer id;

    private Integer parkId;//园区id

    private Integer type;//公告类型（1园区通知；2企业公告；3法律法规；4环保政策；5行业政策）'

    private String name;//名称

    private String content;//内容

    private Date date;//日期

    private Integer operatorId;//操作人id

    private String username;//操作人

    private String remarks;//标识

    private Integer del;//删除标识
}