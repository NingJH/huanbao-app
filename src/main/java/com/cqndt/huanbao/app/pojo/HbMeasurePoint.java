package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-31 15:57
 **/
@Data
public class HbMeasurePoint {
    private Integer id;

    private String name;//测点名称

    private String lon;//经度

    private String lat;//纬度

    private Integer target;//是否达标 1达标 2未达标

    private Date date;//创建时间

    private String remake;//备注

    private Integer state;//状态

    private Integer del;//删除标志

    private Integer parkId;//园区id

    private String latLon;//经纬度

    private Integer regionId;//分区id

    private String regionName;//分区名称

    private Integer measurePointType;//测点类型 1: 大气环境监测点  2: 地表水环境监测点 3: 声环境监测点   4: 地下水环境监测点     5: 土壤环境监测点
}
