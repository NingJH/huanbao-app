package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbMonitorPoint {
    private Integer id;

    private Integer monitorTypeId;

    private Integer parkId;

    private String name;

    private String imgUrl;

    private Double lat;

    private Double lon;

    private Date date;

    private String address;

    private String remarks;

    private String del;
}