package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbFacilitiesDetail {
    private Integer id;

    private Integer facilitiesId;

    private String processName;

    private String imgUrl;

    private String processContent;

    private Date date;

    private String remakes;

    private Integer del;
}