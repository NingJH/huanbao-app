package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

/**
 * Create Njh
 * @Time : 2019-09-12 10:12
 **/
@Data
public class BuildingInterceptionDam {
    private Integer id;

    private Integer buildingId;//环保设施id

    private String name;//拦截坝名称

    private String imgUrl;//图片地址

    private String introduction;//介绍

    private String centerLonLat;//中心点经纬度

    private String borderLonLat;//边界经纬度

    private Date addDate;//添加时间

    private String remarks;//备注

    private Integer del;//删除标志


    private Integer parkId;//园区id
    private Integer regionId;//分区id

}
