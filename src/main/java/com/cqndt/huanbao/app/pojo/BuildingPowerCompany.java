package com.cqndt.huanbao.app.pojo;

import lombok.Data;

@Data
public class BuildingPowerCompany {
    private Integer id;
    private String startTime;
    private String endTime;
    private String companyName;
    private String powerValue;
    private Integer type;
    private Integer feId;
    private Integer del;

    private Integer currentPage;
    private Integer max;

    private Integer page;
    private Integer size;

}
