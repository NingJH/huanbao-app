package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class BuildingSewageTreatmenPlants {
    private Integer id;
    private String equipmentImg;//设施图片
    private String equipmentIntro;//设施介绍
    private String processImg;//工艺流程图
    private String processContent;//工艺流程图内容
    private String processIntro;//工艺流程介绍
    private String sewageImg;//排污口图片
    private String sewageContent;//排污口图片内容
    private String sewageIntro;//排污口介绍
    private String unitImg;//单位图片
    private String unitContent;//单位图片内容
    private String unitIntro;//单位介绍
    private Date time;
    private Integer del;
    private Integer buildId;

    private Integer parkId;//园区id
    private Integer regionId;//分区id
    private String name;//设施名称
    private String centerLonLat;//中心点经纬度
    private String borderLonLat;//边界经纬度
}

