package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.List;

@Data
public class RoleFunction {
    private int id = -1 ; // 编号

    private int functionId = -1 ; // 功能id

    private List<Integer> functionIds; // 功能id

    private int roleId = -1 ; // 角色id

    private int del = -1 ; // 删除标志

    private String date ; // 创建日期

    private String remakes ; // 备注

    private int state = -1 ; // 状态

}
