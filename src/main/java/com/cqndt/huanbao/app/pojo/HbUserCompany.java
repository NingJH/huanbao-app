package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbUserCompany {
    private Integer id;

    private Integer userId;//用户id

    private Integer parkId;//园区id

    private Integer companyId;//企业id

    private Date date;//创建时间

    private String remakes;//备注

    private Integer del;//删除标志

    private Integer state;//状态

    private Integer regionId;//分区id
}