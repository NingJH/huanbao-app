package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class Alarm {
    private int id;

    private Integer userId;

    private Integer parkId;

    private Integer regionId;

    private String name;

    private int level;

    private String lon;

    private String lat;

    private String phone;

    private String content;

    private Date alarmDate;

    private Date addDate;

    private Integer isRead;

    private int del;

    private String remakes;

    private int pushType;//推送1，表示预警

    private String startTime;
    private String endTime;
}
