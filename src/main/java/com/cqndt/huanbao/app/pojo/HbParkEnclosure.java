package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbParkEnclosure {
    private Integer id;

    private Integer parkId;

    private String name;

    private Integer type;

    private Date date;

    private String remakes;

    private Integer del;

    private Integer state;

    private String url;
}