package com.cqndt.huanbao.app.pojo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.Enumeration;
import java.util.Map;
import java.util.Vector;


public class MyHttpRequestWrapper extends HttpServletRequestWrapper {
    private Map<String, String> parameterMap;

    public MyHttpRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public Map getParameterMap() {
        if (this.parameterMap == null) {
            return super.getParameterMap();
        }
        return this.parameterMap;
    }

    public void setParameterMap(Map parameterMap) {
        this.parameterMap = parameterMap;
    }

    public Enumeration getParameterNames() {
        Vector l = new Vector(parameterMap.keySet());
        return l.elements();
    }

    public String[] getParameterValues(String name) {
        Object v = parameterMap.get(name);
        if (v == null) {
            return null;
        } else if (v instanceof String[]) {
            return (String[]) v;
        } else if (v instanceof String) {
            return new String[]{(String) v};
        } else {
            return new String[]{v.toString()};
        }
    }

    public String getParameter(String name) {
        Object v = parameterMap.get(name);
        if (v == null) {
            return null;
        } else if (v instanceof String[]) {
            String[] strArr = (String[]) v;
            if (strArr.length > 0) {
                return strArr[0];
            } else {
                return null;
            }
        } else if (v instanceof String) {
            return (String) v;
        } else {
            return v.toString();
        }
    }
//    @Override
//    public String getParameter(String name) {
//        if(parameterMap == null) {
//            return super.getParameter(name);
//        }
//        return parameterMap.get(name);
//    }


}
