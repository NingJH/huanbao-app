package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbKeeper {
    private Integer id;

    private String name;

    private String phone;

    private String address;

    private String remakes;

    private Integer del;

    private Integer state;

    private Date date;
}