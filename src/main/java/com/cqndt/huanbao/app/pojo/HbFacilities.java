package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbFacilities {
    private Integer id;

    private Integer parkId;

    private String head;

    private String imgUrl;

    private String content;

    private Date date;

    private String remakes;

    private Integer del;

    private Integer regionId;

    private String regionName;

}