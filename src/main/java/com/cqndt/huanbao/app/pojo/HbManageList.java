package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbManageList {
    private Integer id;

    private Integer companyId;

    private Integer userId;

    private Integer type;

    private String imgUrl;

    private Date date;

    private String remakes;

    private Integer del;

    private String file;

    private String fileName;

    private String content;

}