package com.cqndt.huanbao.app.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * Create Njh
 * @Time : 2019-11-03 15:30
 **/
@Data
public class Device {
    private Integer id;
    private Integer parkId;//所属园区
    private Integer regionId;//所属分区
    private String deviceNo;//设备编号
    private String deviceName;//设备名称
    private String deviceType;//设备类型
    private String deviceUnit;//设备单位
    private String deviceImg;//设备图片
    private String address;//监测地址
    private String lon;//经度
    private String lat;//纬度
    private Integer isOnline;//是否在在线（0,在线，1,离线）
    private String monitorType;//监测类型
    private String monitorTypeName;//监测类型名称
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date monitorTime;//监测时间
    private Date createTime;//添加时间
    private Date modifyTime;//修改时间
    private int del;//删除标志（0,未删，1,已删）
    private Integer buildingId;//设施id
    private String maxValue;//告警最大值
    private String minValue;//告警最小值
    private String isFault;//告警最小值
    private String frequency;//数据上传频率
    private Integer deviceFor;//设备所属(0园区设备 1企业设备)
    private Integer companyId;//设备所属公司Id
    private String companyName;//所属公司名称
    private int currentPage;
    private int max;
    private String buildingName;
    private String parkName;
    private String regionName;
    private String deviceIds;
    private String startTime;
    private String endTime;
    private Integer page;
    private Integer limit;
}
