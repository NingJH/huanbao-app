package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbContract {
    private Integer id;

    private Integer compayId;

    private Date warnDate;

    private Date alarmDate;

    private String remarks;

    private String del;

    private String name;

    private String url;

    private Date uploadTime;

    private Integer status;

    private Integer dealStatus;
}