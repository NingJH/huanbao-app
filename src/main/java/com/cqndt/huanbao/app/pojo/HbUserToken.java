package com.cqndt.huanbao.app.pojo;

import lombok.Data;

@Data
public class HbUserToken {

    private Integer id;

    private String userName;

    private Integer type;

    private String phone;

    private String token;
}
