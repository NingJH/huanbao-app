package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbAlarm {
    private Integer id;

    private Integer userId;

    private Integer parkId;

    private Integer regionId;

    private String name;

    private Integer level;

    private String lon;

    private String lat;

    private String phone;

    private String content;

    private Date alarmDate;

    private Date addDate;

    private Integer isRead;

    private Integer del;

    private String remakes;
}