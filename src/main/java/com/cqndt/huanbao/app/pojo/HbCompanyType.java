package com.cqndt.huanbao.app.pojo;

import lombok.Data;

@Data
public class HbCompanyType {
    private Integer id;

    private Integer parentId;

    private Integer type;

    private String typeName;

    private Integer level;
}