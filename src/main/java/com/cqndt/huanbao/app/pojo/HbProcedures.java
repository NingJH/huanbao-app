package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbProcedures {
    private Integer id;

    private Integer parkId;

    private Integer companyId;

    private Integer type;

    private Integer dealStatus;

    private Date uploadDate;

    private String remakes;

    private Integer del;

    private String warnDate;

    private String alarmDate;

    private Integer status;

    private String content;

    private Integer regionId;
}