package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbPark {
    private Integer id;

    private String name;//名称

    private String unit;//管理单位

    private String latLon;//经纬度

    private String imgUrl;//图片

    private String content;//内容

    private String tel;//联系方式

    private String area;//占地面积

    private String location;//地理位置

    private String industry;//产品定位

    private String level;//单位性质、等级

    private Date monitorDate;//定期检测日期

    private String advice;//投诉建议

    private String supervision;//环保督查

    private Date date;//日期

    private String remarks;//标识

    private Integer del;//删除标识

    private Integer areaId;//区域表id

    private String borderLatLon;//边界经纬度

    private String arcgisbjUrl;//边界url

    private String arcgistcUrl;//图层url

    private String layerUrl;//图层url

    private String areaCode;//行政编号

    private Integer userId;//用户id

    private String uuid;//唯一id
}