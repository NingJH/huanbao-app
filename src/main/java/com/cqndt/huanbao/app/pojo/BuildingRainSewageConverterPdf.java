package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class BuildingRainSewageConverterPdf {
    private Integer id;
    private String pdf;
    private String name;
    private Date uploadTime;
    private Integer del;
    private Integer rscId;


    private Integer page;
    private Integer size;
}
