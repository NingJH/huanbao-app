package com.cqndt.huanbao.app.pojo;

import lombok.Data;

/**
 * Create by Intellij IDEA
 * User : wuhao
 * Mail : 863254617@qq.com
 * Time : 2019/7/24 14:18
 */
@Data
public class AlarmLevel {
    private int id = -1; // 编号
    private String name; // 报警等级名称
    private String date; // 创建时间
    private int level = -1; // 报警等级
    private String remakes; // 报警情形说明
    private int del = -1; // 删除标记
    private int state = -1; // 状态
}
