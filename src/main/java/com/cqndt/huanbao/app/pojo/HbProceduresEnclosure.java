package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbProceduresEnclosure {
    private Integer id;

    private Integer procedureId;

    private String reportNum;

    private String unit;

    private Integer type;

    private Date date;

    private String remakes;

    private Integer del;

    private Integer companyId;

    private String warnDate;

    private String alarmDate;

    private Integer status;

    private Integer dealStatus;

    private String monitorType;

    private String note;

    private String reportName;

    private String companyName;//公司名称

}