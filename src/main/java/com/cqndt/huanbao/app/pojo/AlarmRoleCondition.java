package com.cqndt.huanbao.app.pojo;

import lombok.Data;

/**
 * Create by Intellij IDEA
 * User : wuhao
 * Mail : 863254617@qq.com
 * Time : 2019/7/23 16:32
 */
@Data
public class AlarmRoleCondition {

    private int offset;
    private int max = 10;

    private int id = -1; // 编号
    private int roleId = -1; // 角色id
    private int alarmLevelId = -1; // 告警id
    private int level = -1; // 告警等级
    private String date; // 创建时间
    private String remakes; // 备注
    private int del = -1; // 删除标记
    private int state = -1; // 状态
    private int currentPage = 1; //当前页

}
