package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class BuildingFocusEnergyImg {
    private Integer id;
    private String processImg;
    private String introduction;
    private Integer feId;
    private Integer del;
    private Date time;
}
