package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbAlarmRecords {
    private Integer id;

    private Integer companyId;

    private String companyName;

    private Date alarmTime;

    private Integer dealStatus;
}