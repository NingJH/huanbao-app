package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

/**
 * Create Njh
 * @Time : 2019-09-12 10:11
 **/
@Data
public class Building {
    private Integer id;

    private Integer parkId;//园区id

    private Integer regionId;//分区id

    private Integer type;//1,应急设施，2,基础设施

    private Integer facilityType;//1,污水处理厂，2,拦截坝，3,应急事故池，4,一般工业固废暂存场，5,雨污切换阀，6,雨污管网，7,集中供能

    private String centerLonLat;//中心点经纬度

    private String borderLonLat;//边界经纬度

    private String name;//设施名称

    private Date addDate;//添加时间

    private String remarks;//备注

    private Integer del;//删除标志

    private String lonLat;//经纬度

    private int page;
    private int size;
    private String startTime;
    private String endTime;
    private String context;
}
