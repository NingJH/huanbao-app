package com.cqndt.huanbao.app.pojo;

import lombok.Data;

@Data
public class AlarmLevelCondition {
    private int offset;
    private int max = 10;

    private int id = -1; // 编号
    private String name; // 报警等级名称
    private String date; // 创建时间
    private int level = -1; // 报警等级
    private String remakes; // 报警情形说明
    private int del = -1; // 删除标记
    private int state = -1; // 状态
    private int currentPage = -1; // 当前页数
}
