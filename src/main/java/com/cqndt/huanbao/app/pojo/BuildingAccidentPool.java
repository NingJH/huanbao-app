package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Time : 2019-09-18 14:24
 **/
@Data
public class BuildingAccidentPool {
    private Integer id;

    private Integer buildingId;//环保设施id

    private String name;//应急事故池名称

    private String imgUrl;//图片地址

    private String introduction;//介绍

    private String centerLonLat;//中心点经纬度

    private String borderLonLat;//边界经纬度

    private Date addDate;//添加时间

    private String remarks;//备注

    private Integer del;//删除标志


    private Integer parkId;//园区id
    private Integer regionId;//分区id
    private List<BuildingAccidentPoolAnnal> buildingAccidentPoolAnnalList;
}
