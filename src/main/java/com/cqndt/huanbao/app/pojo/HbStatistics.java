package com.cqndt.huanbao.app.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class HbStatistics {
    private Integer id;

    private Integer companyId;

    private Integer sewageId;

    private String companyName;

    private Double emissions;

    private Double threshold;

    private Date date;

    private String remakes;

    private Integer del;

    private String unit;
}