package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.service.ManageListService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/environment/manageList")
@Api(value = "巡查排查信息", description = "巡查排查信息")
public class ManageListController extends BaseController{

    @Autowired
    ManageListService manageListService;

    @GetMapping("/getManageListByUserId")
    @ApiOperation(value = "根据用户ID查询对应的巡查排查", notes = "根据用户ID查询对应的巡查排查")
    @ApiImplicitParam(dataType = "String", name = "userId", value = "", defaultValue = "", required = true, paramType = "query")
    public AjaxResult getManageListByUserId(int page, int size, Integer userId, String name, String refreshJwtToken) {
        PageHelper.startPage(page,size);
        List<Map<String,Object>> manageList =  manageListService.getManageListByUserId(userId,name);
        PageInfo<Map<String,Object>> commandPageInfo = new PageInfo<>(manageList);
        return entityResult(commandPageInfo,refreshJwtToken,"根据用户ID查询对应的巡查排查");
    }
}
