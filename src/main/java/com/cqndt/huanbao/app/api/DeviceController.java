package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.pojo.AjaxResult;

import com.cqndt.huanbao.app.pojo.Device;
import com.cqndt.huanbao.app.service.DeviceService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/environment/device")
public class DeviceController extends BaseController {
    @Autowired
    DeviceService deviceService;
    @GetMapping("/listParkDevice")
    @ApiOperation(value = "环保管家——查询园区设备列表", notes = "环保管家——查询园区设备列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parkId", value="园区id", dataType = "Integer", paramType="query"),
            @ApiImplicitParam(name = "regionId", value="分区id", dataType = "Integer",required = true,paramType="query"),
               })
    public AjaxResult getParkDeviceList(Integer parkId, Integer regionId, Integer userId, String refreshJwtToken) {

        return entityResult(deviceService.getParkDeviceList(parkId,regionId,userId),refreshJwtToken,"查询园区设备列表");
    }
    @GetMapping("/listCompanyDevice")
    @ApiOperation(value = "环保管家——查询企业设备列表", notes = "环保管家——查询企业设备列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parkId", value="园区id", dataType = "Integer", paramType="query"),
            @ApiImplicitParam(name = "regionId", value="分区id", dataType = "Integer",required = true,paramType="query"),
    })
    public AjaxResult getCompanyDeviceList(Integer parkId, Integer regionId, Integer userId, String refreshJwtToken) {

        return entityResult(deviceService.getCompanyDeviceList(parkId, regionId, userId),refreshJwtToken,"查询企业设备列表");
    }

}
