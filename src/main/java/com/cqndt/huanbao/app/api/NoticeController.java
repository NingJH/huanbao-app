package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.service.NoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-29 11:20
 **/
@RestController
@RequestMapping("/environment/notice")
@Api(value = "园区公告", description = "园区公告")
public class NoticeController extends  BaseController{

    @Autowired
    NoticeService noticeService;

    @PostMapping("/selectNotice")
    @ApiOperation(value = "园区公告", notes = "园区公告")
    @ApiImplicitParam(dataType = "String", name = "userId", value = "", defaultValue = "", required = true, paramType = "query")
    public AjaxResult selectNotice(int userId, String refreshJwtToken) {
        return entityResult(noticeService.selectNotice(userId),refreshJwtToken,"根据用户ID查询对应的园区");
    }
}
