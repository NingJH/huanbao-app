package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.pojo.HbPark;
import com.cqndt.huanbao.app.service.HbParkService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/environment/park")
@Api(value = "园区信息", description = "园区信息")
public class ParkController extends BaseController{

    @Autowired
    private HbParkService hbParkService;

    @GetMapping("/getParkInfoByUserId")
    @ApiOperation(value = "根据用户ID查询对应的园区", notes = "根据用户ID查询对应的园区")
    @ApiImplicitParam(dataType = "String", name = "userId", value = "", defaultValue = "", required = true, paramType = "query")
    public AjaxResult getParkInfoByUserId(int userId, String refreshJwtToken) {
        return entityResult(hbParkService.selectByUserId(userId),refreshJwtToken,"根据用户ID查询对应的园区");
    }

    @GetMapping("/getParkInfoByParkId")
    @ApiOperation(value = "根据园区ID查询对应的园区信息", notes = "根据园区ID查询对应的园区信息")
    @ApiImplicitParams({
    @ApiImplicitParam(dataType = "integer", name = "id", value = "", defaultValue = "", paramType = "query"),
    @ApiImplicitParam(dataType = "String", name = "refreshJwtToken", value = "", defaultValue = "", paramType = "query")
    })
    public AjaxResult getParkInfoByParkId(int id,String refreshJwtToken){
        HbPark hbPark=hbParkService.selectParkById(id);
        return  entityResult(hbPark,refreshJwtToken,"根据园区ID查询对应的园区信息");
    }

    @GetMapping("/getReportById")
    @ApiOperation(value = "根据园区ID,分区ID查询对应的环保手续", notes = "根据园区ID,分区ID查询对应的环保手续")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "integer", name = "page", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "integer", name = "size", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "integer", name = "parkId", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "integer", name = "regionId", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "context", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "refreshJwtToken", value = "", defaultValue = "", paramType = "query")
    })
    public AjaxResult getReportById(int page,int size,Integer parkId,Integer regionId,String context,String refreshJwtToken,Integer type){
        PageHelper.startPage(page,size);
        List<Map<String,String>> list=hbParkService.selectReportById(parkId,regionId,context,type);
        return  entityResult(list,refreshJwtToken,"根据园区ID,分区ID查询对应的环保手续");
    }

    @GetMapping("/getGisLayer")
    @ApiOperation(value = "根据园区ID,分区ID查询对应的环保手续", notes = "根据园区ID,分区ID查询对应的环保手续")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "integer", name = "parkId", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "integer", name = "regionId", value = "", defaultValue = "", paramType = "query"),
    })
    public AjaxResult getGisLayer(Integer parkId,Integer regionId,String refreshJwtToken){
        return  entityResult(hbParkService.getGisLayer(parkId,regionId),refreshJwtToken,"查询图层");
    }

    @GetMapping("/getRegionLonLat")
    @ApiOperation(value = "根据分区ID查询分区边界经纬度", notes = "根据分区ID查询分区边界经纬度")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "integer", name = "regionId", value = "", defaultValue = "", paramType = "query")
    })
    public AjaxResult getRegionLonLat(Integer regionId,String refreshJwtToken,Integer parkId){
        return  entityResult(hbParkService.getRegionLonLat(regionId,parkId),refreshJwtToken,"查询分区边界经纬度");
    }

    @GetMapping("/getFuJian")
    @ApiOperation(value = "根据企业ID查询监测附件", notes = "根据企业ID查询监测附件")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "integer", name = "companyId", value = "", defaultValue = "", paramType = "query")
    })
    public AjaxResult getRegionLonLat(Integer page,Integer size,Integer parkId,Integer regionId,String context,String refreshJwtToken){
        PageHelper.startPage(page,size);
        List<Map<String,Object>> list=hbParkService.fuJianList(parkId,regionId,context);
        return  entityResult(list,refreshJwtToken,"根据企业ID查询监测附件");
    }


    /**
     * 根据园区id查询企业环保手续
     * @return
     */
    @ApiOperation(value = "园区环保手续查看",notes = "园区环保手续查看")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value = "园区id",required = true,dataType ="Integer",paramType = "query")
    })
    @RequestMapping(value = "/selectProcedureExamine",method = RequestMethod.GET)
    public AjaxResult selectProcedureExamine(Integer page,Integer size,Integer parkId,Integer type,String context,String refreshJwtToken){
        PageHelper.startPage(page,size);
        List<Map<String,Object>> hbParkList = hbParkService.selectProcedureExamine(parkId,type,context);
//        PageInfo<Map<String,Object>> commandPageInfo = new PageInfo<>(hbParkList);
        return entityResult(hbParkList,refreshJwtToken,"根据企业ID查询监测附件");
    }
}
