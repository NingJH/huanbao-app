package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.aop.serivce.OperationAspect;
import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.pojo.BuildingGeneralGo;
import com.cqndt.huanbao.app.pojo.BuildingRainSewageConverter;
import com.cqndt.huanbao.app.pojo.BuildingRainSewageConverterPdf;
import com.cqndt.huanbao.app.service.IBuildingRainSewageConverterService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.List;
import java.util.Map;

@RestController
@Api(description = "雨污切换阀")
@RequestMapping(value = "/environment/building")
public class BuildingRainSewageConverterController extends BaseController{

    @Autowired
    private IBuildingRainSewageConverterService iBuildingRainSewageConverterService;

    @ApiResponses(value = { @ApiResponse(code = 200 ,message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询雨污转换阀台账",notes = "查询雨污转换阀台账")
    @RequestMapping(value = "/selectRainSewageConverterAccount",method = RequestMethod.POST)
    @OperationAspect(desc = "查询雨污转换阀台账",module = "园区环保设施模块")
    public AjaxResult selectRainSewageConverterAccount(BuildingRainSewageConverterPdf buildingRainSewageConverterPdf, String refreshJwtToken){
        PageHelper.startPage(buildingRainSewageConverterPdf.getPage(),buildingRainSewageConverterPdf.getSize());
        List<BuildingRainSewageConverterPdf> list=iBuildingRainSewageConverterService.selectRainSewageConverterAccount(buildingRainSewageConverterPdf);
        PageInfo<BuildingRainSewageConverterPdf> info=new PageInfo<>(list);
        return pageResult(buildingRainSewageConverterPdf.getPage(),buildingRainSewageConverterPdf.getSize(),Integer.parseInt(String.valueOf(info.getTotal())),info.getList(),refreshJwtToken);
    }
}
