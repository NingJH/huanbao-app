package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.pojo.Building;
import com.cqndt.huanbao.app.service.IBuildingService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * @Time : 2019-09-12 15:02
 **/
@RestController
@Api(description = "园区环保设施")
@RequestMapping(value = "/environment/building")
public class BuildingController extends BaseController{

    @Autowired
    private IBuildingService buildingService;

    @ApiResponses(value = { @ApiResponse(code = 200, message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询园区环保设施",notes = "查询园区环保设施")
    @RequestMapping(value = "/selectBuilding",method = RequestMethod.POST)
    public AjaxResult selectBuilding(Building building, String refreshJwtToken, int userId){
        PageHelper.startPage(building.getPage(),building.getSize());
        List<Map<String,Object>> manageList1 = buildingService.selectBuilding(building,userId);
        PageInfo<Map<String,Object>> commandPageInfo = new PageInfo<>(manageList1);
        return pageResult(building.getPage(),building.getSize(), Integer.parseInt(String.valueOf(commandPageInfo.getTotal())),commandPageInfo.getList(),refreshJwtToken);
    }



    @ApiResponses(value = { @ApiResponse(code = 200, message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询园区环保设施",notes = "查询园区环保设施")
    @RequestMapping(value = "/selectBuild",method = RequestMethod.POST)
    public AjaxResult selectBuild(Building building, String refreshJwtToken){
        System.out.println("进去环保设施接口》》》》》》》》》》");
        List<Map<String,Object>> manageList1 = buildingService.selectBuild(building);

        return entityResult(manageList1,refreshJwtToken,"查询园区环保设施");
    }



}
