package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.aop.serivce.OperationAspect;
import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.pojo.BuildingFocusEnergy;
import com.cqndt.huanbao.app.pojo.BuildingPowerCompany;
import com.cqndt.huanbao.app.pojo.BuildingRainSewageConverterPdf;
import com.cqndt.huanbao.app.service.BuildingFocusEnergyService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.List;
import java.util.Map;

@RestController
@Api(description = "集中供能")
@RequestMapping(value = "/environment/building")
public class BuildingFocusEnergyController extends BaseController{

    @Autowired
    private BuildingFocusEnergyService buildingRainSewageConverter;

    @ApiResponses(value = { @ApiResponse(code = 200 ,message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询集中供能工艺流程",notes = "查询集中供能工艺流程")
    @ApiImplicitParams({
            @ApiImplicitParam(name="feId",value = "集中供能id",dataType ="BuildingFocusEnergy",paramType = ""),
    })
    @RequestMapping(value = "/selectRainFocusEnergyProcess",method = RequestMethod.POST)
    @OperationAspect(desc = "查询集中供能工艺流程",module = "园区环保设施模块")
    public AjaxResult selectRainFocusEnergyProcess(Integer feId, String refreshJwtToken){
        return entityResult(buildingRainSewageConverter.selectRainFocusEnergyProcess(feId),refreshJwtToken,"查询");
    }

    @ApiResponses(value = { @ApiResponse(code = 200 ,message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询集中供能产能企业",notes = "查询集中供能产能企业")
    @ApiImplicitParams({
            @ApiImplicitParam(name="feId",value = "集中供能id",dataType ="BuildingFocusEnergy",paramType = ""),
    })
    @RequestMapping(value = "/selectRainFocusEnergyOutIn",method = RequestMethod.POST)
    @OperationAspect(desc = "查询集中供能产能企业",module = "园区环保设施模块")
    public AjaxResult selectRainFocusEnergyOutIn(BuildingPowerCompany buildingPowerCompany, String refreshJwtToken){
        PageHelper.startPage(buildingPowerCompany.getPage(),buildingPowerCompany.getSize());
        List<Map<String,Object>> list=buildingRainSewageConverter.selectRainFocusEnergyOutIn(buildingPowerCompany);
        PageInfo<Map<String,Object>> info=new PageInfo<>(list);
        return pageResult(buildingPowerCompany.getPage(),buildingPowerCompany.getSize(),Integer.parseInt(String.valueOf(info.getTotal())),info.getList(),refreshJwtToken);
    }

}
