package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.aop.serivce.OperationAspect;
import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.service.IBuildingInterceptionDamService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;

/**
 * @Time : 2019-09-18 09:23
 **/
@RestController
@Api(description = "拦截坝")
@RequestMapping(value = "/environment/building")
public class BuildingInterceptionDamController extends BaseController{

    @Autowired
    private IBuildingInterceptionDamService iBuildingInterceptionDamService;

    @ApiResponses(value = { @ApiResponse(code = 200, message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询拦截坝",notes = "查询拦截坝")
    @RequestMapping(value = "/selectInterceptionDam",method = RequestMethod.GET)
    @OperationAspect(desc = "查询拦截坝",module = "园区环保设施模块")
    public AjaxResult selectInterceptionDam(Integer id, String refreshJwtToken){
        return entityResult(iBuildingInterceptionDamService.selectInterceptionDam(id),refreshJwtToken,"新增");
    }
}
