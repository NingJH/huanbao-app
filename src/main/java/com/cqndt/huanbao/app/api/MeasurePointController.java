package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.service.MeasurePointService;
import com.cqndt.huanbao.app.service.StatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-31 15:25
 **/
@RestController
@RequestMapping("/environment/measurepoint")
@Api(value = "环境质量", description = "环境质量")
public class MeasurePointController extends BaseController{

    @Autowired
    private MeasurePointService measurePointService;


    @GetMapping("/getmeasurePointLonLat")
    @ApiOperation(value = "环境质量经纬度", notes = "环境质量经纬度")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "companyId", value="公司id", dataType = "Integer", paramType="query"),
            @ApiImplicitParam(name = "measurePointType", value="测点类型", dataType = "Integer",required = true,paramType="query"),
            @ApiImplicitParam(name = "type", value="类型：0巡查 1排查 2投诉及建议", dataType = "Integer",required = true,paramType="query")
    })
    public AjaxResult getmeasurePointLonLat(Integer parkId, Integer regionId, Integer typeId, String refreshJwtToken, int userId) {
        List<Map<String,Object>> measurePointList = measurePointService.getmeasurePointLonLat(parkId,regionId,typeId,userId);
        return entityResult(measurePointList,refreshJwtToken,"环境质量经纬度");
    }

    /**
     * 企业环保统计
     *
     * @param parkId 园区id
     * @return
     */
    @ApiOperation(value = "环境质量首页", notes = "环境质量首页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parkId", value = "园区id", required = false, dataType = "Integer", paramType = "query")
    })
    @RequestMapping(value = "/measureStatisticsRank", method = RequestMethod.GET)
    public AjaxResult measureStatisticsRank(Integer parkId,Integer regionId,Integer type,int userId,String refreshJwtToken) {
        return entityResult(measurePointService.measureStatisticsRank(parkId,regionId,type,userId),refreshJwtToken,"环境质量统计");
    }

}
