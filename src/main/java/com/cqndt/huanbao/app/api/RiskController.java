package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.service.RiskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-29 10:42
 **/
@RestController
@RequestMapping("/environment/risk")
@Api(value = "风险源信息", description = "风险源信息")
public class RiskController extends BaseController{

    @Autowired
    RiskService riskService;

    @GetMapping("/getRiskLonLat")
    @ApiOperation(value = "当前登录用户关联的企业位置有风险源的位置", notes = "当前登录用户关联的企业位置有风险源的位置")
    @ApiImplicitParam(dataType = "String", name = "userId", value = "", defaultValue = "", required = true, paramType = "query")
    public AjaxResult getParkInfoByUserId(Integer parkId, Integer regionId, Integer typeId,Integer waterTypeId, Integer airTypeId, Integer udwaterTypeId, Integer soilTypeId, int userId, String refreshJwtToken) {
        return entityResult(riskService.getParkInfoByUserId(parkId,regionId,typeId,waterTypeId,airTypeId,udwaterTypeId,soilTypeId,userId),refreshJwtToken,"当前登录用户关联的企业位置有风险源的位置");
    }
}
