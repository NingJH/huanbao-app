package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.pojo.HbFacilities;
import com.cqndt.huanbao.app.pojo.HbFacilitiesDetail;
import com.cqndt.huanbao.app.service.FacilitiesService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/environment/companyfacilities")
@Api(value = "企业环保设施信息", description = "企业环保设施信息")
public class CompanyFacilitiesController extends BaseController{


//    @Value("${img.url}")
//    private String imgUrl;

    @Autowired
    private FacilitiesService facilitiesService;

    @GetMapping("/facilitiesList")
    @ApiOperation(value = "园区环保设施列表", notes = "园区环保设施列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "int", name = "id", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "int", name = "regionId", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "integer", name = "page", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "integer", name = "size", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "context", value = "", defaultValue = "", paramType = "query")
    })
    public AjaxResult facilitiesList(int page,int size,Integer id,String refreshJwtToken,String context,Integer regionId) {
        PageHelper.startPage(page,size);
        List<Map<String,Object>> facilitiesList = facilitiesService.selectFacilitiesList(id,context,regionId);
        return entityResult(facilitiesList,refreshJwtToken,"园区环保设施列表");
    }

//    @GetMapping("/facilitiesDetail")
//    @ApiOperation(value = "环保设施详情", notes = "环保设施详情")
//    @ApiImplicitParam(dataType = "int", name = "id", value = "", defaultValue = "", required = true, paramType = "query")
//    public AjaxResult facilitiesDetail(Integer id,String refreshJwtToken) {
//        List<HbFacilitiesDetail> facilitiesDetail = facilitiesService.selectFacilitiesDetail(id);
//        return entityResult(facilitiesDetail,refreshJwtToken,"");
//    }
//
//    @GetMapping("/facilitiesInfo")
//    @ApiOperation(value = "环保设施信息", notes = "环保设施信息")
//    @ApiImplicitParam(dataType = "int", name = "id", value = "", defaultValue = "", required = true, paramType = "query")
//    public AjaxResult facilitiesInfo(Integer id,String refreshJwtToken) {
//        HbFacilities facilitiesList = facilitiesService.selectFacilitiesinfo(id);
//        return entityResult(facilitiesList,refreshJwtToken,"");
//    }
}
