package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.dao.HbStatisticsMapper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/environment/alarm")
@Api(description = "一键告警")
public class TestsController {
    @Autowired
    HbStatisticsMapper  hbStatisticsMapper;
    @GetMapping("/info")
    public  void  testInfo(){
        String fs1 = hbStatisticsMapper.yearTimess();
        double fs = Double.parseDouble(StringUtils.isEmpty(fs1)? "0" : fs1);
        String fs2= String.valueOf(fs);
    }

}
