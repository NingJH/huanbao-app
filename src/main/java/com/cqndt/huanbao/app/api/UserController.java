package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.dao.HbUserMapper;
import com.cqndt.huanbao.app.exception.ServiceException;
import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.pojo.HbUser;
import com.cqndt.huanbao.app.pojo.JwtToken;
import com.cqndt.huanbao.app.service.AdvancedPushService;
import com.cqndt.huanbao.app.service.UserService;
import com.cqndt.huanbao.app.util.AliPushUtil;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Api(value = "用户管理", description = "用户管理")
@Slf4j
public class UserController extends BaseController{

    @Autowired
    private UserService userService;
    @Autowired
    private HbUserMapper hbUserMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    AdvancedPushService advancedPushService;
//    @PostMapping("/login")
//    @ApiOperation(notes = "用户登录", value = "用户登录")
//    @ApiImplicitParams({
//            @ApiImplicitParam(dataType = "String", name = "username", value = "", defaultValue = "", required = true, paramType = "query"),
//            @ApiImplicitParam(dataType = "String", name = "password", value = "", defaultValue = "", required = true, paramType = "query")
//    })
//    @Log(description = "登录")
//    public AjaxResult login(String username, String password, String refreshJwtToken) {
//        Map<String, Object> map = userService.getUserInfo(username, password);
//        request.getSession().setAttribute("userId",map.get("id"));
//        request.getSession().setAttribute("name",map.get("name"));
//        if(map.get("is_enable").toString().equals("1")){
//            return AjaxResult.failResult("该账户已被锁定");
//        }
//        log.info("登录信息  : {}",map);
//        if (map != null && map.size() > 0) {
//            return entityResult(map,refreshJwtToken,"登录");
//        }
//        return entityResult(map,refreshJwtToken,"登录");
//    }


    /**
     * 用户登录接口
     * @param user 用户
     * @param response  HttpServletResponse
     * @return User
     */
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Invalid input", response = HbUser.class) })
    @ApiOperation(value = "用户登录接口",notes = "用户登录接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name="username",value = "用户名",dataType ="String",paramType = "query"),
            @ApiImplicitParam(name="password",value = "密码",dataType ="String",paramType = "query"),
    })
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public AjaxResult userLogin(HbUser user, HttpServletRequest request, HttpServletResponse response){
        try {
            JwtToken token = userService.getUserByUsernameAndPassword(user,request, response);
            token.setSessionId(request.getSession().getId());
            request.getSession().setAttribute("userId",token.getUserId());
            request.getSession().setAttribute("name",token.getName());
            //登录推送告警
            //advancedPushService.AdvancedPush((Integer) request.getSession().getAttribute("userId"));
            return entityResult(token,null,"登录");

        }catch (ServiceException e){
            return exceptionReslut(e,null);
        }
    }

    /**
     * 用户登出
     * @return User
     */
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Invalid input", response = HbUser.class) })
    @ApiOperation(value = "用户登出",notes = "用户登出")
    @RequestMapping(value = "/environment/logOut",method = RequestMethod.POST)
    public AjaxResult logOut(int userId, String refreshJwtToken){
        HbUser hbUser = hbUserMapper.getUserById(userId);
        if (stringRedisTemplate.hasKey("jwt_token:" + hbUser.getUsername() + hbUser.getType() + ":token")) {
            String oldToken = stringRedisTemplate.opsForValue().get("jwt_token:" + hbUser.getUsername() + hbUser.getType() + ":token");
            stringRedisTemplate.delete("jwt_token:" + hbUser.getUsername() + hbUser.getType() + ":token");
            if (stringRedisTemplate.hasKey("jwt_token:" + oldToken + ":secret")) {
                stringRedisTemplate.delete("jwt_token:" + oldToken + ":secret");
            }
        }
        return commonResult(1,null,"登出");
    }
}
