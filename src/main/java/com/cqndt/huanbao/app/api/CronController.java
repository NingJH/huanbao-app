package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.dao.HbUserCompanyMapper;
import com.cqndt.huanbao.app.dao.HbUserMapper;
import com.cqndt.huanbao.app.pojo.Device;
import com.cqndt.huanbao.app.pojo.HbUser;
import com.cqndt.huanbao.app.pojo.HbUserCompany;
import com.cqndt.huanbao.app.pojo.HbUserToken;
import com.cqndt.huanbao.app.util.AliPushUtil;
import com.cqndt.huanbao.app.util.JsonDateValueProcessor;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import java.util.Date;
import java.util.List;
import java.util.Map;

@EnableScheduling
@Component
@Slf4j
public class CronController {
    @Autowired
    HbUserMapper hbUserMapper;
    @Autowired
    HbUserCompanyMapper userCompanyMapper;
    @Autowired
    StringRedisTemplate stringRedisTemplate;
   // @Scheduled(cron = "0 0/30 * * * ? ")
    //@Scheduled(cron = " 0/5 * * * * ? ")
    public void AdvancedPush() {
       //获取所有app用户
        List<HbUser> userList = hbUserMapper.getUserList();
        //获取所有告警设备
        List<Device> deviceList=hbUserMapper.getDeviceLsit();
        //获取告警异常数据
        List<Map<String, String>> mapList1 = hbUserMapper.getAlarmTBS001();
        List<Map<String, String>> mapList2 = hbUserMapper.getAlarmTBS011();
        List<Map<String, String>> mapList3 = hbUserMapper.getAlarmTBS060();
        List<Map<String, String>> mapList4 = hbUserMapper.getAlarmTBS101();
        Map<String, String> alarmValue1 = hbUserMapper.getAlarmValueTBS001();
        Map<String, String> alarmValue2 = hbUserMapper.getAlarmValueTBS011();
        Map<String, String> alarmValue3 = hbUserMapper.getAlarmValueTBS060();
        Map<String, String> alarmValue4 = hbUserMapper.getAlarmValueTBS101();

        for (HbUser hbUser:userList) {
            //查询用户对应的园区、分区
            HbUserCompany hbUserCompany= userCompanyMapper.getHbUserCompanyInfo(hbUser.getId());
            for (Device device:deviceList) {
                if (null!=hbUserCompany) {
                    if (hbUserCompany.getParkId() == device.getParkId() || hbUserCompany.getRegionId() == device.getRegionId()) {
                        HbUserToken token = hbUserMapper.getByPhone(hbUser.getPhone());
                        if (null != token) {
                            mapList1.forEach(item -> {
                                if (2 == Integer.parseInt(alarmValue1.get("is_fault"))) {
                                    if (Double.parseDouble(item.get("v")) > Double.parseDouble(alarmValue1.get("max_value")) ||
                                            Double.parseDouble(item.get("v")) < Double.parseDouble(alarmValue1.get("min_value"))) {
                                        JsonConfig jsonConfig1 = new JsonConfig();
                                        jsonConfig1.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
                                        JSONObject json1 = new JSONObject();
                                        json1 = json1.fromObject(item, jsonConfig1);
                                        try {
                                            if (null != token) {
                                                AliPushUtil.AdvancedPush(item.get("device_name"), "设备编号:"+item.get("device_no")+"  告警值："+item.get("v")+"时间："+item.get("create_time")+"  经纬度："+item.get("lon")+","+item.get("lat")+"地址： "+item.get("address"), json1, token.getToken().substring(50, 100));
                                            }
                                        } catch (Exception e) {
                                            System.out.println("阿里推送失败");
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                                mapList2.forEach(item1 -> {
                                    if (2 == Integer.parseInt(alarmValue2.get("is_fault"))) {
                                        if (Double.parseDouble(item1.get("v")) > Double.parseDouble(alarmValue2.get("max_value"))) {
                                            JsonConfig jsonConfig1 = new JsonConfig();
                                            jsonConfig1.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
                                            JSONObject json1 = new JSONObject();
                                            json1 = json1.fromObject(item1, jsonConfig1);
                                            try {
                                                if (null != token) {
                                                    AliPushUtil.AdvancedPush(item1.get("device_name"), "设备编号:"+item1.get("device_no")+"  告警值："+item1.get("v")+"时间："+item1.get("create_time")+"  经纬度："+item1.get("lon")+","+item1.get("lat")+"地址： "+item1.get("address"), json1, token.getToken().substring(50, 100));
                                                }
                                            } catch (Exception e) {
                                                System.out.println("阿里推送失败");
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                                mapList3.forEach(item3 -> {
                                    if (2 == Integer.parseInt(alarmValue3.get("is_fault"))) {
                                        if (Double.parseDouble(item3.get("v")) > Double.parseDouble(alarmValue3.get("max_value"))) {
                                            JsonConfig jsonConfig1 = new JsonConfig();
                                            jsonConfig1.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
                                            JSONObject json1 = new JSONObject();
                                            json1 = json1.fromObject(item3, jsonConfig1);
                                            try {
                                                if (null != token) {
                                                    AliPushUtil.AdvancedPush(item3.get("device_name"), "设备编号:"+item3.get("device_no")+"  告警值："+item3.get("v")+"时间："+item3.get("create_time")+"  经纬度："+item3.get("lon")+","+item3.get("lat")+"地址： "+item3.get("address"), json1, token.getToken().substring(50, 100));
                                                }
                                            } catch (Exception e) {
                                                System.out.println("阿里推送失败");
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                                mapList4.forEach(item4 -> {
                                    if (2 == Integer.parseInt(alarmValue4.get("is_fault"))) {
                                        if (Double.parseDouble(item4.get("v")) > Double.parseDouble(alarmValue4.get("max_value"))) {
                                            JsonConfig jsonConfig1 = new JsonConfig();
                                            jsonConfig1.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());
                                            JSONObject json1 = new JSONObject();
                                            json1 = json1.fromObject(item4, jsonConfig1);
                                            try {
                                                if (null != token) {
                                                    AliPushUtil.AdvancedPush(item4.get("device_name"), "设备编号:"+item4.get("device_no")+"  告警值："+item4.get("v")+"时间："+item4.get("create_time")+"  经纬度："+item4.get("lon")+","+item4.get("lat")+"地址： "+item4.get("address"), json1, token.getToken().substring(50, 100));
                                                }
                                            } catch (Exception e) {
                                                System.out.println("阿里推送失败");
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });

                        }
                    }
                }
            }
        }
    }
}
