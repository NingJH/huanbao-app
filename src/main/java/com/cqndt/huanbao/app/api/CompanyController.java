package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.service.CompanyService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/environment/company")
@Api(value = "企业信息", description = "企业信息")
@Slf4j
public class CompanyController extends BaseController{


    @Autowired
    private CompanyService companyService;

    @GetMapping("/getCompanyLonLat")
    @ApiOperation(value = "当前登录用户关联的企业位置", notes = "当前登录用户关联的企业位置")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "int", name = "parkId", value = "", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "int", name = "regionId", value = "", defaultValue = "", required = true, paramType = "query"),
    })
    public AjaxResult getCompanyLonLat(Integer parkId, Integer regionId, int userId, String refreshJwtToken) {
        return entityResult(companyService.getCompanyLonLat(parkId,regionId,userId),refreshJwtToken,"当前登录用户关联的企业位置");
    }

    @GetMapping("/companyList")
    @ApiOperation(value = "公司列表", notes = "公司列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value="园区id", dataType = "int", paramType="query"),
            @ApiImplicitParam(name = "regionId", value="分区id", dataType = "int", paramType="query"),
            @ApiImplicitParam(dataType = "integer", name = "page", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "integer", name = "size", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "context", value = "", defaultValue = "", paramType = "query")
    })
    public AjaxResult companyList(int page,int size,Integer userId,Integer id,String refreshJwtToken,String context,Integer regionId) {
        PageHelper.startPage(page,size);
        List<Map<String,String>> list=companyService.selectCompanyList(id,context,regionId,userId);
        return entityResult(list,refreshJwtToken,"查询公司列表");
    }
}
