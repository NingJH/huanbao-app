package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.aop.serivce.OperationAspect;
import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.pojo.BuildingGeneralAccount;
import com.cqndt.huanbao.app.pojo.BuildingGeneralGo;
import com.cqndt.huanbao.app.service.IBuildingGeneralService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.List;

/**
 * @Time : 2019-09-19 19:06
 **/
@RestController
@Api(description = "一般固废暂存场")
@RequestMapping(value = "/environment/building")
public class BuildingGeneralController extends BaseController{

    @Autowired
    private IBuildingGeneralService iBuildingGeneralService;


    @ApiResponses(value = { @ApiResponse(code = 200, message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询一般固废暂存场",notes = "查询一般固废暂存场")
    @RequestMapping(value = "/selectGeneral",method = RequestMethod.GET)
    @OperationAspect(desc = "查询一般固废暂存场",module = "园区环保设施模块")
    public AjaxResult selectGeneral(Integer id, String refreshJwtToken){
        return entityResult(iBuildingGeneralService.selectGeneral(id),refreshJwtToken,"查询");
    }


    @ApiResponses(value = { @ApiResponse(code = 200, message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询一般固废暂存场去向",notes = "查询一般固废暂存场去向")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value = "一般固废暂存场id",required = true,dataType ="String",paramType = "insert"),
    })
    @RequestMapping(value = "/queryGeneralGo",method = RequestMethod.POST)
    @OperationAspect(desc = "查询一般固废暂存场去向",module = "园区环保设施模块")
    public AjaxResult queryGeneralGo(BuildingGeneralGo buildingGeneralGo, String refreshJwtToken){
        PageHelper.startPage(buildingGeneralGo.getPage(),buildingGeneralGo.getSize());
        List<BuildingGeneralGo> list=iBuildingGeneralService.queryGeneralGo(buildingGeneralGo);
        PageInfo<BuildingGeneralGo> info=new PageInfo<>(list);
        return pageResult(buildingGeneralGo.getPage(),buildingGeneralGo.getSize(),Integer.parseInt(String.valueOf(info.getTotal())),info.getList(),refreshJwtToken);
    }

    @ApiResponses(value = { @ApiResponse(code = 200, message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询一般固废暂存场",notes = "查询一般固废暂存场")
    @RequestMapping(value = "/selectGeneralMeasure",method = RequestMethod.POST)
    @OperationAspect(desc = "查询一般固废暂存场防护措施",module = "园区环保设施模块")
    public AjaxResult selectGeneralMeasure(Integer generalId, String refreshJwtToken){
        return entityResult(iBuildingGeneralService.selectGeneralMeasure(generalId),refreshJwtToken,"查询");
    }

    @ApiResponses(value = { @ApiResponse(code = 200, message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询一般固废暂存场台账",notes = "查询一般固废暂存场台账")
    @RequestMapping(value = "/selectGeneralAccount",method = RequestMethod.POST)
    @OperationAspect(desc = "查询一般固废暂存场台账",module = "园区环保设施模块")
    public AjaxResult selectGeneralAccount(BuildingGeneralAccount buildingGeneralAccount, String refreshJwtToken){
        PageHelper.startPage(buildingGeneralAccount.getPage(),buildingGeneralAccount.getSize());
        List<BuildingGeneralAccount> list=iBuildingGeneralService.selectGeneralAccount(buildingGeneralAccount);
        PageInfo<BuildingGeneralAccount> info=new PageInfo<>(list);
        return pageResult(buildingGeneralAccount.getPage(),buildingGeneralAccount.getSize(),Integer.parseInt(String.valueOf(info.getTotal())),info.getList(),refreshJwtToken);
    }
}
