package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.aop.serivce.OperationAspect;
import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.pojo.BuildingPowerCompany;
import com.cqndt.huanbao.app.service.BuildingPowerCompanyService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.List;

@RestController
@Api(description = "能量公司")
@RequestMapping(value = "/environment/building")
public class BuildingPowerCompanyController extends BaseController{


    @Autowired
    private BuildingPowerCompanyService buildingPowerCompanyService;

    @ApiResponses(value = { @ApiResponse(code = 200 ,message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询能量公司列表",notes = "查询能量公司列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name="page",value = "当前页数",dataType ="",paramType = ""),
            @ApiImplicitParam(name="size",value = "每页最大数量",dataType ="",paramType = ""),
            @ApiImplicitParam(name="companyName",value = "公司名称",dataType ="",paramType = ""),
            @ApiImplicitParam(name="type",value = "环保设施id",dataType ="",paramType = ""),
    })
    @RequestMapping(value = "/foundPowerCompany",method = RequestMethod.POST)
    @OperationAspect(desc = "查询能量公司列表",module = "园区环保设施模块")
    public AjaxResult foundPowerCompany(@RequestBody BuildingPowerCompany buildingPowerCompany, String refreshJwtToken){
        PageHelper.startPage(buildingPowerCompany.getCurrentPage(),buildingPowerCompany.getMax());
        List<BuildingPowerCompany> companyList=buildingPowerCompanyService.companyList(buildingPowerCompany);
        PageInfo<BuildingPowerCompany> commandPageInfo = new PageInfo<>(companyList);
        return pageResult(buildingPowerCompany.getCurrentPage(),buildingPowerCompany.getMax(),Integer.parseInt(String.valueOf(commandPageInfo.getTotal())),commandPageInfo.getList(),refreshJwtToken);
    }

}
