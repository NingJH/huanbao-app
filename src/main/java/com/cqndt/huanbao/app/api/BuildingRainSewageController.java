package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.aop.serivce.OperationAspect;
import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.service.IBuildingRainSewageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.List;
import java.util.Map;

@RestController
@Api(description = "雨污管网")
@RequestMapping(value = "/environment/building")
public class BuildingRainSewageController extends BaseController{

    @Autowired
    private IBuildingRainSewageService iBuildingRainSewageService;

    @ApiResponses(value = { @ApiResponse(code = 200 ,message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询雨污管网pdf",notes = "查询雨污管网")
    @RequestMapping(value = "/selectRainSewagePdf",method = RequestMethod.POST)
    @OperationAspect(desc = "查询雨污管网pdf",module = "园区环保设施模块")
    public AjaxResult selectRainSewagePdf(Integer rsId, String refreshJwtToken){
        List<Map<String,Object>> list=iBuildingRainSewageService.selectRainSewagePdf(rsId);
        return entityResult(list,refreshJwtToken,"查询");
    }
}
