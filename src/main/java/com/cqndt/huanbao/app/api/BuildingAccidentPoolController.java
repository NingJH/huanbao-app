package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.aop.serivce.OperationAspect;
import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.pojo.BuildingAccidentPoolAnnal;
import com.cqndt.huanbao.app.service.IBuildingAccidentPoolService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.List;

/**
 * @Time : 2019-09-18 17:24
 **/
@RestController
@Api(description = "应急事故池")
@RequestMapping(value = "/environment/building")
public class BuildingAccidentPoolController extends BaseController{

    @Autowired
    private IBuildingAccidentPoolService iBuildingAccidentPoolService;

    @ApiResponses(value = { @ApiResponse(code = 200, message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询应急事故池",notes = "查询应急事故池")
    @RequestMapping(value = "/selectAccidentPool",method = RequestMethod.GET)
    @OperationAspect(desc = "查询应急事故池",module = "园区环保设施模块")
    public AjaxResult selectAccidentPool(Integer id, String refreshJwtToken){
        return entityResult(iBuildingAccidentPoolService.selectAccidentPool(id),refreshJwtToken,"新增");
    }

    @ApiResponses(value = { @ApiResponse(code = 200, message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询应急事故池",notes = "查询应急事故池")
        @RequestMapping(value = "/selectAccidentPoolAnnl",method = RequestMethod.POST)
    @OperationAspect(desc = "查询应急事故池记录",module = "园区环保设施模块")
    public AjaxResult selectAccidentPoolAnnl(BuildingAccidentPoolAnnal buildingAccidentPoolAnnal, String refreshJwtToken){
        PageHelper.startPage(buildingAccidentPoolAnnal.getPage(),buildingAccidentPoolAnnal.getSize());
        List<BuildingAccidentPoolAnnal> list=iBuildingAccidentPoolService.selectAccidentPoolAnnl(buildingAccidentPoolAnnal);
        PageInfo<BuildingAccidentPoolAnnal> info=new PageInfo<>(list);
        return pageResult(buildingAccidentPoolAnnal.getPage(),buildingAccidentPoolAnnal.getSize(),Integer.parseInt(String.valueOf(info.getTotal())),info.getList(),refreshJwtToken);
    }
}
