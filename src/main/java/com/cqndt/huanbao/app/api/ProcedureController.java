package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.dao.HbProceduresEnclosureMapper;
import com.cqndt.huanbao.app.dao.HbProceduresMapper;
import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.pojo.HbProceduresEnclosure;
import com.cqndt.huanbao.app.service.ProceduresService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/environment/procedure")
@Api(value = "环保手续信息", description = "环保手续信息")
public class ProcedureController extends BaseController{

    @Autowired
    private HbProceduresMapper hbProceduresMapper;

    @Autowired
    private HbProceduresEnclosureMapper hbProceduresEnclosureMapper;

    @Autowired
    private ProceduresService proceduresService;

    @GetMapping("/listHbProducesInfo")
    @ApiOperation(value = "查看环保手续信息", notes = "查看环保手续信息")
    @ApiImplicitParam(dataType = "String", name = "parkId", value = "", defaultValue = "", required = true, paramType = "query")
    public AjaxResult listHbProducesInfo(int page, int size, String parkId, String name, String refreshJwtToken) {
        PageHelper.startPage(page,size);
        List<Map<String,Object>> hbProceduresList = hbProceduresMapper.listHbProducesInfo(parkId,name);
        PageInfo<Map<String,Object>> commandPageInfo = new PageInfo<>(hbProceduresList);
        return entityResult(commandPageInfo,refreshJwtToken,"查看环保手续信息");
    }

    @GetMapping("/listHbProducesEnclosureInfo")
    @ApiOperation(value = "查看环保手续附件信息", notes = "查看环保手续附件信息")
    @ApiImplicitParam(dataType = "Integer", name = "procedureId", value = "", defaultValue = "", required = true, paramType = "query")
    public AjaxResult listHbProducesEnclosureInfo(int page,int size,Integer procedureId,String name,String refreshJwtToken) {
        PageHelper.startPage(page,size);
        List<HbProceduresEnclosure> hbProceduresEnclosureList = hbProceduresEnclosureMapper.listHbProducesEnclosureInfo(procedureId,name);
        PageInfo<HbProceduresEnclosure> commandPageInfo = new PageInfo<>(hbProceduresEnclosureList);
        return entityResult(commandPageInfo,refreshJwtToken,"查看环保手续附件信息");
    }


    @GetMapping("/measureReport")
    @ApiOperation(value = "查询监测报告", notes = "查询监测报告")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parkId", value="园区id", dataType = "Integer", paramType="query"),
            @ApiImplicitParam(name = "companyId", value="公司id", dataType = "Integer",paramType="query"),
            @ApiImplicitParam(name = "type", value="类型(1监测报告、2批复、3环评、4验收、5应急预案、6排污许可)", dataType = "Integer",paramType="query"),
            @ApiImplicitParam(name = "regionId", value="园区id", dataType = "Integer",paramType="query"),
            @ApiImplicitParam(dataType = "integer", name = "page", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "integer", name = "size", value = "", defaultValue = "", paramType = "query")

    })
    public AjaxResult selectMeasureReport(int page,int size,Integer parkId,Integer companyId,Integer type,Integer regionId,String refreshJwtToken) {
        PageHelper.startPage(page,size);
        Map<String,Object> map=proceduresService.selectMeasureReport(parkId,companyId,type,regionId);
        return  entityResult(map,refreshJwtToken,"查询监测报告");
    }

}
