package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.service.StatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-30 15:25
 **/
@RequestMapping(value = "/environment/statistics")
@Api(description = "环保统计相关接口文档")
@RestController
public class StatisticsController extends BaseController{

    @Autowired
    private StatisticsService statisticsService;

    /**
     * 园区环保统计
     *
     * @param parkId 园区id
     * @return
     */
    @ApiOperation(value = "园区环保统计接口", notes = "环保统计接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "园区id", required = false, dataType = "Integer", paramType = "query")
    })
    @GetMapping(value = "/statisticsRank")
    public AjaxResult statisticsRank(Integer parkId, Integer regionId,Integer type , int userId, String refreshJwtToken) {
        return entityResult(statisticsService.statisticsRank(parkId, regionId,type,userId),refreshJwtToken,"园区环保统计");
    }

    /**
     * 园区环保统计
     *
     * @param parkId 园区id
     * @return
     */
    @ApiOperation(value = "园区环保统计曲线接口", notes = "园区环保统计曲线接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parkId", value = "园区id", required = false, dataType = "Integer", paramType = "query")
    })
    @GetMapping(value = "/statisticsRank1")
    public AjaxResult statisticsRank1(Integer parkId, Integer regionId,Integer type , int userId, String refreshJwtToken) {
        return entityResult(statisticsService.statisticsRank1(parkId, regionId,type,userId),refreshJwtToken,"园区环保统计");
    }

    /**
     * 企业环保统计
     *
     * @param companyId 企业id
     * @return
     */
    @ApiOperation(value = "企业环保统计接口", notes = "企业环保统计接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "companyId", value = "企业id", required = false, dataType = "Integer", paramType = "query")
    })
    @RequestMapping(value = "/companyStatisticsRank", method = RequestMethod.GET)
    public AjaxResult companyStatisticsRank(Integer type,Integer companyId,int userId,String refreshJwtToken) {
        return entityResult(statisticsService.companyStatisticsRank(type,companyId,userId),refreshJwtToken,"企业环保统计");
    }

}
