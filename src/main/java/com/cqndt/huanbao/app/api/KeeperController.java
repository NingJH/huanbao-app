package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.aop.serivce.OperationAspect;
import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.pojo.HbKeeper;
import com.cqndt.huanbao.app.pojo.OtherAlarmRecords;
import com.cqndt.huanbao.app.service.AlarmService;
import com.cqndt.huanbao.app.service.KeeperLonlatService;
import com.cqndt.huanbao.app.service.KeeperService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/environment/keeper")
@Api(value = "环保管家",description = "环保管家")
public class KeeperController extends BaseController{

    @Autowired
    KeeperService keeperService;

    @Autowired
    KeeperLonlatService keeperLonlatService;
    @Autowired
    AlarmService alarmService;

    @GetMapping("/listKeeper")
    @ApiOperation(value = "环保管家列表", notes = "环保管家列表")
    public AjaxResult listKeeper(int page, int size, String name, String refreshJwtToken) {
        PageHelper.startPage(page,size);
        List<HbKeeper> keeperList = keeperService.listKeeper(name);
        PageInfo<HbKeeper> commandPageInfo = new PageInfo<>(keeperList);
        return entityResult(commandPageInfo,refreshJwtToken,"环保管家列表");
    }

    @GetMapping("/listKeeperLonlat")
    @ApiOperation(value = "环保管家列表带经纬度", notes = "环保管家列表带经纬度")
    public AjaxResult listKeeperLonlat(String parkId,String regionId,String refreshJwtToken) {
        return entityResult(keeperLonlatService.listKeeperLonlat(parkId,regionId),refreshJwtToken,"环保管家列表带经纬度");
    }




    @GetMapping("/recordList")
    @ApiOperation(value = "环保管家——巡查/排查/督查及投诉情况列表", notes = "环保管家——巡查/排查/督查及投诉情况列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "companyId", value="公司id", dataType = "Integer", paramType="query"),
            @ApiImplicitParam(name = "userId", value="用户id", dataType = "Integer",required = true,paramType="query"),
            @ApiImplicitParam(name = "type", value="类型：0巡查 1排查 2投诉及建议", dataType = "Integer",required = true,paramType="query"),
            @ApiImplicitParam(dataType = "integer", name = "page", value = "", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "integer", name = "size", value = "", defaultValue = "", required = true, paramType = "query"),
    })
    public AjaxResult recordList(int page,int size,Integer companyId, Integer userId, Integer type,String refreshJwtToken) {
        if(userId==null){
            return failureResult("用户不存在");
        }
        PageHelper.startPage(page,size);
        List<Map<String, Object>> list=keeperService.selectRecordList(companyId,userId,type);
        return entityResult(list,refreshJwtToken,"查询列表");
    }

    @GetMapping("/measureDetail")
    @ApiOperation(value = "监测点详情", notes = "监测点详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parkId", value="园区id", dataType = "Integer", paramType="query"),
            @ApiImplicitParam(name = "regionId", value="分区id", dataType = "Integer", paramType="query"),
            @ApiImplicitParam(name = "measureId", value="监测点id", dataType = "Integer",paramType="query"),
            @ApiImplicitParam(name = "type", value="类型（1二氧化硫、2二氧化氮、3可吸入颗粒物、4一氧化碳、5臭氧、6细颗粒物 7其它）", dataType = "Integer",paramType="query")
    })
    public AjaxResult measureDetail(Integer parkId, Integer measureId, Integer type,String refreshJwtToken,Integer regionId) {
        return entityResult(keeperService.selectMeasureDetail(parkId,measureId,type,regionId),refreshJwtToken,"监测点详情");
    }

    @GetMapping("/measureList")
    @ApiOperation(value = "监测点列表", notes = "监测点列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parkId", value="园区id", dataType = "Integer", paramType="query"),
            @ApiImplicitParam(name = "regionId", value="分区id", dataType = "Integer", paramType="query"),
            @ApiImplicitParam(name = "type", value="测点类型 1: 大气环境监测点,2: 地表水环境监测点,3: 声环境监测点,4: 地下水环境监测点,5: 土壤环境监测点",dataType = "Integer",paramType="query"),
            @ApiImplicitParam(name = "context", value="搜索", dataType = "String", paramType="query"),
            @ApiImplicitParam(dataType = "integer", name = "page", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "integer", name = "size", value = "", defaultValue = "", paramType = "query"),
    })
    public AjaxResult measureList(int page,int size,Integer parkId,String refreshJwtToken,String context,Integer regionId,Integer type) {
        PageHelper.startPage(page,size);
        List<Map<String, Object>> list=keeperService.selectMeasureList(parkId,context,regionId,type);
        return entityResult(list,refreshJwtToken,"监测点列表");
    }

    @GetMapping("/warnAlarmList")
    @ApiOperation(value = "预警告警列表", notes = "预警告警列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parkId", value="园区id", dataType = "Integer", paramType="query"),
            @ApiImplicitParam(dataType = "integer", name = "page", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "integer", name = "size", value = "", defaultValue = "", paramType = "query")
    })
    public AjaxResult warnAlarmList(int page,int size,Integer parkId,String context,Integer userId,String refreshJwtToken) {
        PageHelper.startPage(page,size);
        List<Map<String,Object>> list=keeperService.selectWarnAlarmList(parkId,context,userId);
        return entityResult(list,refreshJwtToken,"预警告警列表");
    }

    @GetMapping("/warnAlarmDetail")
    @ApiOperation(value = "预警告警详情", notes = "预警告警详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value="环保手续附件id", dataType = "Integer", paramType="query")
    })
    public AjaxResult warnAlarmDetail(Integer id,String refreshJwtToken) {
        return entityResult(keeperService.selectWarnAlarmDetail(id),refreshJwtToken,"预警告警详情");
    }

    @GetMapping("/keeperLonlat")
    @ApiOperation(value = "环保管家经纬度", notes = "环保管家经纬度")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "lon", value="经度", dataType = "String", paramType="query"),
            @ApiImplicitParam(name = "lat", value="纬度", dataType = "String", paramType="query"),
            @ApiImplicitParam(name = "userId", value="用户id", dataType = "Integer", paramType="query")
    })
    public AjaxResult keeperLonlat(String lon, String lat,Integer userId,String refreshJwtToken){
        int result=keeperService.addKeeperLonlat(lon,lat,userId);
        if(result==1){
            return commonResult(1,refreshJwtToken,"环保管家经纬度添加");
        }
        return commonResult(-1,refreshJwtToken,"环保管家经纬度添加");
    }

    @GetMapping("/selectsgWarm")
    @ApiOperation(value = "事故告警", notes = "一键告警")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "integer", name = "page", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "integer", name = "size", value = "", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "content", value = "过滤查询", defaultValue = "", paramType = "query")
    })
    public AjaxResult selectsgWarmList(int page,int size,String content,Integer userId,String refreshJwtToken) {
        PageHelper.startPage(page,size);
        System.out.println(page);
        System.out.println(size);
        System.out.println(content);
        System.out.println(userId);
        List<Map<String, Object>> list=keeperService.getsgAlarmList(content,userId);
        return entityResult(list,refreshJwtToken,"事故告警列表");
    }
}
