package com.cqndt.huanbao.app.api;


import com.cqndt.huanbao.app.dao.HbUserMapper;
import com.cqndt.huanbao.app.exception.ErrorInfo;
import com.cqndt.huanbao.app.exception.ErrorType;
import com.cqndt.huanbao.app.exception.ServiceException;
import com.cqndt.huanbao.app.pojo.AjaxResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * user: zhaojianji
 * date: 2017/05/25
 * desc:  公共controller类，所有的controller类都应该继承此类
 */
@Slf4j
@Component
public class BaseController {



    @Resource
    private HbUserMapper userMapper;
    @Resource
    private UserController userController;

    /**
     * 重定向到指定url
     *
     * @param url 指定的url
     * @return spring 默认返回对应视图
     */
    protected String redirect(String url) {
        return "redirect:" + url;
    }

    /**
     * 跳转到指定url
     *
     * @param url 指定的url
     * @return 返回视图
     */
    protected String forward(String url) {
        return "forward:" + url;
    }

//    /**
//     * 从上下文中获取登录用户的信息
//     *
//     * @return 返回用户信息
//     */
//    protected UserDetail getUserDetail() {
//        SecurityContextHolder.getContext().getAuthentication();
//        SecurityContext context = SecurityContextHolder.getContext();
//        Authentication authentication = context.getAuthentication();
//        Object o = authentication.getPrincipal();
//        log.info("主角登场 -> " + o);
//        if (o instanceof UserModel) {
//            UserModel userDetails = (UserModel) o;
//            log.info("default-userDetails->" + userDetails);
//            return userDetails.getUserDetail();
//        }
//        return null;
//    }

    /**
     * 判断请求方是手机还是pc
     *
     * @param request 请求
     * @return true 是手机访问 false 非手机访问
     */

    protected boolean isMobile(HttpServletRequest request) {
        String userAgent = request.getHeader("User-Agent");
        String[] agent = {"Android", "iPhone", "Windows Phone", "MQQBrowser"};
        if (!userAgent.contains("Windows NT") || (userAgent.contains("Windows NT") && userAgent.contains("compatible; MSIE 9.0;"))) {
            // 排除 苹果桌面系统
            if (!userAgent.contains("Windows NT") && !userAgent.contains("Macintosh")) {
                for (String s : agent) {
                    if (userAgent.contains(s)) {
                        return true;
                    }
                }

            }
        }
        return false;
    }

    protected AjaxResult exceptionReslut(ServiceException e, String refreshJwtToken) {

        return new AjaxResult(false, e.getErrCode(), e.getErrMsg(),refreshJwtToken);
    }

    protected AjaxResult commonResult(int result,String refreshJwtToken, String actionName) {
        AjaxResult ajaxResult = new AjaxResult();
        String message = actionName;
        Map<String,Object> map = new HashMap<>();
        if (result > 0) {
            ajaxResult.setOk(true);
            ajaxResult.setCode(200);
            ajaxResult.setRefreshJwtToken(refreshJwtToken);
            message += "成功";
        } else {
            ajaxResult.setOk(false);
            ajaxResult.setCode(500);
            ajaxResult.setRefreshJwtToken(refreshJwtToken);
//            message += "失败";
            map.put("messageName",message);
            ajaxResult.setResponse(map);
        }
        ajaxResult.setMessage(message);

        return ajaxResult;
    }


    protected AjaxResult entityResult(Object data, String refreshJwtToken, String message) {

        AjaxResult ajaxResult = new AjaxResult();
        if (data != null) {
            ajaxResult.setOk(true);
            ajaxResult.setCode(200);
            ajaxResult.setData(data);
            ajaxResult.setRefreshJwtToken(refreshJwtToken);
            message += "成功";
        } else {
            ajaxResult.setOk(false);
            ajaxResult.setCode(500);
            ajaxResult.setData(data);
            ajaxResult.setRefreshJwtToken(refreshJwtToken);
            message += "失败";
        }
        ajaxResult.setMessage(message);
        return ajaxResult;
    }

    protected AjaxResult entityResults(Object data,String refreshJwtToken, String message) {

        AjaxResult ajaxResult = new AjaxResult();
        if (data != (Object) 0) {
            ajaxResult.setOk(true);
            ajaxResult.setCode(200);
            ajaxResult.setData(data);
            ajaxResult.setRefreshJwtToken(refreshJwtToken);
            message += "成功";
        } else if (data == (Object) 0) {
            ajaxResult.setOk(false);
            ajaxResult.setCode(401);
            ajaxResult.setData(data);
            ajaxResult.setRefreshJwtToken(refreshJwtToken);
            message += "失败";
        } else {
            ajaxResult.setOk(false);
            ajaxResult.setCode(500);
            ajaxResult.setData(data);
            ajaxResult.setRefreshJwtToken(refreshJwtToken);
            message += "失败";
        }
        ajaxResult.setMessage(message);
        return ajaxResult;
    }


    protected AjaxResult listResult(Collection collection, String refreshJwtToken){
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setOk(true);
        ajaxResult.setRefreshJwtToken(refreshJwtToken);
        ajaxResult.setCode(200);
        ajaxResult.setMessage("获取数据成功,数据条数:" + collection.size());
        ajaxResult.setCurRecord(collection.size());
        ajaxResult.setPage(false);
        ajaxResult.setData(collection);
        return ajaxResult;
    }

    protected AjaxResult listResul1(Collection collection, String refreshJwtToken){
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setOk(true);
        ajaxResult.setRefreshJwtToken(refreshJwtToken);
        ajaxResult.setCode(200);
        ajaxResult.setData(collection);
        return ajaxResult;
    }

    protected AjaxResult pageResult(int curPage, int prePageSize, int totalRecord, Collection collection,String refreshJwtToken) {
        int totalPage;
        if (totalRecord % prePageSize == 0) {
            totalPage = totalRecord / prePageSize;
        } else {
            totalPage = totalRecord / prePageSize + 1;
        }
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setOk(true);
        ajaxResult.setRefreshJwtToken(refreshJwtToken);
        ajaxResult.setCode(200);
        ajaxResult.setMessage("获取数据成功,数据条数:" + collection.size());
        ajaxResult.setCurPage(curPage);
        ajaxResult.setCurRecord(collection.size());
        ajaxResult.setCount(totalRecord);
        ajaxResult.setTotalPage(totalPage);
        ajaxResult.setPage(true);
        ajaxResult.setData(collection);
        ajaxResult.setPrePageSize(prePageSize);
        return ajaxResult;

    }


    protected AjaxResult bindErrorResult(BindingResult result) {
        List<ErrorInfo> errorInfos = new ArrayList<>();
        result.getAllErrors().forEach(error ->
            errorInfos.add(new ErrorInfo(((DefaultMessageSourceResolvable) error.getArguments()[0]).getCodes()[1], error.getDefaultMessage())));
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setOk(false);
        ajaxResult.setCode(ErrorType.DATA_BIND_ERROR.getCode());
        ajaxResult.setMessage(ErrorType.DATA_BIND_ERROR.getErrorMsg());
        ajaxResult.setErrors(errorInfos);
        log.error("返回错误结果:" + ajaxResult);
        return ajaxResult;
    }

    protected AjaxResult failureResult(String message) {
        List<ErrorInfo> errorInfos = new ArrayList<>();
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setOk(false);
        ajaxResult.setCode(ErrorType.DATA_BIND_ERROR.getCode());
        ajaxResult.setMessage(message);
        ajaxResult.setErrors(errorInfos);
        log.error("返回错误结果:" + ajaxResult);
        return ajaxResult;
    }
}
