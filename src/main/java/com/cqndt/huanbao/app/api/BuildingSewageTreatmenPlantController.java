package com.cqndt.huanbao.app.api;

import com.cqndt.huanbao.app.aop.serivce.OperationAspect;
import com.cqndt.huanbao.app.pojo.AjaxResult;
import com.cqndt.huanbao.app.service.BuildingSewageTreatmenPlantService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;

@RestController
@Api(description = "污水处理厂")
@RequestMapping(value = "/environment/building")
public class BuildingSewageTreatmenPlantController extends BaseController{

    @Autowired
    private BuildingSewageTreatmenPlantService buildingSewageTreatmenPlantService;

    @ApiResponses(value = { @ApiResponse(code = 200 ,message = "Invalid input" ,response = Component.class)})
    @ApiOperation(value = "查询污水处理厂",notes = "查询污水处理厂")
    @ApiImplicitParams({
            @ApiImplicitParam(name="buildId",value = "设备id",dataType ="integer",paramType = ""),

    })
    @RequestMapping(value = "/querySewagePlant",method = RequestMethod.POST)
    @OperationAspect(desc = "查询污水处理厂",module = "园区环保设施模块")
    public AjaxResult querySewagePlant(Integer buildId, String refreshJwtToken){
        return entityResult(buildingSewageTreatmenPlantService.queryPlants(buildId),refreshJwtToken,"查询");
    }


    @ApiResponses(value = { @ApiResponse(code = 200, message = "Invalid input")})
    @ApiOperation(value = "数据曲线",notes = "数据曲线")
    @RequestMapping(value = "/queryAlarmLevel",method = RequestMethod.POST)
    @OperationAspect(desc = "数据曲线",module = "数据曲线")
    public AjaxResult echar(Integer parkId,String deviceNo,String refreshJwtToken){

//        AlarmLevelCondition alarmLevelCondition = new AlarmLevelCondition();
//        alarmLevelCondition.setDel(0);
//        AlarmLevelCollection alarmLevelCollection= alarmRoleService.listAlarmLevelByCondition(alarmLevelCondition);
//        return listResult(alarmLevelCollection.getEntityList(), refreshJwtToken);
        return entityResult(buildingSewageTreatmenPlantService.echar(parkId, deviceNo),refreshJwtToken,"数据曲线查询");
    }

}
