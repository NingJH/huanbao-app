package com.cqndt.huanbao.app.api;
import com.cqndt.huanbao.app.aop.serivce.OperationAspect;
import com.cqndt.huanbao.app.pojo.*;
import com.cqndt.huanbao.app.service.AlarmService;
import com.cqndt.huanbao.app.service.IAlarmRoleService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-09-06 09:46
 **/
@RestController
@RequestMapping(value = "/environment/alarm")
@Api(description = "一键告警")
@Slf4j
public class AlarmController extends BaseController{

    @Autowired
    AlarmService alarmService;
    @Autowired
    private IAlarmRoleService alarmRoleService;


    @ApiResponses(value = { @ApiResponse(code = 200, message = "Invalid input")})
    @ApiOperation(value = "查询所有报警等级接口",notes = "查询所有报警等级接口")
    @RequestMapping(value = "/queryAlarmLevel",method = RequestMethod.POST)
    @OperationAspect(desc = "查询所有报警等级接口",module = "一键报警对象模块")
    public AjaxResult alarmLevelList(String refreshJwtToken){
        AlarmLevelCondition alarmLevelCondition = new AlarmLevelCondition();
        alarmLevelCondition.setDel(0);
        AlarmLevelCollection alarmLevelCollection= alarmRoleService.listAlarmLevelByCondition(alarmLevelCondition);
        return listResult(alarmLevelCollection.getEntityList(), refreshJwtToken);
    }
    @ApiOperation(value = "一键告警",notes = "一键告警")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "lon",value = "经度",required = true,dataType ="String",paramType = "query"),
            @ApiImplicitParam(name = "lat",value = "纬度",required = true,dataType ="String",paramType = "query"),
            @ApiImplicitParam(name = "name",value = "名称",required = true,dataType ="String",paramType = "query"),
            @ApiImplicitParam(name = "content",value = "内容",required = true,dataType ="String",paramType = "query"),
            @ApiImplicitParam(name = "level",value = "告警等级",required = true,dataType ="int",paramType = "query"),
            @ApiImplicitParam(name = "remakes",value = "备注",required = false,dataType ="String",paramType = "query"),
            @ApiImplicitParam(name = "ids",value = "所选告警对象",required = false,dataType ="String",paramType = "query")
    })
    @RequestMapping(value = "/alarmPush",method = RequestMethod.POST)
    public AjaxResult alarmPush(String ids,String lon, String lat, String alarmName, String content,Integer level, String remakes,Integer parkId, Integer regionId,Integer userId,String refreshJwtToken){
        try{
            return commonResult(alarmService.alarmPush(ids,lon, lat, alarmName, content, level, remakes, parkId, regionId,userId),refreshJwtToken,"推送");
        }catch (Exception e){
            return commonResult(0,refreshJwtToken,"推送");
        }
    }


    @RequestMapping(value = "/selectWarmObject",method = RequestMethod.POST)
    @OperationAspect(desc = "查询园区级告警下属分区",module = "告警模块")
    public AjaxResult selectWarmObject(Integer parkId,Integer userId ,String refreshJwtToken){
        return  entityResult(alarmService.selectWarmRegion(parkId,userId),refreshJwtToken,"查询园区级告警对象");
    }

    @RequestMapping(value = "/selectWarmCompany",method = RequestMethod.POST)
    @OperationAspect(desc = "查询企业级告警推送对象",module = "告警模块")
    public AjaxResult selectWarmCompany(Integer parkId,Integer regionId,Integer userId,String refreshJwtToken) {
           List<HbCompany>  hbCompanyList=alarmService.selectWarmCompany(parkId,regionId,userId);
        log.info("企业告警对对象>>>>>>>>>>>>>"+hbCompanyList.size()+"+>>>>>>>>>"+hbCompanyList.get(0).getName()+">>>>>>>>>>>");

        return entityResult(alarmService.selectWarmCompany(parkId,regionId,userId),refreshJwtToken,"查询园区级告警对象");
    }


//    @RequestMapping(value = "/selectsgWarmInfo",method = RequestMethod.POST)
//    @OperationAspect(desc = "事故告警详情",module = "一键告警模块")
//    public AjaxResult selectsgWarmInfo( Integer id, String refreshJwtToken) {
//        List<Map<String, Object>> otherAlarmInfo = hbIOtherAlarmRecordsService.selectWarnAlarmDetail(id);
//        return entityResult(otherAlarmInfo,refreshJwtToken,"查询事故告警详情");
//    }

}
