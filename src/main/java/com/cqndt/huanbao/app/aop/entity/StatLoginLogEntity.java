package com.cqndt.huanbao.app.aop.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


/**
 * @Author mengjiajie
 * @Date 2019-06-24
 * @Email yin_qingqin@163.com
 **/
@Data
@ApiModel(value="StatLoginLogEntity", description="登录日志")
public class StatLoginLogEntity{

    @ApiModelProperty(value = "编号", required = false, example = "")
    private String logCode;

    @ApiModelProperty(value = "新增时间", required = false, example = "")
    private Date createTime;

    @ApiModelProperty(value = "修改时间", required = false, example = "")
    private Date modifierTime;

    @ApiModelProperty(value = "创建者", required = false, example = "")
    private String creater;

    @ApiModelProperty(value = "修改人", required = false, example = "")
    private String modifier;

    @ApiModelProperty(value = "所属机构", required = false, example = "")
    private String possessor;

    @ApiModelProperty(value = "备注", required = false, example = "")
    private String remark;

    @ApiModelProperty(value = "用户标识", required = false, example = "")
    private String accountCode;

    @ApiModelProperty(value = "用户名称", required = false, example = "")
    private String accountName;

    @ApiModelProperty(value = "会话ID", required = false, example = "")
    private String sessionId;

    @ApiModelProperty(value = "系统标识", required = false, example = "")
    private String systemCode;

    @ApiModelProperty(value = "登录IP", required = false, example = "")
    private String ip;

    @ApiModelProperty(value = "登录设备  0.PC  1.手机  2.平板", required = false, example = "")
    private Integer loginDevice;

    @ApiModelProperty(value = "登录时间", required = false, example = "")
    private Date loginTime;

    @ApiModelProperty(value = "登录设备信息", required = false, example = "")
    private String loginDeviceInfo;

    @ApiModelProperty(value = "退出流程 0.未返回退出信息 1.正常退出 2.过期退出  3.登出退出", required = false, example = "")
    private String exitProcess;

    @ApiModelProperty(value = "退出时间", required = false, example = "")
    private Date exitTime;


}