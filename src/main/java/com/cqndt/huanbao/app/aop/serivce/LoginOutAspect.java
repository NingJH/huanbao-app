package com.cqndt.huanbao.app.aop.serivce;

import java.lang.annotation.*;

/**
 * Create by Intellij IDEA
 * User : mengjiajie
 * Mail : 15826014394@163.com
 * Time : 2019/6/28 14:54
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface LoginOutAspect {
    String desc() default "";
    String module() default "";
}
