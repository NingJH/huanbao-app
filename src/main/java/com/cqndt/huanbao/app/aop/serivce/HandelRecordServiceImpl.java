package com.cqndt.huanbao.app.aop.serivce;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Create by Intellij IDEA
 * User : mengjiajie
 * Mail : 15826014394@163.com
 * Time : 2019/7/9 19:35
 **/
@Service
public class HandelRecordServiceImpl implements  HandelRecordService{
    @Value("${local.login.url}")
    private String loacalLoginUrl;
    @Value("${aop.handel.add.url}")
    private String handelAddUrl;
    private static HttpClientUtils httpClientUtils = new HttpClientUtils();

    @Override
    @Async
    public void dealHandel(Map<String,String> handelMap) {
        httpClientUtils.httpPostForm(handelAddUrl, handelMap);
    }
}
