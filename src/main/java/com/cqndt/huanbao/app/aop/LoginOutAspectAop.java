package com.cqndt.huanbao.app.aop;


import com.cqndt.huanbao.app.aop.serivce.HttpClientUtils;
import com.cqndt.huanbao.app.exception.ErrorType;
import com.cqndt.huanbao.app.exception.ServiceException;
import com.cqndt.huanbao.app.pojo.JwtHelper;
import com.cqndt.huanbao.app.util.DateUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 操作日志AOP
 *
 * @author yin_q
 */
@Aspect
@Component
@Slf4j
public class LoginOutAspectAop {

    private static HttpClientUtils httpClientUtils = new HttpClientUtils();

    @Value("${aop.login.out.url}")
    private String loginOutUrl;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    // 操作对象
//    private ThreadLocal<Map<String,String>> loginMap = new InheritableThreadLocal<>();
//    private ThreadLocal<Map<String,String>> handelMap = new InheritableThreadLocal<>();

    private Map<String, String> loginOutMap = new HashMap<>();


    @Pointcut(value = "@annotation(com.cqndt.huanbao.app.aop.serivce.LoginOutAspect)")
    public void LoginOutAspect() {
    }


    private static HttpServletRequest request;
    private static String sessionId;
    /**
     * 前置通知
     */
    @Before("LoginOutAspect()")
    public void doBefore() {
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        assert sra != null;
        request = sra.getRequest();
        Map<String, String> stringMap = getRequestSessionId(request);
        sessionId = stringMap.get("sessionId");

        loginOutMap.put("sessionId", sessionId);
        //退出登录记录
        loginOutRecord();

    }


    /**
     * 接口后置通知
     */
    @AfterReturning(pointcut = "LoginOutAspect()")
    public void doAfter() {
        dealOutLogin();
    }

    /**
     * 异常通知
     */
    @AfterThrowing(pointcut = "LoginOutAspect()")
    public void doAfterThrowing() {
    }

    private void loginOutRecord() {
        loginOutMap.put("exitTime", DateUtil.formatDate(new Date(), "yyyy-MM-dd hh:mm:ss"));
        loginOutMap.put("exitProcess", "1");
    }

    private void dealOutLogin() {
        try {
            httpClientUtils.httpPostForm(loginOutUrl, loginOutMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Map<String,String> getRequestSessionId(HttpServletRequest request) {
        Map<String,String> map = new HashMap<>();
        final String authHeader = request.getHeader("authorization");
        if (authHeader == null || !authHeader.startsWith("bearer;")) {
            throw new ServiceException(ErrorType.FORBIDDEN);
        }
        final String token = authHeader.substring(7);

        String base64Secret = stringRedisTemplate.opsForValue().get("jwt_token:" + authHeader + ":secret");
        final Claims claims = JwtHelper.parseJWT(token, base64Secret);
        map.put("sessionId",claims.get("sessionId").toString());
        return map;
    }


}
