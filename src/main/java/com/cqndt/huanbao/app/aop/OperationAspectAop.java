package com.cqndt.huanbao.app.aop;

import com.alibaba.fastjson.JSON;
import com.cqndt.huanbao.app.aop.serivce.HandelRecordService;
import com.cqndt.huanbao.app.aop.serivce.HttpClientUtils;
import com.cqndt.huanbao.app.aop.serivce.OperationAspect;
import com.cqndt.huanbao.app.exception.ErrorType;
import com.cqndt.huanbao.app.exception.ServiceException;
import com.cqndt.huanbao.app.pojo.JwtHelper;
import com.cqndt.huanbao.app.util.DateUtil;
import com.cqndt.huanbao.app.util.MapUtil;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 操作日志AOP
 *
 * @author yin_q
 */
@Aspect
@Component
@Slf4j
public class OperationAspectAop {

    private static HttpClientUtils httpClientUtils = new HttpClientUtils();

    @Value("${aop.handel.add.url}")
    private String handelAddUrl;
    @Value("${aop.handel.update.url}")
    private String handelUpdateUrl;
    @Value(("${system.code}"))
    private String systemCode;
    @Value("${local.login.url}")
    private String loacalLoginUrl;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private HandelRecordService handelRecordService;


    // 操作对象
//    private ThreadLocal<Map<String,String>> loginMap = new InheritableThreadLocal<>();
//    private ThreadLocal<Map<String,String>> handelMap = new InheritableThreadLocal<>();

    private Map<String, String> handelMap = new HashMap<>();


    @Pointcut(value = "@annotation(com.cqndt.huanbao.app.aop.serivce.OperationAspect)")
    public void OperationAspect() {
    }

    private static HttpServletRequest request;
    private static HttpSession httpSession;
    private static String sessionId;
    private static String userId;
    private static String name;

    /**
     * 前置通知
     *
     * @param joinPoint 参数对象
     */
    @Before("OperationAspect()&&@annotation(aspect)")
    public void doBefore(JoinPoint joinPoint, OperationAspect aspect) {
        try {
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            ServletRequestAttributes sra = (ServletRequestAttributes) ra;
            assert sra != null;
            request = sra.getRequest();
            httpSession = request.getSession();
            if (isLoginRequest(request)) {
                sessionId = httpSession.getId();
            } else if (String.valueOf(request.getRequestURL()).contains("dictionary/queryDictionaryTypeList")) {
                return;
            } else {
                Map<String, String> stringMap = getRequestSessionId(request);
                sessionId = stringMap.get("sessionId");
                userId = stringMap.get("userId");
                name = stringMap.get("name");
            }

            //获取浏览器 设备 版本等信息
            UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
            OperatingSystem operatingSystem = userAgent.getOperatingSystem();
            Browser browser = userAgent.getBrowser();
            Version browserVersion = userAgent.getBrowserVersion();
            //操作记录
            handelRecord(aspect, request, joinPoint, operatingSystem, browser, browserVersion);
        } catch (Exception e) {
            log.info("操作前抛出异常" + e.getMessage());
        }

    }


    /**
     * 接口后置通知
     *
     * @param object 对象
     */
    @AfterReturning(returning = "object", pointcut = "OperationAspect()")
    public void doAfter(Object object) {
        try {
            Map<String, Object> map = MapUtil.Obj2Map(object);
            dealHandel(map);
        } catch (Exception e) {
            log.info("操作后抛出异常" + e.getMessage());
        }
    }

    /**
     * 异常通知
     *
     * @param e 异常
     */
    @AfterThrowing(throwing = "e", pointcut = "OperationAspect()")
    public void doAfterThrowing(Throwable e) {
        handelMap.put("hasException", "1");
        handelMap.put("exceptionData", String.valueOf(e.getMessage()));

        dealHandel(handelMap);
    }


    private void handelRecord(OperationAspect aspect, HttpServletRequest request, JoinPoint joinPoint, OperatingSystem operatingSystem, Browser browser, Version browserVersion) {
        handelMap.put("classPath", String.valueOf(joinPoint.getSignature().getDeclaringType()));
        handelMap.put("deviceInfo", operatingSystem + " " + browser + " " + browserVersion);
        handelMap.put("ip", request.getRemoteAddr());
        handelMap.put("methodName", joinPoint.getSignature().getName());
        handelMap.put("operation", aspect.desc());
        handelMap.put("module", aspect.module());
        handelMap.put("param", Arrays.toString(joinPoint.getArgs()));
        handelMap.put("startDate", DateUtil.formatDate(new Date(), "yyyy-MM-dd hh:mm:ss"));
        handelMap.put("type", String.valueOf(isAjax(request)));
        handelMap.put("url", String.valueOf(request.getRequestURL()));
        handelMap.put("way", request.getMethod());
    }

    private int isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With")) ? 0 : 1;
    }



    private void dealHandel(Map map) {
        handelMap.put("sessionId", sessionId);
        handelMap.put("finishTime", DateUtil.formatDate(new Date(), "yyyy-MM-dd hh:mm:ss"));
        handelMap.put("returnData", JSON.toJSONString(map));
        handelMap.put("returnTime", DateUtil.formatDate(new Date(), "yyyy-MM-dd hh:mm:ss"));
        handelMap.put("accountCode", userId);
        handelMap.put("accountName", name);
        handelRecordService.dealHandel(handelMap);

    }



    private Map<String, String> getRequestSessionId(HttpServletRequest request) {

        Map<String, String> map = new HashMap<>();
        final String authHeader = request.getHeader("authorization");
        if (authHeader == null || !authHeader.startsWith("bearer;")) {
            throw new ServiceException(ErrorType.FORBIDDEN);
        }
        final String token = authHeader.substring(7);

        String base64Secret = stringRedisTemplate.opsForValue().get("jwt_token:" + authHeader + ":secret");
        final Claims claims = JwtHelper.parseJWT(token, base64Secret);
        map.put("sessionId", claims.get("sessionId").toString());
        map.put("userId", claims.get("userId").toString());
        map.put("name", claims.get("name").toString());
        return map;
    }

    private boolean isLoginRequest(HttpServletRequest request) {
        String s = String.valueOf(request.getRequestURL());
        return s.contains(loacalLoginUrl);
    }
}
