package com.cqndt.huanbao.app.aop.serivce;

import java.lang.annotation.*;

/**
 * Create by Intellij IDEA
 * User : mengjiajie
 * Mail : 15826014394@163.com
// * Time : 2019/6/24 17:59
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface OperationAspect {
    String desc() default "";
    String module() default "";
}
