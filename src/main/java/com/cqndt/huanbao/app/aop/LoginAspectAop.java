package com.cqndt.huanbao.app.aop;


import com.cqndt.huanbao.app.aop.serivce.HttpClientUtils;
import com.cqndt.huanbao.app.aop.serivce.OperationAspect;
import com.cqndt.huanbao.app.util.DateUtil;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 操作日志AOP
 *
 * @author yin_q
 */
@Aspect
@Component
@Slf4j
public class LoginAspectAop {

    private static HttpClientUtils httpClientUtils = new HttpClientUtils();

    @Value("${aop.login.url}")
    private String loginUrl;
    @Value(("${system.code}"))
    private String systemCode;

    // 操作对象
//    private ThreadLocal<Map<String,String>> loginMap = new InheritableThreadLocal<>();
//    private ThreadLocal<Map<String,String>> handelMap = new InheritableThreadLocal<>();

    private Map<String, String> loginMap = new HashMap<>();


    @Pointcut(value = "@annotation(com.cqndt.huanbao.app.aop.serivce.LoginAspect)")
    public void LoginAspect() {
    }
    private static HttpServletRequest request;
    public static HttpSession httpSession;

    /**
     * 前置通知
     *
     */
    @Before("LoginAspect()&&@annotation(aspect)")
    public void doBefore(OperationAspect aspect) {

        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        assert sra != null;
        request = sra.getRequest();
        if (httpSession == null) {
            httpSession = request.getSession();
        }

        //获取浏览器 设备 版本等信息
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        OperatingSystem operatingSystem = userAgent.getOperatingSystem();
        Browser browser = userAgent.getBrowser();
        Version browserVersion = userAgent.getBrowserVersion();

            //登录记录
            loginRecord(request, operatingSystem, browser, browserVersion);

    }


    /**
     * 接口后置通知
     */
    @AfterReturning(pointcut = "LoginAspect()")
    public void doAfter() {
        try {
            deallogin();
        }catch (Exception e){
            log.info("登录后跑出异常");
        }
    }

    /**
     * 异常通知
     */
    @AfterThrowing(pointcut = "LoginAspect()")
    public void doAfterThrowing() {
    }


    private void loginRecord(HttpServletRequest request, OperatingSystem operatingSystem, Browser browser, Version browserVersion) {

        loginMap.put("systemCode", systemCode);
        loginMap.put("ip", request.getRemoteAddr());
        loginMap.put("loginDevice", "0");
        loginMap.put("loginTime", DateUtil.formatDate(new Date(), "yyyy-MM-dd hh:mm:ss"));
        loginMap.put("loginDeviceInfo", operatingSystem + " " + browser + " " + browserVersion);

    }

    private void deallogin() {
        String userId = String.valueOf(httpSession.getAttribute("userId"));
        String name = String.valueOf(httpSession.getAttribute("name"));
        loginMap.put("sessionId", httpSession.getId());
        loginMap.put("accountCode", userId);
        loginMap.put("accountName", name);
        try {
            httpClientUtils.httpPostForm(loginUrl, loginMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
