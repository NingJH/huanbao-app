package com.cqndt.huanbao.app.config;

import com.cqndt.huanbao.app.util.DateUtil;
import com.cqndt.huanbao.app.util.MapUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/**
 * Create by Intellij IDEA
 * User : mengjiajie
 * Mail : 15826014394@163.com
 * Time : 2019/6/12 17:36
 **/
@Aspect
@Component
@Profile({"dev", "test"})
@Slf4j
public class DealDateAspect {

    @Pointcut(value = "@annotation(com.cqndt.huanbao.app.service.DealDateService)")
    public void DealDateService() {
    }

    /**
     * 在切点之前织入
     *
     * @param joinPoint
     * @throws Throwable
     */
    @Before("DealDateService()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        assert sra != null;
        HttpServletRequest request = sra.getRequest();

        String url = request.getRequestURL().toString();
        String method = request.getMethod();
        String queryString = request.getQueryString();
        log.info("请求地址" + url);
        log.info("请求方法" + method);
        log.info("请求参数-->" + Arrays.toString(joinPoint.getArgs()) + queryString);
    }

    //由于使用了反射机制,影响整体性能不推荐使用
    @AfterReturning(returning = "object", pointcut = "DealDateService()")
    public void doAfter(Object object) throws Throwable {
        Map<String, Object> map = MapUtil.Obj2Map(object);
        //拿到entityList
        ArrayList list=(ArrayList) map.get("entityList");
        //循环 拿到list里面的类 比如user
        for (Object l:list){
            Field date = l.getClass().getDeclaredField("date");
            date.setAccessible(true);
            date.set(l,DateUtil.formatTime(date.get(l).toString()));
        }
    }


}
