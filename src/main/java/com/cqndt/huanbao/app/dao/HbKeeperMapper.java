package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.HbKeeper;
import com.cqndt.huanbao.app.pojo.HbKeeperLonlat;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface HbKeeperMapper {
    HbKeeper selectByPrimaryKey(Integer id);

    List<HbKeeper> listKeeper(@Param("name") String name);

    List<Map<String,Object>> recordList(@Param("companyId") Integer companyId,@Param("type") Integer type);
    List<Map<String,Object>> recordList1(@Param("companyId") Integer companyId,@Param("userId") Integer userId, @Param("type") Integer type);

    List<Map<String,Object>> measureDetail(@Param("parkId") Integer parkId,@Param("measureId") Integer measureId,@Param("type") Integer type,@Param("begin") String begin,@Param("end") String end,@Param("regionId") Integer regionId);

    List<Map<String,Object>> measureList(@Param("parkId") Integer parkId,@Param("context") String context,@Param("regionId") Integer regionId,@Param("type") Integer type);

    List<Map<String,Object>> warnAlarmList(@Param("parkId") Integer parkId,@Param("context")  String context);

    //hpe表的id
    List<Map<String,Object>> warnAlarmDetail(@Param("id") Integer id);

    List<Map<String,Object>> lists(@Param("id") Integer id);


    List<Map<String,Object>> max(@Param("parkId") Integer parkId,@Param("measureId") Integer measureId,@Param("type") Integer type,@Param("begin") String begin,@Param("end") String end,@Param("regionId") Integer regionId);

    int addKeeperLonlat(@Param("keeperId")Integer keeperId,@Param("lon") String lon,@Param("lat") String lat,@Param("date") String date,@Param("userId") Integer userId);

    HbKeeperLonlat queryKeeperLonlat(Integer userId);

    int updateKeeperLonlat(@Param("keeperId")Integer keeperId,@Param("lon") String lon,@Param("lat") String lat,@Param("date") String date,@Param("userId") Integer userId);

    int selectKeeperId(@Param("userId") Integer userId);
    List<Map<String,Object>> getsgAlarmList(@Param("name") String name, @Param("userId")Integer userId);
}