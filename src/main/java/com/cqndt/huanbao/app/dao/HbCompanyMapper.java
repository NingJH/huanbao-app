package com.cqndt.huanbao.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HbCompanyMapper {
    List<Map<String, Object>> getCompanyInfoByParkId(@Param("parkId") String parkId, @Param("regionId") String regionId, @Param("userId") String userId, @Param("name") String name);

    List<Map<String, Object>> CompanyType(@Param("parentId") int parentId);

    List<Map<String, Object>> listImpCompanyType();

    List<Map<String, Object>> CompanyTypeInfo(Integer parentId);

    /**
     * 根据园区id 查询园区下面的公司
     * @param id
     * @return
     */
    List<Map<String,String>> companyList(@Param("id") Integer id,@Param("context")  String context,@Param("regionId") Integer regionId,@Param("userId") Integer userId);

    List<Map<String,Object>> getCompanyLonLat(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("userId") int userId);

}