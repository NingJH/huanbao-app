package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.HbUserCompany;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 * @Time : 2019-08-29 14:29
 **/
@Mapper
public interface HbUserCompanyMapper {

    List<Map<String,Object>> selectRegionIdByUserId(Integer userId);

    Integer selectParkByRegionId(@Param("userId") Integer userId, @Param("regionId") Integer regionId);

    List<Map<String,Object>> selectRegion(@Param("userId") Integer userId, @Param("parkId") Integer parkId, @Param("regionId") Integer regionId);

    List<Map<String,Object>> getParkNameByid(Integer id);
    HbUserCompany getHbUserCompanyInfo(@Param("userId") Integer userId);

}
