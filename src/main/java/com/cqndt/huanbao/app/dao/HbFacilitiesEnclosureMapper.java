package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.HbFacilities;
import com.cqndt.huanbao.app.pojo.HbFacilitiesDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HbFacilitiesEnclosureMapper {

    List<Map<String,Object>> listCompanyFacilitiesEnclosure(@Param("facilitiesId") Integer facilitiesId, @Param("name") String name);

    /**
     * 根据园区id，查询园区设备列表
     * @param id
     * @return
     */
    List<Map<String,Object>> queryFacilitiesList(@Param("id") Integer id,@Param("context") String context,@Param("regionId") Integer regionId);

    /**
     * 根据环保设施id，查询环保设施详情
     * @param id
     * @return
     */
    List<Map<String,Object>> queryHbFacilitiesDetail(Integer id);

    /**
     * 根据环保设施id，查询环保设施基本信息
     * @param id
     * @return
     */
    HbFacilities queryFacilitiesInfo(Integer id);
}
