package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.BuildingGeneral;
import com.cqndt.huanbao.app.pojo.BuildingGeneralAccount;
import com.cqndt.huanbao.app.pojo.BuildingGeneralGo;
import com.cqndt.huanbao.app.pojo.BuildingGeneralMeasure;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Time : 2019-09-19 19:16
 **/
@Mapper
public interface HbBuildingGeneralMapper {


    /**
     * 查询工业固废暂存场台账
     * @param id
     * @return
     */
    List<BuildingGeneralAccount> selectGeneralAccount(Integer id);


    /**
     * 查询一般固废去向
     * @param buildingGeneralGo
     * @return
     */
    List<BuildingGeneralGo> queryGeneralGo(BuildingGeneralGo buildingGeneralGo);

    /**
     * 查询工业固废暂存场
     * @param id
     * @return
     */
    BuildingGeneral selectGeneral(Integer id);

    /**
     * 查询工业固废暂存场防护措施
     * @param id
     * @return
     */
    List<BuildingGeneralMeasure> selectGeneralMeasure(Integer id);

    List<Map<String,Object>> selectGeneralByBuildingId(Integer buildingId);
}
