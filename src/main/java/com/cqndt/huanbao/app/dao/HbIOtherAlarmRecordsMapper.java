package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.OtherAlarmRecords;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface HbIOtherAlarmRecordsMapper {
    int saveOtherAlarmRecords(OtherAlarmRecords otherAlarmRecords);
}
