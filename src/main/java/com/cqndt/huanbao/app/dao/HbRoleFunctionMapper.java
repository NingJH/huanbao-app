package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-09-02 10:17
 **/
@Mapper
public interface HbRoleFunctionMapper {
    List<Integer> listParentIdsByRoleId(@Param("roleId") int roleId);
}
