package com.cqndt.huanbao.app.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface BuildingRainSewageMapper {

    List<Map<String,Object>> selectRainSewageByBuildingId(Integer buildingId);

    List<Map<String,Object>> selectRainSewagePdf(Integer rsId);
}
