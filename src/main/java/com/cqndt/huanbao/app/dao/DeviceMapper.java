package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.Device;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Create Njh
 *
 * @Time : 2019-11-11 19:49
 **/
@Mapper
public interface DeviceMapper {

  List<Map<String, Object>> pointDevice(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("userId") int userId, @Param("deviceFor") Integer deviceFor);

  List<Device> getParkDeviceList(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("userId") int userId);


  List<Device> getCompanyDeviceList(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("userId") int userId);

  Device selectDeviceById(@Param("deviceId") Integer deviceId);

  String selectUserName(@Param("userId") Integer userId);

  List<Device> selectDeviceByParkId(@Param("id") Integer id, @Param("regionId") Integer regionId);

  Map<String, Integer> getAlarmLine(@Param("deviceNo") Object deviceNo);

  String selectFrequency(@Param("deviceNo") String deviceNo);

  Device selectDeviceByDeviceNo(@Param("deviceNo") String deviceNo);


}
