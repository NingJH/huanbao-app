package com.cqndt.huanbao.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DictionaryItemMapper {
    List<String> listDictionaryItemAll(@Param("typeCode") String typeCode);


}
