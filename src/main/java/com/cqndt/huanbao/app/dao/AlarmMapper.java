package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface AlarmMapper {

    int saveAlarm(Alarm alarm);

    int selectPhone(String phone);

    List<HbUser> getUserByAlarmLevelId(@Param("alarmLevelId") int alarmLevelId);

    List<Alarm> selectAlarm(@Param("id") Integer id, @Param("regionId") Integer regionId);

    List<Map<String, String>> getAlarmTBS001();

    List<Map<String, String>> getAlarmTBS011();

    List<Map<String, String>> getAlarmTBS060();

    List<Map<String, String>> getAlarmTBS101();

    Map<String, String> getAlarmValueTBS001();

    Map<String, String> getAlarmValueTBS011();

    Map<String, String> getAlarmValueTBS060();

    Map<String, String> getAlarmValueTBS101();

//    List<Integer> selectRoleIdByAlarmLevel(@Param("level") int level);
//
//    List<User> selectUserByRoleId(@Param("roleIdList") List<Integer> roleIdList);

    /**
     * 查询推送的用户
     * @param parkId 园区
     * @param regionId 分区
     * @param userId 指定用户
     * @param id 当前登录用户id
     * @return
     */
    List<HbUser> selectWarmObject(@Param("parkId") Integer parkId, @Param("regionId") String[] regionId, @Param("userId") String[] userId, @Param("id") int id);

    /**
     * 查询该用户对应的分区
     * @param parkId 园区id
     * @param userId 用户id
     * @return
     */
    List<ParkRegion> selectWarmRegion(@Param("parkId") Integer parkId, @Param("userId") Integer userId);

    /**
     * 查询该用户分配的公司
     * @param parkId 园区Id
     * @param regionId 分区id
     * @param companyId 公司id
     * @param userId 用户id
     * @return
     */
    List<HbCompany> selectWarmCompany(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("companyId") String[] companyId, @Param("userId") int userId);
}
