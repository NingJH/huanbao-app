package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.HbAlarm;
import com.cqndt.huanbao.app.pojo.HbUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Create Njh
 *
 * @Time : 2019-09-11 15:01
 **/
@Mapper
public interface HbAlarmUserMapper {

    List<HbUser> getUserByAlarmLevelId(@Param("alarmLevelId") int alarmLevelId);

    int saveHbAlarm(HbAlarm hbAlarm);
}
