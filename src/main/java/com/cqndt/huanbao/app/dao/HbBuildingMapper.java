package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.Building;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Time : 2019-09-12 15:58
 **/
@Mapper
public interface HbBuildingMapper {
    /**
     * 查询园区环保设施
     */
    List<Map<String,Object>> selectBuilding(Building building);

    /**
     * 查询园区环保设施 2020-2-13
     *
     */
    List<Map<String,Object>> selectBuild(Building building);

    /**
     * 雨水污水分类
     * @param
     * @return
     */
    int getRainSewageType(Integer id);

}
