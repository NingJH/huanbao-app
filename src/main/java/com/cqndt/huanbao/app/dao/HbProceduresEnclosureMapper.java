package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.HbProceduresEnclosure;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface HbProceduresEnclosureMapper {
    List<HbProceduresEnclosure> listHbProducesEnclosureInfo(@Param("procedureId") Integer procedureId, @Param("name") String name);
}