package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.Function;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-09-02 13:13
 **/
@Mapper
public interface HbFunctionMapper {

    List<Function> listFunctionByRoleId(@Param("roleId") int roleId);
}
