package com.cqndt.huanbao.app.dao;


import com.cqndt.huanbao.app.pojo.AlarmLevel;
import com.cqndt.huanbao.app.pojo.AlarmLevelCondition;

import java.util.List;

/**
 * Create by Intellij IDEA
 * User : wuhao
 * Mail : 863254617@qq.com
 * Time : 2019/7/24 14:18
 */
public interface IAlarmRoleMapper {
    List<AlarmLevel> listAlarmLevelByCondition(AlarmLevelCondition alarmLevelCondition);

    int countAlarmLevelByCondition(AlarmLevelCondition alarmLevelCondition);
}
