package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.BuildingPowerCompany;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BuildingPowerCompanyMapper {


    List<BuildingPowerCompany> companyList(BuildingPowerCompany buildingPowerCompany);

}
