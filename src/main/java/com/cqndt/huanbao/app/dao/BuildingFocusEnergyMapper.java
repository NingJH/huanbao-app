package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.BuildingPowerCompany;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface BuildingFocusEnergyMapper {

    List<Map<String,Object>> selectFocusEnergyByBuildingId(Integer buildingId);


    List<Map<String,Object>> selectRainFocusEnergyProcess(Integer feId);

    List<Map<String,Object>> selectRainFocusEnergyOutIn(BuildingPowerCompany buildingPowerCompany);
}
