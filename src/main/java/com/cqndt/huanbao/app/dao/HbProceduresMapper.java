package com.cqndt.huanbao.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HbProceduresMapper {

    List<Map<String,Object>> listHbProducesInfo(@Param("parkId") String parkId, @Param("name") String name);

    /**
     * 查询公司简介
     * @param id
     * @return
     */
    String companyIntro(Integer id);

    List<Map<String,String>> measureReport(@Param("parkId") Integer parkId, @Param("companyId") Integer companyId, @Param("type") Integer type,@Param("regionId") Integer regionId);

    String selectProceduresContent(Integer companyId);
}