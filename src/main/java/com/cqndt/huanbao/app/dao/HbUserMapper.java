package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.Device;
import com.cqndt.huanbao.app.pojo.HbUser;
import com.cqndt.huanbao.app.pojo.HbUserToken;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HbUserMapper {
    Map<String, Object> getUserInfo(@Param("username") String username, @Param("password") String password);

    int getRoleByUserId(@Param("userId") int userId);

    HbUser getUserByUsernameAndPassword(HbUser user);

    HbUser getUserById(int userId);

    List<Integer> getUserIdsByUserId(Integer userId);
    String  getRoleName(@Param("userId") Integer userId);
    int saveHbUserToken(HbUserToken token);
    int upHbUserToken(HbUserToken token);
    HbUserToken getByUserName(@Param("username") String username);

    List<Map<String, String>> getAlarmTBS001();

    List<Map<String, String>> getAlarmTBS011();

    List<Map<String, String>> getAlarmTBS060();

    List<Map<String, String>> getAlarmTBS101();

    Map<String, String> getAlarmValueTBS001();

    Map<String, String> getAlarmValueTBS011();

    Map<String, String> getAlarmValueTBS060();

    Map<String, String> getAlarmValueTBS101();
     HbUserToken getByPhone(@Param("phone")String phone);
    HbUser getUserByuserId(@Param("userId") Integer userId);
    List<HbUser> getUserList();
    List<Device> getDeviceLsit();


}