package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.HbPark;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HbParkMapper {
    List<Map<String,Object>> selectByUserId(int id);

    List<Map<String,Object>> listRegionInfo(Integer parkId);

    /**
     * 根据id查询园区信息
     * @param id
     * @return
     */
    HbPark selectPark(Integer id);

    /**
     * 根据园区ID,分区ID查询对应的环保手续
     * @param parkId,regionId
     * @return
     */
    List<Map<String,String>> selectReportById(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("context") String context,@Param("type") Integer type);


    String getParkGisLayer(Integer parkId);

    String getRegionGisLayer(Integer regionId);

    List<Map<String,Object>> getRegionLonLat(@Param("regionId") Integer regionId,@Param("parkId") Integer parkId);

    List<Map<String,Object>> fuJianList(@Param("parkId") Integer parkId,@Param("regionId") Integer regionId,@Param("context") String context);

    List<Map<String,Object>> selectProcedureExamine(@Param("parkId") Integer parkId,@Param("type") Integer type,@Param("context") String context);
}