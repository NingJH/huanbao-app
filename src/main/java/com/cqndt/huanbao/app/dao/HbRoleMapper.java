package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-09-02 10:11
 **/
@Mapper
public interface HbRoleMapper {

    Role getRoleByUserId(@Param("userId") int userId);
}
