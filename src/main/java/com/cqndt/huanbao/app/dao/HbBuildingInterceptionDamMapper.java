package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.BuildingInterceptionDam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Time : 2019-09-18 09:27
 **/
@Mapper
public interface HbBuildingInterceptionDamMapper {

    /**
     * 通过id查询拦截坝
     * @param id
     * @return
     */
    List<BuildingInterceptionDam> selectInterceptionDamById(@Param("id") Integer id);

    /**
     * 通过设施id查询拦截坝
     * @param buildingId
     * @return
     */
    List<Map<String,Object>> selectInterceptionDamByBuildingId(@Param("buildingId")Integer buildingId);
}
