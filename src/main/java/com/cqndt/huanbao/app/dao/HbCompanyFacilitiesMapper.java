package com.cqndt.huanbao.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HbCompanyFacilitiesMapper {
    List<Map<String,Object>> listCompanyFacilitiesInfoByParkId(@Param("parkId") String parkId, @Param("name") String name);
}