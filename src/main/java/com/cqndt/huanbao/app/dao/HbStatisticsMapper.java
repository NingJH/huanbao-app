package com.cqndt.huanbao.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-30 15:45
 **/
@Mapper
public interface HbStatisticsMapper {

    String rankSolidByYear(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("sewageId") Integer sewageId, @Param("year") Integer year, @Param("userId") int userId);

    String rankSolidByYearTop(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("sewageId") Integer sewageId, @Param("yearList") List<Integer> year, @Param("userId") int userId);

    String rankSolidByYearTop1(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("sewageId") Integer sewageId, @Param("yearList") List<Integer> year, @Param("userId") int userId);

    String rankSolidByYear1(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("year") Integer year, @Param("userId") int userId);

    String yearTimes(@Param("companyId") Integer companyId, @Param("sewageId") Integer sewageId, @Param("year") Integer year, @Param("userId") int userId);

    String yearTimes1(@Param("companyId") Integer companyId, @Param("sewageId") Integer sewageId, @Param("year") Integer year, @Param("userId") int userId);
    String yearTimess();
    String getTypeTOP(@Param("companyId")Integer companyId, @Param("sewageId")Integer sewageId, @Param("year")Integer year, @Param("userId")int userId);
}
