package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.BuildingRainSewageConverterPdf;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface BuildingRainSewageConverterMapper {

    List<Map<String,Object>> selectRainSewageConverterByBuildingId(Integer buildingId);

    List<BuildingRainSewageConverterPdf> selectPdf(Integer rscId);
}
