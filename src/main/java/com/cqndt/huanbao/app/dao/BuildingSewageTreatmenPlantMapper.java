package com.cqndt.huanbao.app.dao;


import com.cqndt.huanbao.app.pojo.BuildingSewageTreatmenPlants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface BuildingSewageTreatmenPlantMapper {


    Map<String,Object> selectPlants(Integer buildId);

    List<Map<String,Object>> selectPro(Integer buildId);

    List<Map<String,Object>> selectSew(Integer buildId);

    List<Map<String,Object>> selectUnit(Integer buildId);

    List<Map<String,Object>> selectSewageTreatmenPlantByBuildingId(Integer buildingId);


    List<Map<String,Object>> selectTbs001();

    List<Map<String,Object>> selectTbs011();

    List<Map<String,Object>> selectTbs060();

    List<Map<String,Object>> selectTbs101();
}
