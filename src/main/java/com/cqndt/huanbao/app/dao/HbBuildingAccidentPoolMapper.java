package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.BuildingAccidentPool;
import com.cqndt.huanbao.app.pojo.BuildingAccidentPoolAnnal;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Time : 2019-09-19 15:38
 **/
@Mapper
public interface HbBuildingAccidentPoolMapper {

    /**
     * 查询应急事故池
     * @param id
     * @return
     */
    List<BuildingAccidentPool> selectAccidentPool(@Param("id") Integer id);

    /**
     * 查询应急事故池记录
     * @return
     */
    List<BuildingAccidentPoolAnnal> selectAccidentPoolAnnl(BuildingAccidentPoolAnnal buildingAccidentPoolAnnal);

    List<Map<String,Object>> selectAccidentPoolByBuildingId(Integer buildingId);
}
