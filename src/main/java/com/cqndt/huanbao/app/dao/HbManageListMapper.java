package com.cqndt.huanbao.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HbManageListMapper {
    List<Map<String,Object>> getManageListByUserId(@Param("companyIdList") List<Integer> companyId, @Param("name") String name);

    List<Integer> getUserId(Integer userId);
}