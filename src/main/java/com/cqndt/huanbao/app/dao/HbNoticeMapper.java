package com.cqndt.huanbao.app.dao;

import com.cqndt.huanbao.app.pojo.HbNotice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * Create by Intellij IDEA
 * @Time : 2019-08-29 12:02
 **/
@Mapper
public interface HbNoticeMapper {

    List<HbNotice> selectNotice(@Param("userId") int userId);
}
