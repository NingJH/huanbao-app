package com.cqndt.huanbao.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-31 15:56
 **/
@Mapper
public interface HbMeasurePointMapper {


    List<Map<String,Object>> getmeasurePointLonLat(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("measurePointType") Integer measurePointType, @Param("userId") int userId);

    List<Map<String,Object>> measurePoint(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("measurePointType")Integer measurePointType,@Param("type") Integer type);

    String measurePointTop(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("measurePointType") Integer measurePointType,@Param("type") Integer type);
}
