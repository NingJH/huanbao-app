package com.cqndt.huanbao.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MenuMapper {
    List<Integer> getMenuByUserNotHave(@Param("roleId") int roleId);
}
