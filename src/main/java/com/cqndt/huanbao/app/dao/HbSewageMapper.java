package com.cqndt.huanbao.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-30 15:39
 **/
@Mapper
public interface HbSewageMapper {

    String getItemName(@Param("typeCode") String typeCode);
}
