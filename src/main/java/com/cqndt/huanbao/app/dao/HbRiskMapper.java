package com.cqndt.huanbao.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-31 14:13
 **/
@Mapper
public interface HbRiskMapper {

    List<Map<String,Object>> getParkInfoByUserId(@Param("parkId") Integer parkId, @Param("regionId") Integer regionId, @Param("waterTypeId") Integer waterTypeId, @Param("airTypeId") Integer airTypeId, @Param("udwaterTypeId") Integer udwaterTypeId, @Param("soilTypeId") Integer soilTypeId, @Param("userId") int userId);
}
