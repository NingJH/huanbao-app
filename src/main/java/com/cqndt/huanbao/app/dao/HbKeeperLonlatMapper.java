package com.cqndt.huanbao.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-06-19 10:00
 **/
@Mapper
public interface HbKeeperLonlatMapper {
    List<Map<String,Object>> listKeeperLonlat(@Param("parkId") String parkId,@Param("regionId") String regionId);
}
