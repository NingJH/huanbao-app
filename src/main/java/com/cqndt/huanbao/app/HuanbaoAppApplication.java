package com.cqndt.huanbao.app;

import com.didispace.swagger.EnableSwagger2Doc;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.MultipartAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan("com.cqndt.huanbao.app.dao")
@EnableTransactionManagement(proxyTargetClass = true)
@ServletComponentScan //开启扫描器 扫描注解的过滤器和拦截器
@EnableAutoConfiguration(exclude = {MultipartAutoConfiguration.class}) //自动装配时排除掉分部文件上传的配置
@EnableSwagger2Doc
@EnableScheduling
@Slf4j
public class HuanbaoAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(HuanbaoAppApplication.class, args);
    }

}
