package com.cqndt.huanbao.app.util;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * @author: dongzejun
 * @date: 2019/5/9 17:55
 */
@Configuration
public class PdfUtil {

    @Value("${pdf-url}")
    private  String pdfPath;

    public static void imagesToPdf(String fileName, String imagesPath) {

        String pdfName = fileName.substring(0, fileName.indexOf("."));
        try {
            fileName = imagesPath + pdfName + ".pdf";
            File file = new File(fileName);
            // 第一步：创建一个document对象。
            Document document = new Document();
            document.setMargins(0, 0, 0, 0);
            // 第二步：
            // 创建一个PdfWriter实例，
            PdfWriter.getInstance(document, new FileOutputStream(file));
            // 第三步：打开文档。
            document.open();
            // 第四步：在文档中增加图片。
            File files = new File(imagesPath);
            String[] images = files.list();
            int len = images.length;
            for (int i = 0; i < len; i++) {
                if (images[i].toLowerCase().endsWith(".bmp")
                        || images[i].toLowerCase().endsWith(".jpg")
                        || images[i].toLowerCase().endsWith(".jpeg")
                        || images[i].toLowerCase().endsWith(".gif")
                        || images[i].toLowerCase().endsWith(".png")) {
                    String temp = imagesPath + "/" + images[i];
                    Image img = Image.getInstance(temp);
                    img.setAlignment(Image.ALIGN_CENTER);
                    // 根据图片大小设置页面，一定要先设置页面，再newPage（），否则无效
                    document.setPageSize(new Rectangle(img.getWidth(), img.getHeight()));
                    document.newPage();
                    document.add(img);
                }
            }
            // 第五步：关闭文档。
            document.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String saveImg(String name, MultipartFile[] files) {
        List<String> list = new LinkedList();
        StringBuilder builder = new StringBuilder();
        for (MultipartFile file : files) {
            String oldName = file.getOriginalFilename();
            int index = oldName.lastIndexOf(".");
            String result = oldName.substring(index);
            if (".jpg".equals(result) || ".jpeg".equals(result) || ".gif".equals(result) || ".png".equals(result) || ".bmp".equals(result)) {
                File url = new File(pdfPath + name + "/");
                if (!url.exists()) {
                    url.mkdirs();
                }
                File fileSave = new File(pdfPath + name + "/" + oldName);
                try {
                    file.transferTo(fileSave);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                String newFileName = UUID.randomUUID() + oldName.substring(oldName.indexOf("."));
                String newImgUrl = pdfPath + name + "/";
                File url = new File(newImgUrl);
                if (!url.exists()) {
                    url.mkdirs();
                }
                File fileSave = new File(newImgUrl + newFileName);
                try {
                    file.transferTo(fileSave);
                    list.add(newFileName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        String pdfName = UUID.randomUUID().toString();
        imagesToPdf(pdfName + ".jpg", pdfPath + name + "/");
        list.add(pdfName + ".pdf");
        delAllFile(pdfPath + name + "/");
        boolean first = true;
        for (String string : list) {
            if (first) {
                first = false;
            } else {
                builder.append(",");
            }
            builder.append(string);
        }
        return builder.toString();
    }

    public boolean delAllFile(String path) {
        boolean flag = false;
        File file = new File(path);
        if (!file.exists()) {
            return flag;
        }
        if (!file.isDirectory()) {
            return flag;
        }
        String[] images = file.list();
        int len = images.length;
        for (int i = 0; i < len; i++) {
            if (images[i].toLowerCase().endsWith(".bmp")
                    || images[i].toLowerCase().endsWith(".jpg")
                    || images[i].toLowerCase().endsWith(".jpeg")
                    || images[i].toLowerCase().endsWith(".gif")
                    || images[i].toLowerCase().endsWith(".png")) {
                File file1 = new File(path + images[i]);
                if (file1.exists()) {
                    file1.delete();
                }
                flag = true;
            }
        }
        return flag;
    }

}
