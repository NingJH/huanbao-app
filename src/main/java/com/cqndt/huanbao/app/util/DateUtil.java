package com.cqndt.huanbao.app.util;

import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    /**
     * 将时间字符串转化为  yyyy-MM-dd HH:mm:ss 时间
     * @param pstrString
     * @return
     */
    public static Date stringToDate(String pstrString){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date toDate = null;
        try {
            toDate = sdf.parse(pstrString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return toDate;
    }

    /**
     * 将时间字符串转化为  yyyy-MM-dd HH:mm:ss 时间
     * @param pstrString
     * @return
     */
    public static Date stringToDateYMD(String pstrString){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date toDate = null;
        try {
            toDate = sdf.parse(pstrString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return toDate;
    }

    /**
     * 获取指定时间当天的结束时间
     * @param date
     * @return
     */
    public static Date getDateToEnd(Date date){
        LocalDateTime dateTime = LocalDateTime.of(dateToLocalDate(date), LocalTime.MAX);
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = dateTime.atZone(zone).toInstant();
        return Date.from(instant);
    }

    public static String formatDate(Date date, String pattern){
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }

    /**
     * Date转LocalDate
     * @param date
     * @return
     */
    public static LocalDate dateToLocalDate(Date date){
        Instant instant = date.toInstant();
        ZoneId zoneId = ZoneId.systemDefault();
        LocalDate toLocalDate = instant.atZone(zoneId).toLocalDate();
        return toLocalDate;
    }

    /**
     * 得到N天前的日期
     * @param date
     * @param nDayNum
     * @return
     */
    public static Date beforeNDaysDate(Date date, Integer nDayNum){
        SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
        Date d = null;
        try {
            d = formatter.parse(dateToString(date));
            long t1 = d.getTime();
            long t3=nDayNum-1;
            long t2=t3 * 24 * 60 * 60 * 1000;
            d.setTime(t1-t2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringToDate(formatter.format(d));
    }

    /**
     * 将时间转化为   yyyy-MM-dd HH:mm:ss 字符串
     * @param date
     * @return
     */
    public static String dateToString(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return(sdf.format(date));
    }

    /**
     * 获取指定时间当天的开始时间
     * @param date
     * @return
     */
    public static Date getDateToStart(Date date){
        LocalDateTime dateTime = LocalDateTime.of(dateToLocalDate(date), LocalTime.MIN);
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = dateTime.atZone(zone).toInstant();
        return Date.from(instant);
    }

    /**
     * 获取当前年份
     * @return
     */
    public static Integer getYear(){
        Calendar a=Calendar.getInstance();
        return a.get(Calendar.YEAR);
    }

    public static String formatTime(String date){
        if(StringUtils.isEmpty(date)){
            return null;
        }
        return date.substring(0, date.length() - 11);
    }

    /**
     * 将时间转化为   yyyy-MM-dd HH:mm:ss 字符串
     * @param date
     * @return
     */
    public static String dateToStr(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return(sdf.format(date));
    }
}
