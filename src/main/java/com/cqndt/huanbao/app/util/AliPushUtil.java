package com.cqndt.huanbao.app.util;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.http.ProtocolType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.aliyuncs.push.model.v20160801.*;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.springframework.context.annotation.PropertySource;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 阿里云消息推送
 * 推送的OpenAPI文档 https://help.aliyun.com/document_detail/30074.html
 *
 * @author rxj
 * @version 1.0
 * @date 2017/4/14
 */
@Slf4j
@PropertySource("classpath:config/push.properties")
public class AliPushUtil {

    /**
     * 构造函数私有化
     */
    private AliPushUtil() {

    }

    /**
     * 推送消息给android
     * 参见文档 https://help.aliyun.com/document_detail/30081.html
     *
     * @param jsonMap 推送的内容
     * @param account app账号
     * @param title 标题
     * @throws ClientException
     * @throws ServerException
     * @throws IOException
     */
    public static void pushMessageToAndroid(JSONObject jsonMap, String account, String title) throws ServerException, ClientException, IOException {
        InputStream is = AliPushUtil.class.getClassLoader().getResourceAsStream("config/push.properties");
        Properties properties = new Properties();
        properties.load(is);
        String accessKeyId = properties.getProperty("common.accessKeyId");
        String accessKeySecret = properties.getProperty("common.accessKeySecret");
        String appKey = properties.getProperty("common.appKey");//这是灾险情上报的appkey
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);
        PushMessageToAndroidRequest androidRequest = new PushMessageToAndroidRequest();
        //推送内容需要保护,请使用HTTPS协议
        androidRequest.setProtocol(ProtocolType.HTTPS);
        //推送内容较长,请使用POST请求
        androidRequest.setMethod(MethodType.POST);
        androidRequest.setAppKey(Long.valueOf(appKey));
        //推送目标 device：推送给设备 	 account：推送给指定帐号	 alias：推送给指定别名	 tag：推送给指定Tag 	all：推送给全部设备
        androidRequest.setTarget("ACCOUNT");
        androidRequest.setTargetValue(account);
        //发送的消息内容（UTF-8编码）
        androidRequest.setBody(jsonMap.toString());
        androidRequest.setTitle(title);
        PushMessageToAndroidResponse pushMessageToAndroidResponse = client.getAcsResponse(androidRequest);
        System.out.printf("RequestId: %s, MessageId: %s\n",
                pushMessageToAndroidResponse.getRequestId(), pushMessageToAndroidResponse.getMessageId());
    }

    /**
     * 推送消息给android
     * 参见文档 https://help.aliyun.com/document_detail/30081.html
     *
     * @param jsonMap 对象
     * @throws ClientException
     * @throws ServerException
     * @throws IOException
     */
    public static void pushMessageToAndroid(JSONObject jsonMap) throws ServerException, ClientException, IOException {
        InputStream is = AliPushUtil.class.getClassLoader().getResourceAsStream("conf/push.properties");
        Properties properties = new Properties();
        properties.load(is);
        String accessKeyId = properties.getProperty("common.accessKeyId");
        String accessKeySecret = properties.getProperty("common.accessKeySecret");
        String appKey = properties.getProperty("common.appKey");
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);
        PushMessageToAndroidRequest androidRequest = new PushMessageToAndroidRequest();
        //推送内容需要保护,请使用HTTPS协议
        androidRequest.setProtocol(ProtocolType.HTTPS);
        //推送内容较长,请使用POST请求
        androidRequest.setMethod(MethodType.POST);
        androidRequest.setAppKey(Long.valueOf(appKey));
        //推送目标 device：推送给设备 	 account：推送给指定帐号	 alias：推送给指定别名	 tag：推送给指定Tag 	all：推送给全部设备
        androidRequest.setTarget("ACCOUNT");
        androidRequest.setTargetValue("nandi");
        androidRequest.setBody(jsonMap.toString());//发送的消息内容（UTF-8编码）
        androidRequest.setTitle("短信推送");
        PushMessageToAndroidResponse pushMessageToAndroidResponse = client.getAcsResponse(androidRequest);
        System.out.printf("RequestId: %s, MessageId: %s\n",
                pushMessageToAndroidResponse.getRequestId(), pushMessageToAndroidResponse.getMessageId());
    }

    /**
     * 推送通知给android
     * 参见文档https://help.aliyun.com/document_detail/30082.html
     *
     * @param jsonMap
     * @throws ServerException
     * @throws ClientException
     * @throws IOException
     */
    public static void pushNoticeToAndroid(JSONObject jsonMap, String phone) throws ServerException, ClientException, IOException {
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAIIsO8n4DLyiwy", "Ec5aBbjeOsae4yXegsQWUWkrxlHVKw");
        DefaultAcsClient client = new DefaultAcsClient(profile);
        PushNoticeToAndroidRequest androidRequest = new PushNoticeToAndroidRequest();
        //推送内容需要保护,请使用HTTPS协议
        androidRequest.setProtocol(ProtocolType.HTTPS);
        //推送内容较长,请使用POST请求
        androidRequest.setMethod(MethodType.POST);
        androidRequest.setAppKey(Long.valueOf("23577167"));
        //推送目标 device：推送给设备 	 account：推送给指定帐号	 alias：推送给指定别名	 tag：推送给指定Tag 	all：推送给全部设备
        androidRequest.setTarget("ACCOUNT");
        androidRequest.setTargetValue(phone);//设置推送号码
        androidRequest.setTitle("任务派遣");//发送的通知标题,最长20个字符,中文算1个字符
        androidRequest.setBody("您有一个派遣任务需要完成!");//发送的通知内容（UTF-8编码）
        androidRequest.setExtParameters(jsonMap.toString());
        PushNoticeToAndroidResponse pushNoticeToAndroidResponse = client.getAcsResponse(androidRequest);
        System.out.printf("RequestId: %s, MessageId: %s\n",
                pushNoticeToAndroidResponse.getRequestId(), pushNoticeToAndroidResponse.getMessageId());
    }

    public static void pushMassageToIOS(JSONObject jsonMap, String phone) throws ServerException, ClientException, IOException {
        InputStream is = AliPushUtil.class.getClassLoader().getResourceAsStream("conf/push.properties");
        Properties properties = new Properties();
        properties.load(is);
        String accessKeyId = properties.getProperty("ios.accessKeyId");
        String accessKeySecret = properties.getProperty("ios.accessKeySecret");
        String appKey = properties.getProperty("ios.appKey");
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);
        PushMessageToiOSRequest iosRequest = new PushMessageToiOSRequest();
        //推送内容需要保护,请使用HTTPS协议
        iosRequest.setProtocol(ProtocolType.HTTPS);
        //推送内容较长,请使用POST请求
        iosRequest.setMethod(MethodType.POST);
        // 推送目标
        iosRequest.setAppKey(Long.valueOf(appKey));
        iosRequest.setTarget("ACCOUNT");
        iosRequest.setTargetValue(phone);
        iosRequest.setTitle("通知");
        iosRequest.setBody(jsonMap.toString());
        PushMessageToiOSResponse pushMessageToiOSResponse = client.getAcsResponse(iosRequest);
        System.out.printf("RequestId: %s, MessageId: %s\n",
                pushMessageToiOSResponse.getRequestId(), pushMessageToiOSResponse.getMessageId());

    }

    /**
     * 给安卓推送视屏
     *
     * @param jsonMap
     * @param phone
     * @throws Exception
     */
    public static void pushVideoToAndroid(JSONObject jsonMap, String phone) throws Exception {
        InputStream is = AliPushUtil.class.getClassLoader().getResourceAsStream("conf/push.properties");
        Properties properties = new Properties();
        properties.load(is);
        String accessKeyId = properties.getProperty("accessKeyId");
        String accessKeySecret = properties.getProperty("accessKeySecret");
        String appKey = properties.getProperty("danger.appKey");//这是灾险情上报的appkey
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);
        PushMessageToAndroidRequest androidRequest = new PushMessageToAndroidRequest();
        //推送内容需要保护,请使用HTTPS协议
        androidRequest.setProtocol(ProtocolType.HTTPS);
        //推送内容较长,请使用POST请求
        androidRequest.setMethod(MethodType.POST);
        androidRequest.setAppKey(Long.valueOf(appKey));
        //推送目标 device：推送给设备 	 account：推送给指定帐号	 alias：推送给指定别名	 tag：推送给指定Tag 	all：推送给全部设备
        androidRequest.setTarget("ACCOUNT");
        androidRequest.setTargetValue(phone);
        jsonMap.put("type", 3);//3表示视频
        androidRequest.setTitle(jsonMap.getString("invite"));//发送的通知标题,最长20个字符,中文算1个字符
        androidRequest.setBody(jsonMap.toString());
        PushMessageToAndroidResponse pushMessageToAndroidResponse = client.getAcsResponse(androidRequest);
        System.out.printf("RequestId: %s, MessageId: %s\n",
                pushMessageToAndroidResponse.getRequestId(), pushMessageToAndroidResponse.getMessageId());
    }





    /**
     * 安卓、ios通用通知推送
     * create by rpc 2018-1-4
     *
     * @param title   通知标题
     * @param body    通知的消息体
     * @param params  参数
     * @param account APP帐号
     * @throws ServerException
     * @throws ClientException
     * @throws IOException
     */
    public static synchronized void pushNoticeByAccount(String title, String body, JSONObject params, String account) throws ServerException, ClientException, IOException {
        InputStream is = AliPushUtil.class.getClassLoader().getResourceAsStream("config/push.properties");
        Properties properties = new Properties();
        properties.load(is);
        String accessKeyId = properties.getProperty("common.accessKeyId");
        String accessKeySecret = properties.getProperty("common.accessKeySecret");
        String appKey = properties.getProperty("common.appKey");
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);
        PushRequest pushRequest = new PushRequest();
        // 推送目标
        pushRequest.setAppKey(Long.valueOf(appKey));
        //推送目标: DEVICE:按设备推送 ALIAS : 按别名推送 ACCOUNT:按帐号推送  TAG:按标签推送; ALL: 广播推送
        pushRequest.setTarget("ACCOUNT");
        //根据Target来设定,如Target=DEVICE, 则对应的值为 设备id1,设备id2. 多个值使用逗号分隔.(帐号与设备有一次最多100个的限制)
        pushRequest.setTargetValue(account);
        // 消息类型 MESSAGE NOTICE
        pushRequest.setPushType("NOTICE");
        // 设备类型 ANDROID iOS ALL.
        pushRequest.setDeviceType("ALL");

        // 推送配置
        // 消息的标题
        pushRequest.setTitle(title);
        // 消息的内容
        pushRequest.setBody(body);

        // 推送配置: iOS
        //iOS的通知是通过APNs中心来发送的,需要填写对应的环境信息。"DEV" : 表示开发环境 "PRODUCT" : 表示生产环境
        pushRequest.setIOSApnsEnv("PRODUCT");
        // 添加标记(自增)
        pushRequest.setIOSBadgeAutoIncrement(true);
        //通知的扩展属性(注意 : 该参数要以json map的格式传入,否则会解析出错)
        pushRequest.setIOSExtParameters(params.toString());

        //设定通知的扩展属性。(注意 : 该参数要以 json map 的格式传入,否则会解析出错)
        pushRequest.setAndroidExtParameters(params.toString());
        //通知的提醒方式 "VIBRATE" : 震动 "SOUND" : 声音 "BOTH" : 声音和震动 NONE : 静音
        pushRequest.setAndroidNotifyType("BOTH");

        PushResponse pushResponse = client.getAcsResponse(pushRequest);
        System.out.printf("RequestId: %s, MessageID: %s\n",
                pushResponse.getRequestId(), pushResponse.getMessageId());
    }
    public static void AdvancedPush(String title, String body, JSONObject params, String account) throws Exception {
        if (StringUtils.isEmpty(account)){
            System.out.println("----------------该app账号暂未登录-------------------------------");
            return;
        }
        log.info("推送数据"+params);
        InputStream is = AliPushUtil.class.getClassLoader().getResourceAsStream("config/push.properties");
        Properties properties = new Properties();

        properties.load(is);
        String accessKeyId = properties.getProperty("common.accessKeyId");
        String accessKeySecret = properties.getProperty("common.accessKeySecret");
        String appKey = properties.getProperty("common.appKey");
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);
        PushRequest pushRequest = new PushRequest();
        // 推送目标
        pushRequest.setAppKey(Long.valueOf(appKey));
        pushRequest.setTarget("ACCOUNT"); //推送目标: DEVICE:推送给设备; ACCOUNT:推送给指定帐号,TAG:推送给自定义标签; ALL: 推送给全部
        pushRequest.setTargetValue(account);
        pushRequest.setPushType("NOTICE"); // 消息类型 MESSAGE NOTICE
        pushRequest.setDeviceType("ALL"); // 设备类型 ANDROID iOS ALL.
        // 推送配置
        pushRequest.setTitle(title); // 消息的标题
        pushRequest.setBody(body); // 消息的内容
        // 推送配置: Android
        pushRequest.setAndroidNotifyType("BOTH");//通知的提醒方式 "VIBRATE" : 震动 "SOUND" : 声音 "BOTH" : 声音和震动 NONE : 静音
        pushRequest.setAndroidOpenType("NONE"); //点击通知后动作 "APPLICATION" : 打开应用 "ACTIVITY" : 打开AndroidActivity "URL" : 打开URL "NONE" : 无跳转
        if(!StringUtils.isEmpty(params)){
            //通知的扩展属性(注意 : 该参数要以json map的格式传入,否则会解析出错)
            pushRequest.setIOSExtParameters(params.toString());
            //设定通知的扩展属性。(注意 : 该参数要以 json map 的格式传入,否则会解析出错)
            pushRequest.setAndroidExtParameters(params.toString());
        }

        // 指定notificaitonchannel id
        pushRequest.setAndroidNotificationChannel("0x11");
        PushResponse pushResponse = client.getAcsResponse(pushRequest);
        System.out.printf("RequestId: %s, MessageID: %s\n",
                pushResponse.getRequestId(), pushResponse.getMessageId());
    }


//    public static void main(String[] args) {
//        JSONObject jsonObject = new JSONObject();
////        jsonObject.put("type", 1);
////        jsonObject.put("room", "123");
//        try {
//            System.out.println(jsonObject);
//            pushNoticeByAccount("视频通话", "XXX请求视频通话", jsonObject, "12345678910");
////            pushNoticeByAccount("视频通话","XXX请求视频通话",jsonObject,"123123123");
////            pushNoticeToiOS("XXX请求视频通话",jsonObject,"123456");
//        } catch (ClientException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
}
